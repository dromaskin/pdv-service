<!DOCTYPE html>
<html>
    <head>
        <title>Parkme Zona Azul - Em Breve</title>
        <link rel="stylesheet" href="/bootstrap.min.css">
        <link rel="stylesheet" href="/recover.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
          ga('create', 'UA-83159847-1', 'auto');
          ga('send', 'pageview');
        </script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="navbar-brand">
                        <div class="logo"></div>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="col-xs-12">
                    <div class="jumbotron text-center">
                        <h2>Funcionalidade disponível em breve. Aguarde.</h2>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
