<h1>PARKME TERMOS DE USO</h1>
    1.	ACEITAÇÃO DOS TERMOS E CONDIÇÕES DE USO 

1.1.	Este Termo de Uso apresenta as "Condições Gerais " aplicáveis ao uso dos serviços oferecidos por PARKME BRASIL LTDA. ME, pessoa jurídica de direito privado, inscrita no CNPJ/MF sob nº. 22.346.230/0001-40, com sede na Av. Paulista, 807 – Cj. 2315, Bela Vista, São Paulo/SP, CEP 01311-001, na cidade de São Paulo/SP, doravante denominada de PROVEDORA, do aplicativo para celular denominado PARKME PARKING.

1.2.	A PROVEDORA fornece a você, doravante denominado de USUÁRIO, sujeito aos Termos e Condições de Uso abaixo, acesso a informações que utilizam produtos e/ou serviços voltados para o estacionamento rotativo do município de São Paulo, disponíveis neste aplicativo para celular denominado PARKME PARKING. 

1.3.	As informações contidas no aplicativo para celular denominado PARKME PARKING poderão ser modificadas ou extintas a qualquer momento, sem que haja a necessidade de informação prévia ao USUÁRIO. O USUÁRIO poderá revisar a versão mais atual deste termo de uso, tocando no link "Termos e Condições" localizado no Menu do aplicativo. Será o USUÁRIO o responsável por verificar periodicamente se houve mudanças neste termo. 

1.4.	Na tela de Cadastro, após tocar no botão referente ao texto "Aceito os termos e condições de uso do aplicativo” para confirmar o aceite, o USUÁRIO declara que leu e concordou expressamente com a versão mais recente do Termo de Uso e com todas as demais políticas e princípios que o regem, fato este que o vinculará automaticamente às regras aqui contidas. 

1.5.	O USUÁRIO deverá ler, certificar-se de haver entendido e aceitar todas as condições estabelecidas no Termo de Uso, assim como nos demais documentos incorporados ao mesmo por referência, antes de seu cadastro como USUÁRIO.

1.6.	A ACEITAÇÃO DO TERMO DE USO É ABSOLUTAMENTE INDISPENSÁVEL À UTILIZAÇÃO DO APLICATIVO E DE SEUS SERVIÇOS/PRODUTOS. Se o USUÁRIO continuar usando aplicativo depois de publicado mudanças no Termo de Uso, estará indicando automaticamente a aceitação das novas condições. 

2.	CAPACIDADE PARA CADASTRAR-SE E UTILIZAR O APLICATIVO. 

2.1.	Os serviços/produtos do aplicativo estão disponíveis apenas para as pessoas que tenham capacidade legal para contratá-los. Não podem utilizá-los, assim, pessoas que não gozem dessa capacidade, como também pessoas que tenham sido inabilitadas do aplicativo PARKME PARKING temporária ou definitivamente. 

2.2.	Também não é permitido que uma mesma pessoa tenha mais de um cadastro. Se a PROVEDORA do aplicativo PARKME PARKING detectar cadastros duplicados, através do sistema de verificação de dados, irá inabilitar definitivamente todos os cadastros. 

3.	DESCRIÇÃO DO OBJETO 

3.1.	O aplicativo PARKME PARKING consiste em colocar a disposição do USUÁRIO uma ligação direta com produtos e serviços referentes à venda e ativação de créditos para estacionamento rotativo no município de São Paulo. 

3.2.	Para obter acesso ao ambiente de informações, o usuário não tem obrigatoriedade de estar cadastrado. Todavia, para acesso ao ambiente de comercialização, o usuário deverá cadastrar-se no aplicativo PARKME PARKING, no momento em que efetua a compra de seu primeiro CARTÃO AZUL DIGITAL.

3.3.	O USUÁRIO fica desde logo informado e concorda que os SERVIÇOS/PRODUTOS via aplicativo PARKME PARKING estarão disponíveis 24 (vinte e quatro) horas por dia, 07 (sete) dias por semana, salvo a ocorrência da culpa de terceiros, caso fortuito ou força maior. A utilização das vagas de estacionamento rotativo estará vinculada aos horários de funcionamento conforme regulamentação do município de São Paulo. 

3.4.	A partir do aplicativo PARKME PARKING, o USUÁRIO poderá realizar, e não somente, as seguintes operações: 

3.4.1.	Fazer cadastramento e atualização de dados pessoais; 

3.4.2.	Possibilidade de se tornar USUÁRIO da PROVEDORA, através da compra e/ou ativação de créditos para estacionar nos estacionamentos rotativos do município de São Paulo; 

3.4.3.	Visualizar o histórico de transações feitas com o aplicativo PARKME PARKING;

3.4.4.	Cadastrar novas opções de placas de veículos, e definir qual a placa a ser carregada automaticamente como padrão; 

3.4.5.	Acionar os alarmes do aplicativo para informar o tempo restante para o vencimento da ativação do último Cartão Azul Digital ativo para determinada placa de veículo estacionado em região de estacionamento rotativo;

3.4.6.	Cadastrar, em seu CPF, mais de uma placa para aquisição de CARTÕES AZUIS DIGITAIS (CAD). No entanto, uma mesma pessoa não poderá ter mais de 01 (um) cadastro. O CARTAO AZUL DIGITAL é o produto pelo qual o USUÁRIO paga, em reais, para compra de crédito eletrônico de tempo de estacionamento rotativo na cidade de São Paulo, permitindo a permanência em uma das vagas por tempo determinado.

3.4.7.	Ativar, por cada IMEI ou UUID, os Cartões Azuis Digitais para até 2 placas simultâneas.

3.4.8.	Adquirir e ativar os Cartões Azuis Digitais a qualquer tempo. Se a ativação for realizada entre a meia noite e o horário de início da regra de estacionamento da região, o cartão terá sua validade iniciada a partir do primeiro minuto do início da regra de estacionamento do local.

4.	OBRIGAÇÕES DE REGISTRO DO USUÁRIO 

4.1.	Com vistas à utilização dos SERVIÇOS/PRODUTOS, de acordo com os termos deste Termo de Uso, o USUÁRIO se obriga a fornecer, quando do seu registro, informações pessoais verdadeiras e atuais, sendo que o mesmo USUÁRIO desde logo autoriza a manutenção destas informações em banco de dados e, facultativamente, autoriza também o envio de malas diretas ao e-mail cadastrado. 

4.2.	A PROVEDORA TERÁ O DIREITO DE SUSPENDER A UTILIZAÇÃO DOS SERVIÇOS PELO USUÁRIO CASO VERIFIQUE, POR QUAISQUER MEIOS IDÔNEOS, QUE AS INFORMAÇÕES PRESTADAS PELO USUÁRIO SÃO FALSAS OU IRREAIS, INDEPENDENTEMENTE DE NOTIFICAÇÃO PRÉVIA DO USUÁRIO. 

4.3.	A PROVEDORA poderá fornecer as informações do USUÁRIO a terceiros nos seguintes casos: 

4.3.1.	 Por determinação Judicial; 

4.3.2.	Em atendimento a requerimento do Ministério Público, de autoridade policial e demais órgãos competentes; 

4.3.3.	 Para garantir a não violação de direitos de terceiros; 


5.	POLÍTICA DE ENTREGA ELETRÔNICA E SEU CONSENTIMENTO 

5.1.	A PROVEDORA poderá enviar e-mails para o endereço eletrônico cadastrado pelo USUÁRIO, exceto se o USUÁRIO, quando consultado, se manifestar em sentido contrário. Os e-mails de caráter técnico-informativo, que tratem dos serviços e das condições ora contratados, não estarão sujeitos à recusa do USUÁRIO.

6.	IDENTIFICAÇÃO DO USUÁRIO 

6.1.	Os SERVIÇOS/PRODUTOS serão disponibilizados para cada USUÁRIO mediante a utilização de mecanismo de identificação e senha. A correta utilização deste mecanismo será de inteira e exclusiva responsabilidade do USUÁRIO, o qual estará ciente de que a senha de acesso aos SERVIÇOS/PRODUTOS é pessoal e intransferível. O USUÁRIO se obriga a avisar imediatamente à PROVEDORA caso suspeite ou tenha conhecimento de que a sua senha esteja sendo utilizada por outrem. 

6.2.	Em hipótese alguma a PROVEDORA será responsável pela perda da senha ou utilização desta por outrem. 

6.3.	Apenas será confirmado o cadastramento do interessado que preencher todos os campos do cadastro. O futuro USUÁRIO deverá completá-lo com informações exatas, precisas e verdadeiras, e assume o compromisso de atualizar os dados pessoais sempre que neles ocorrer alguma alteração. 

6.4.	A PROVEDORA se reserva o direito de utilizar todos os meios válidos, possíveis e idôneos para identificar seu USUÁRIO, bem como de solicitar dados adicionais e documentos que estime serem pertinentes a fim de conferir os dados pessoais informados. 

6.5.	Caso a PROVEDORA decida verificar a veracidade dos dados cadastrais de um USUÁRIO e se constate haver entre eles dados incorretos ou inverídicos, ou ainda caso o USUÁRIO se furte ou negue a enviar os documentos requeridos, a PROVEDORA poderá bloqueá-lo para novas aquisições e/ou ativações, ou ainda suspender temporariamente ou cancelar definitivamente o cadastro do USUÁRIO, sem prejuízo de outras medidas que entender necessárias e oportunas. 

6.6.	Havendo a aplicação de qualquer das sanções acima referidas, automaticamente serão canceladas as aquisições de ofertas feitas pelo USUÁRIO, não assistindo ao USUÁRIO, por essa razão, qualquer sorte de indenização ou ressarcimento. Aplicada a sanção, caso o USUÁRIO ainda possua créditos remanescentes de serviços adquiridos, a PROVEDORA se obriga a fazer a devolução de valor a ser discutido em cada caso.


7.	DA CENTRAL DE ATENDIMENTO AO USUÁRIO:

7.1.	A PROVEDORA manterá a Central de Atendimento Telefônico para comunicação de extravio, furto ou fraude de SENHA, comunicação de apropriação indevida por terceiros, compras, ativações, consulta de saldo e demais informações necessárias. Os meios de contato com a PROVEDORA serão o e-mail suporte@parkme.com.br e a central telefônica (11) 3181-4986, que serão divulgados por intermédio dos meios de comunicação como, exemplificativamente, mas sem exclusão de outros, o próprio aplicativo, site, correspondência virtual e anúncios.

8.	RESPONSABILIDADE 

8.1.	O USUÁRIO concorda e reconhece que eventuais negociações ou participações que fizer ou optar junto às promoções veiculadas por anunciantes, presentes no aplicativo, incluindo pagamento ou entrega de bens e serviços, condições, garantias e apresentações associadas a tal negociação, ocorrerão exclusivamente, por conta e responsabilidade do USUÁRIO e do anunciante. O USUÁRIO concorda que o aplicativo PARKME PARKING não é responsável por quaisquer prejuízos originários da falha ou falta de qualquer serviço/produto disponibilizado por terceiros a exemplo dos anunciantes, fornecedores e prestadores de serviços. 

8.2.	Podem existir links de outros sites na página da PROVEDORA, o que não significa que esses sites sejam de sua propriedade ou por ela operados, não sendo, portanto, responsável pelos conteúdos, práticas e serviços ofertados ou divulgados nos mesmos. A presença de links para outros sites não implica relação de sociedade, associação, consórcio, vinculação, supervisão, cumplicidade ou solidariedade da PROVEDORA para com esses sites, seus conteúdos e seus respectivos titulares.

8.3.	A PROVEDORA, igualmente, não será responsabilizada por custos, prejuízos e/ou danos causados ao USUÁRIO ou a terceiros por:

8.3.1.	Dano, prejuízo ou perda do equipamento do USUÁRIO causado por falha no sistema, no servidor ou na internet, ou em decorrência da transferência de dados, arquivos, imagens, textos ou áudio para o celular do USUÁRIO;

8.3.2.	Mau funcionamento dos serviços, linhas e/ou conexões utilizados pelo USUÁRIO;

8.3.3.	Mau funcionamento de software de terceiros;

8.3.4.	Má utilização do SERVIÇO, incluindo, mas não se limitando a, erro no uso ou não utilização dos CARTÕES AZUIS DIGITAIS adquiridos, ou ainda quando excedido o limite permitido para o uso das vagas.

8.4.	Em hipótese alguma o aplicativo PARKME PARKING poderá ser responsabilizado por qualquer erro originado na utilização ou não utilização do SERVIÇO/PRODUTO, principalmente quanto ao recebimento de multas de trânsito, erro de digitação ou erro na escolha do pagamento das tarifas. 

9.	CANCELAMENTO DA CONTA CADASTRO:

9.1.	É facultado à PROVEDORA e ao USUÁRIO encerrarem suas relações contratuais, hipótese em que a PROVEDORA procederá ao cancelamento da CONTA CADASTRO. Deve-se observar ainda que: a) Quando o cancelamento se der por iniciativa do USUÁRIO, será considerado efetivado somente após comunicação feita à Central de Atendimento Telefônico; b) Quando o cancelamento se der por iniciativa da PROVEDORA, o fato deverá ser comunicado previamente ao USUÁRIO. Deixando o USUÁRIO de cumprir qualquer disposição deste Termo, poderá a PROVEDORA, independentemente de notificação ou de qualquer outra formalidade prévia, cancelar o respectivo CADASTRO, impedindo sua utilização.

9.2.	A PROVEDORA efetuará ainda o cancelamento da CONTA CADASTRO, independentemente de aviso, nas seguintes hipóteses:

9.2.1.	Por ordem da Autoridade Competente;

9.2.2.	Por ordem do Poder Judiciário;

9.2.3.	Quando se constatar:
9.2.3.1.	Utilização de meio inidôneos, com objetivo de postergar pagamentos e/ou cumprimento de obrigações assumidas com a PROVEDORA;
9.2.3.2.	Irregularidade nas informações prestadas, julgadas de natureza grave pela PROVEDORA; e
9.2.3.3.	CPF/MF cancelado pela Receita Federal;

9.3.	O cancelamento da CONTA CADASTRO não extingue as relações contratadas entre o USUÁRIO e a PROVEDORA, o que só ocorrerá depois de liquidadas todas as obrigações existentes.


10.	DA VIGÊNCIA E DEVOLUÇÃO DOS CRÉDITOS:

10.1.	O CRÉDITO é válido por tempo indeterminado; no entanto, no caso de solicitação de devolução dos créditos, como por exemplo na solicitação de cancelamento da conta cadastro, serão cobradas TARIFAS a serem definidas pela PROVEDORA para cada caso.


11.	DISPOSIÇÕES GERAIS 

11.1.	O presente instrumento constitui-se no único documento regulador das relações contratuais, revogando-se expressamente todo e qualquer contrato anteriormente existente entre as partes, que trate do mesmo objeto aqui especificado. 

11.2.	A não exigência, por qualquer uma das partes, do cumprimento de qualquer cláusula ou condição estabelecida, será considerada mera tolerância, não implicando na sua novação, e tampouco na abdicação do direito de exigi-la no futuro, não afetando a validade deste instrumento e quaisquer de suas condições. 

11.3.	Na hipótese de qualquer uma das disposições deste contrato vir a ser considerada contrária à lei brasileira, por qualquer autoridade governamental ou decisão judicial, as demais disposições não afetadas continuarão em vigor e as partes deverão alterar este instrumento de forma a adequá-lo a tal lei ou à decisão judicial. 

11.4.	Este contrato obriga as partes e seus sucessores a qualquer título, constituindo-se em título executivo extrajudicial, nos termos do artigo 585, inciso II do Código de Processo Civil. 

11.5.	O USUÁRIO autoriza a PROVEDORA utilizar recursos, tais como, mas não se limitando a, "cookies" (pequenas mensagens comerciais instantâneas), a fim de oferecer um serviço melhor e mais personalizado. 

12.	FORO 

12.1.	As partes elegem o foro da Comarca da Cidade de São Paulo, Estado de São Paulo, para dirimir eventuais dúvidas ou controvérsias decorrentes do presente Contrato, excluindo-se qualquer outro, por mais privilegiado que seja. 
