<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>App Zona Azul em São Paulo - Parkme Brasil</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="images/favicon.ico">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <!-- Fonts -->




        <!-- CSS -->

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/responsive.css">
        <link rel="stylesheet" href="https://fera.ag/footer-signature/styles.css">


        <!-- Js -->
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/main.js"></script>
        <script>
         new WOW(
            ).init();
        </script>

        <link rel="stylesheet" href="https://fera.ag/assinatura/autoral/styles.css">

        <style>
            .btn-regulamento {
                margin-top: 50px;
                display: inline-block;
                border: 2px solid #3083df;
                padding: 10px 20px;
                border-radius: 4px;
            }
        </style>

    </head>
    <body>




    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="app-showcase wow fadeInDown" data-wow-delay=".5s">
                        <img src="images/iphone.png" alt="">
                    </div>

                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="block wow fadeInRight" data-wow-delay="1s">
                        <span class="logo">
                            <img src="images/logo.png" alt="">
                        </span>
                        <h2><strong>Parkme</strong> - Seu aplicativo</br> de Zona Azul em São Paulo</h2>
                        <p>Aplicativo oficial Zona Azul Digital.  <br/>Versões disponíveis para Android e IOS.</p>
                        <div class="download-btn">
                            <a href="https://play.google.com/store/apps/details?id=com.parkme.consumer&hl=pt_BR" target="_blank" class="andriod">
                                <img src="images/andriod-button.png" alt="">
                            </a>
                            <a href="https://itunes.apple.com/br/app/parkme-parking/id417605484?mt=8" target="_blank" class="apple">
                                <img src="images/apple-button.png" alt="">
                            </a>
                        </div>

                        <div class="contato">
                            <p>Empresas e Frotas</p>
                            <a href="tel:+551131814986">(11) 3181-4986</a>
                            <p>Horário comercial, das 9h às 18h, de segunda às sexta.</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </header>






    <section id="service">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title wow pulse" data-wow-delay=".5s">
                        <h2><strong>3 motivos para usar</strong> Parkme Zona Azul</h2>
                        <p>
                            Para quem procura um App de Zona Azul fácil e prático de usar. Você vai conseguir comprar seu <strong>Cartão Azul Digital (CAD)</strong> sem complicações. Chegamos para facilitar sua vida.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="block wow fadeInLeft" data-wow-delay=".7s">
                        <img src="images/exclamation.png" alt="">
                        <h3>Sistema de Notificações</h3>
                        <p>
                         O Parkme Zona Azul oferece a opção do usuário receber notificações sempre que desejar. Isso permite que você receba notificações quando:
                            <ul type="circle">
                             <li>O CAD estiver próximo de expirar.</li>
                             <li>O CAD tiver expirado.</li>
                            </ul>
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="block wow fadeInLeft" data-wow-delay=".8s">
                        <img src="images/clock.png" alt="">
                        <h3>Apenas 3 passos de compra</h3>
                        <p>
                            Em 3 passos você finaliza a compra do seu CAD.
                            Depois do primeiro cadastro, nosso sistema nunca mais irá solicitar dados cadastrais. Passos de compra:
                            <ul type="circle">
                             <li>Dados do usuário</li>
                             <li>Resumo da compra</li>
                             <li>Detalhes do pagamento</li>
                            </ul>

                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="block wow fadeInLeft" data-wow-delay="1.1s">
                        <img src="images/map.png" alt="">
                        <h3>Visão com Mapa</h3>
                        <p>
                            Nenhum outro app oferece a oportunidade de visualizar as quadras disponíveis via mapa para compra de um Cartão Digital Azul (CAD). Ao selecionar uma vaga no mapa, uma visão rápida é exibida para o usuário com informações do endereço completo, distância, foto da rua e um botão para realizar a compra.

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="ads">
        <div class="row">
            <div class="col-md-12">
                <div class="title wow pulse" data-wow-delay=".5s">
                    <h2><strong>Parceria Bradesco e Parkme</strong></h2>
                    <p>Desconto de 10% no Zona Azul digital através da ParkMe.</p>
                    <a href="/regulamento-bradesco.html" target="_blank">
                        <img src="/images/parceria-bradesco-parkme.jpg">
                    </a>
                    <br />
                    <a target="_blank" class="btn-regulamento" href="/regulamento-bradesco.html">
                        Confira o regulamento da promoção
                    </a>
                </div>
            </div>
        </div>
    </section>



    <section id="showcase">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block text-center wow fadeInUp" data-wow-delay=".5s">
                        <img src="images/showcase-img.png" alt="">

                    </div>
                </div>
            </div>
        </div>
    </section>




    <section id="feature">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title wow pulse" data-wow-delay=".5s">
                        <h2><strong>Como usar</strong> Parkme Zona Azul em São Paulo</h2>
                        <p>
                            Confira as funcionalidades principais para que sua experiência seja a mais agradável possível.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="block">
                        <div class="media wow fadeInDown" data-wow-delay=".5s">
                            <img class="media-object pull-left" src="images/item-1.png" alt="Image">
                            <div class="media-body">
                                <h4 class="media-heading">Dados do usuário</h4>
                                <p>Caso você não possua uma conta no App, basta preencher os campos com os seus dados pessoais que servirão para identificação  posteriormente: <strong>nome, sobrenome, email, cpf/cnpj e celular</strong></p>
                            </div>
                        </div>
                        <div class="media wow fadeInDown" data-wow-delay=".8s">
                                <img class="media-object pull-left" src="images/item-2.png" alt="Image">
                            <div class="media-body">
                                <h4 class="media-heading">Autenticação</h4>
                                <p>Você deve informar seu email e senha para efetuar a autenticação no aplicativo. Nos próximos acessos o App já detecta seu perfil automaticamente e sua compra fica ainda mais ágil, não exigindo mais informações de login e senha. Em poucos passos sua compra será concluída.</p>
                            </div>
                        </div>
                        <div class="media wow fadeInDown" data-wow-delay="1.1s">
                                <img class="media-object pull-left" src="images/item-3.png" alt="Image">
                            <div class="media-body">
                                <h4 class="media-heading">Resumo da compra</h4>
                                <p>Nesse passo o aplicativo permite a inserção dos seguintes campos: Placa, Tipo de Vaga (Período ou quantidade de CADs pré-pagos, Ativação imediata, Tipo de vaga), Data e Hora de entrada, Data e hora de saída</p>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="block">
                        <div class="media wow fadeInDown" data-wow-delay="1.4s">
                            <img class="media-object pull-left" src="images/item-4.png" alt="Image">
                        <div class="media-body">
                            <h4 class="media-heading">Detalhes do pagamento</h4>
                            <p>Nessa visão o App permite que você informe os dados de pagamento baseado no valor total calculado referente a regra de estacionamento. Na sequência você escolhe a forma de pagamento (cartão de crédito) e é só avançar.</p>
                        </div>
                        </div>
                        <div class="media wow fadeInDown" data-wow-delay="1.7s">
                                <img class="media-object pull-left" src="images/item-5.png" alt="Image">
                            <div class="media-body">
                                <h4 class="media-heading">Finalização - Ativação do CAD</h4>
                                <p>Quando você confirma a Aquisição/Ativação do CAD e confirma o número de CADs através de um alerta, é iniciado o processo de validação da compra. O tempo de permanência de cada CAD ativo pode ser acompanhado através do menu do usuário autenticado <strong>Ver conta > Minhas compras.</strong></p>
                            </div>
                        </div>
                        <div class="media wow fadeInDown" data-wow-delay="1.9s">
                                <img class="media-object pull-left" src="images/item-6.png" alt="Image">
                            <div class="media-body">
                                <h4 class="media-heading">Comprovante</h4>
                                <p>O comprovante de pagamento é exibido ao final da Aquisição/Ativação. O App permite que o usuário autenticado acesse os comprovantes a qualquer momento através do menu <strong>Ver conta > Minhas compras.</strong></p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>


    <section id="testimonial">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <div id="owl-example" class="owl-carousel">
                            <div>
                                <div class="col-md-4">
                                    <img src="images/boni.png" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h5>
                                        “Comprar o Cartão Azul no Parkme foi mesmo muito fácil. Aprovado!”
                                    </h5>
                                    <div class="name">André Boni / Desenvolvedor</div>
                                </div>
                            </div>
                            <div>
                                <div class="col-md-4">
                                    <img src="images/camilacoutinho.png" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h5>
                                        “Eu já tinha tentado usar apps concorrentes e a experiência não foi bacana. Esse achei bem simples de usar”
                                    </h5>
                                    <div class="name">Camila Amoroso / Empresária</div>
                                </div>
                            </div>

                        </div>

                        <div class="logo-footer-box">
                              <a class="logo-footer" href="#">
                                  <img src="images/footer-logo.png" alt="">
                              </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="fera-ag_container">
      <div>
        <p>feito por <a href="https://fera.ag/?utm_source=landing-zonaazul&utm_medium=referral&utm_campaign=clientes" class="brand" target="_blank"><i class="fera-ag_icon"></i>F E R A</a></p>
      </div>
    </div>

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-83159847-2', 'auto');
    ga('send', 'pageview');

    </script>

    </body>
</html>
