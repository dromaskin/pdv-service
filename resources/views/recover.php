<!DOCTYPE html>
<html>
    <head>
        <title>Parkme Zona Azul - Recuperar Senha</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
        <link rel="stylesheet" href="/bootstrap.min.css">
        <link rel="stylesheet" href="/recover.css">
        <script src="/jquery.min.js"></script>
        <script src="/recover.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="navbar-brand">
                        <div class="logo"></div>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="col-xs-10 col-xs-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form action="" method="POST">
                                <div class="form-group">
                                    <label for="password">Senha</label>
                                    <input
                                        type="password"
                                        id="password"
                                        name="password"
                                        placeholder="Senha"
                                        class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="password_confirm">Confirmar Senha</label>
                                    <input
                                        type="password"
                                        id="password_confirm"
                                        name="password_confirm"
                                        placeholder="Senha"
                                        class="form-control" />
                                </div>
                                <div class="alert hide"></div>
                                <button
                                    type="submit"
                                    disabled="disabled"
                                    class="btn btn-primary btn-block">
                                    Enviar
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
