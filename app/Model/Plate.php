<?php

namespace App\Model;

use App\Model\Base;
use App\Validate;

class Plate extends Base
{

    protected function validations()
    {
        return [
            'number' => function ($attribute, $value, $parameters, $validator) {
                return Validate::plate($value);
            }
        ];
    }

    protected function messages()
    {
        return [
            'user_id.required' => 'Usuário do cartão é obrigatório',
            'user_id.exists' => 'Usuário do cartão não encontrado',
            'number.required' => 'Número da placa é obrigatório',
            'number' => 'Número da placa é inválido'
        ];
    }

    protected function getRules()
    {
        return [
            'number' => 'required|number',
            'user_id' => 'required|exists:users,id'
        ];
    }

    protected $table = 'plates';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number',
        'user_id',
    ];

    protected $hidden = ['created_at', 'updated_at', 'active'];

    public function user() {
        return $this->belongsTo('\App\Model\User', 'user_id');
    }

    public function setNumberAttribute($value)
    {
        $this->attributes['number'] = strtoupper($value);
    }
}
