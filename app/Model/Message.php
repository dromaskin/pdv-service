<?php

namespace App\Model;

use App\Model\Base;

class Message extends Base
{

    protected function validations()
    {
        return [];
    }

    protected function messages()
    {
        return [];
    }

    protected function getRules()
    {
        return [];
    }

    protected $table = 'messages';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'text',
        'type',
        'sent_at',
        'received_at',
        'token',
        'number',
    ];
}
