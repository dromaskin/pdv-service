<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Validator;

abstract class Base extends Model
{

    protected $validator;

    protected $rules;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (method_exists($this, 'validations')) {
            if (is_array($this->validations())) {
                foreach ($this->validations() as $name => $func) {
                    if (is_callable($func)) {
                        Validator::extend($name, $func);
                    }
                }
            }
        }
    }

    abstract protected function getRules();

    public function removeRule($rule)
    {
        if (isset($this->rules[$rule])) {
            unset($this->rules[$rule]);
        }
    }

    protected function getValidator()
    {
        if (!$this->validator) {
            $messages = method_exists($this, 'messages') ? $this->messages() : [];
            $this->validator = Validator::make($this->toArray(), $this->getRules(), $messages);
        }

        return $this->validator;
    }

    public function isValid()
    {
        $validator = $this->getValidator();
        return $validator->passes();
    }

    public function errors()
    {
        return $this->getValidator()->errors();
    }
}
