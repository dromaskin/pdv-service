<?php

namespace App\Model;

use App\Model\Base;
use App\Services\StreetService;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;
use Config;

class Parking extends Base
{

    protected $rules = [
        'order_id' => 'required|exists:orders,id',
        'user_id' => 'required|exists:users,id',
        'parking_spot_id' => 'exists:parking_spots,id',
        'valid_to' => 'required|to',
        'valid_from' => 'required|from|time_available',
        'amount' => 'required|amount'
    ];

    public static function getAllowed()
    {
        return ['activated', 'scheduled'];
    }

    protected function validations()
    {
        return [
            'from' => function ($attribute, $value, $parameters, $validator) {
                $date = (new \DateTime($value))->modify('+5 seconds')->getTimestamp();
                $now = (new \DateTime())->getTimestamp();
                return $date >= $now;
            },
            'to' => function ($attribute, $value, $parameters, $validator) {
                $data = $validator->getData();

                if (isset($data['extension_id'])) {
                    return true;
                }
                $datetimeTo = (new \DateTime($data['valid_to']))->getTimestamp();
                $datetimeFrom = (new \DateTime($data['valid_from']))->getTimestamp();
                return $datetimeTo > $datetimeFrom;
            },
            'time_available' => function ($attribute, $value, $parameters, $validator) {
                // taking off the validation
                return true;
                $data = $validator->getData();
                $service = app()->make('StreetService');
                $spot = $service->getSpot($data['parking_spot_id']);

                $to = new \DateTime($data['valid_to']);
                $from = new \DateTime($data['valid_from']);
                $avaibleHours = $service->availableHoursForDate($to->getTimestamp(), $spot->street->rule->id);
                if ($avaibleHours['allowed'] && !empty($avaibleHours['rule'])) {
                    $rule = $avaibleHours['rule'];
                    return $from->format('Hi') >= ($rule['from']*100) &&
                        $to->format('Hi') <= ($rule['to']*100) &&
                        $from->format('Hi') < $to->format('Hi');
                }

                return false;
            },
            'amount' => function ($attribute, $value, $parameters, $validator) {
                return is_numeric($value) && $value > 0 && $value < 3;
            }
        ];
    }

    protected function messages()
    {
        return [
            'order_id.required' => 'Ativação da vaga - ID da compra é obrigatório',
            'order_id.exists' => 'Ativação da vaga - ID da compra não encontrado',
            'user_id.required' => 'Ativação da vaga - Usuário da vaga é obrigatório',
            'user_id.exists' => 'Ativação da vaga - Usuário da vaga não encontrado',
            'parking_spot_id.required' => 'Local da vaga é obrigatório',
            'parking_spot_id.exists' => 'Local da vaga não encontrado',
            'valid_from.required' => 'Horário de entrada da vaga obrigatório',
            'valid_to.required' => 'Horário de saída da vaga obrigatório',
            'valid_from.from' => 'Horário de entrada da vaga inválido',
            'valid_to.to' => 'Horário de saída da vaga inválido',
            'valid_from.time_available' => 'Não há necessidade ativação de CADs ' .
                'para esta quadra no período selecionado.',
            'amount' => 'Ativação da vaga - Quantidade de crédito inválida',
            'amount.required' => 'Ativação da vaga - Quantidade de crédito é obrigatória'
        ];
    }

    protected function getRules()
    {
        return $this->rules;
    }

    protected $table = 'parkings';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'extension_id',
        'order_id',
        'user_id',
        'guest_id',
        'street_id',
        'parking_spot_id',
        'hours_changed',
        'cancel_another',
        'valid_from',
        'valid_to',
        'amount',
        'active',
        'lat',
        'lon',
        'rule'
    ];



    public function getValidFrom($format)
    {
        return (new \DateTime($this->valid_from))->format($format);
    }

    public function getValidTo($format)
    {
        return (new \DateTime($this->valid_to))->format($format);
    }

    public function order()
    {
        return $this->belongsTo('\App\Model\Order', 'order_id');
    }

    public function user()
    {
        return $this->belongsTo('\App\Model\User', 'user_id');
    }

    public function spot()
    {
        return $this->belongsTo('\App\Model\ParkingSpot', 'parking_spot_id');
    }

    public function credits()
    {
        return $this->belongsToMany(
            '\App\Model\Credit',
            'parkings_credits',
            'parking_id',
            'credit_id'
        )->withTimestamps();
    }

    public static function getTimeForRule($number)
    {
        $rules = Config::get('cad.cet.rules');

        while (list($index, $rule) = each($rules)) {
            if ($rule['number'] == $number) {
                return $rule['time'];
            }
        }

        \Log::info('regra não encontrada');
        \Log::info($number);

        throw new GenericException("Regra de estacionamento não encontrada");
    }
}
