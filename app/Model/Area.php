<?php

namespace App\Model;

use App\Model\Base;

class Area extends Base
{

    protected function validations()
    {
        return [];
    }

    protected function getRules()
    {
        return [];
    }

    protected $table = 'areas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rule_id',
        'name',
        'active'
    ];

    public function rule()
    {
        return $this->belongsTo('\App\Model\Rule', 'rule_id');
    }
}
