<?php

namespace App\Model;

use App\Model\Base;
use App\Validate;

class CreditCard extends Base
{

    protected function validations()
    {
        return [
            'credit_card' => function ($attribute, $value, $parameters, $validator) {
                return Validate::creditCard($value);
            }
        ];
    }

    protected function messages()
    {
        return [
            'user_id.required' => 'Usuário do cartão é obrigatório',
            'user_id.exists' => 'Usuário do cartão não encontrado',
            'number.required' => 'Número do cartão é obrigatório',
            'number.credit_card' => 'Número do cartão inválido'
        ];
    }

    protected function getRules()
    {
        return [
            'number' => 'required|credit_card',
            'user_id' => 'required|exists:users,id'
        ];
    }

    protected $table = 'user_credit_cards';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'token',
        'last_digits',
        'name',
        'number',
        'type',
        'card_company_id'
    ];

    protected $hidden = ['created_at', 'updated_at', 'active', 'token'];

    public function user() {
        return $this->belongsTo('\App\Model\User', 'user_id');
    }

    public function setNumberAttribute($value)
    {
        $this->attributes['number'] = $value;
        $this->attributes['last_digits'] = substr($value, -4);
    }
}
