<?php

namespace App\Model;

use App\Model\Base;

class Credit extends Base
{

    protected function validations()
    {
        return [];
    }

    protected function getRules()
    {
        return [];
    }

    protected $table = 'credits';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo('\App\Model\User', 'user_id');
    }
}
