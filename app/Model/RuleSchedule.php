<?php

namespace App\Model;

use App\Model\Base;

class RuleSchedule extends Base
{

    protected function validations()
    {
        return [];
    }

    protected function getRules()
    {
        return [];
    }

    protected $table = 'rule_schedules';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rule_id',
        'days',
        'from',
        'to',
        'all_day',
        'forbidden',
        'free',
        'active',
    ];

    public function rule()
    {
        return $this->belongsTo('\App\Model\Rule', 'rule_id');
    }
}
