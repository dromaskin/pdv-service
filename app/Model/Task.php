<?php

namespace App\Model;

use App\Model\Base;

class Task extends Base
{

    protected function validations()
    {
        return [
            'type' => function ($attribute, $value, $parameters, $validator) {
                return $value == 'sms' || $value == 'ticket';
            }
        ];
    }

    protected function messages()
    {
        return [
            'target_id.required' => 'Agendamento de tarefa - alvo não definido',
            'target_id.numeric' => 'Agendamento de tarefa - alvo inválido',
            'type.required' => 'Tipo de tarefa é obrigatório',
            'type.type' => 'Tipo de tarefa inválido',
            'at_time.required' => 'Horário da tarefa é obrigatório',
            'at_time.date_format' => 'Horário da tarefa inválido'
        ];
    }

    protected function getRules()
    {
        return [
            'target_id' => 'required|numeric',
            'type' => 'required|type',
            'at_time' => 'required|date_format'
        ];
    }

    protected $table = 'tasks';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'target_id',
        'type',
        'at_time'
    ];

    public function target() {
        if ($this->type == 'sms') {
            return \App\Model\Message::find($this->target_id);
        } else {
            return \App\Model\Parking::find($this->target_id);
        }
    }
}
