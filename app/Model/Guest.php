<?php

namespace App\Model;

use App\Model\Base;

class Guest extends Base
{

    protected $rules = [
        'name' => 'required',
        'login' => 'required'
    ];

    protected function validations()
    {
        return [];
    }

    protected function messages()
    {
        return [
            'name.required' => 'Nome é obrigatório',
            'login.required' => 'Usuário é obrigatório',
        ];
    }

    protected function getRules()
    {
        return $this->rules;
    }

    protected $table = 'guests';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'login',
        'email',
        'password'
    ];

    protected $hidden = [
        'password'
    ];

    public function owner()
    {
        return $this->belongsTo('\App\Model\User', 'user_id');
    }
}
