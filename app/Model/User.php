<?php

namespace App\Model;

use App\Model\Base;
use App\Validate;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends Base implements AuthenticatableContract
{

    protected $rules = [
        'name' => 'required',
        'last_name' => 'required',
        'accept_notifications' => 'required|boolean',
        'terms' => 'required|accepted',
        'email' => 'required|email',
        'document' => 'required|document',
        'phone_number' => 'required|phone'
    ];

    use Authenticatable;

    protected function validations()
    {
        return [
            'document' => function ($attribute, $value, $parameters, $validator) {
                if (strlen($value) == 11) {
                    return Validate::cpf($value);
                } else if (strlen($value) == 14) {
                    return Validate::cnpj($value);
                }
                return false;
            },
            'phone' => function ($attribute, $value, $parameters, $validator) {
                return Validate::phoneNumber($value);
            }
        ];
    }

    protected function messages()
    {
        return [
            'name.required' => 'Nome é obrigatório',
            'terms.required' => 'Você deve aceitar os termos de uso antes de prosseguir',
            'terms.accepted' => 'Você deve aceitar os termos de uso antes de prosseguir',
            'last_name.required' => 'Último nome é obrigatório',
            'accept_notifications.required' => 'Aceito notificações é obrigatório',
            'accept_notifications.boolean' => 'Aceito notificações inválido',
            'document.required' => 'Número do documento é obrigatório',
            'document' => 'Número do documento inválido',
            'phone_number.required' => 'Número do celular é obrigatório',
            'phone_number.phone' => 'Número do celular inválido'
        ];
    }

    protected function getRules()
    {
        return $this->rules;
    }

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'email',
        'document',
        'phone_number',
        'accept_notifications',
        'terms',
        'password'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function orders() {
        return $this->hasMany('\App\Model\Order', 'user_id');
    }

    public function credits() {
        return $this->hasMany('\App\Model\Credit', 'user_id');
    }

    public function plates() {
        return $this->hasMany('\App\Model\Plate', 'user_id');
    }

    public function cards() {
        return $this->hasMany('\App\Model\CreditCard', 'user_id');
    }
}
