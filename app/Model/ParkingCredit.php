<?php

namespace App\Model;

use App\Model\Base;

class ParkingCredit extends Base
{

    protected function validations()
    {
        return [];
    }

    protected function getRules()
    {
        return [];
    }

    protected $table = 'parkings_credits';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'credit_id',
        'parking_id'
    ];

    public function credit()
    {
        return $this->belongsTo('\App\Model\Credit', 'credit_id');
    }

    public function parking()
    {
        return $this->belongsTo('\App\Model\Parking', 'parking_id');
    }
}
