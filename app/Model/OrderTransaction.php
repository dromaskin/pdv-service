<?php

namespace App\Model;

use App\Model\Base;

class OrderTransaction extends Base
{

    protected function validations()
    {
        return [

        ];
    }

    protected function messages()
    {
        return [
            'status.required' => 'Status da compra é obrigatório',
            'status.boolean' => 'Status da compra inválido',
            'order_id.required' => 'ID da compra é obrigatório',
            'order_id.exists' => 'ID da compra não encontrado'
        ];
    }

    protected function getRules()
    {
        return [
            'status' => 'required|boolean',
            'order_id' => 'required|exists:orders,id'
        ];
    }

    protected $table = 'order_transactions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'status',
        'message',
        'data',
        'type'
    ];

    public function order() {
        return $this->belongsTo('\App\Model\Order', 'order_id');
    }
}
