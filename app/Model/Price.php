<?php

namespace App\Model;

use App\Model\Base;

class Price extends Base
{

    protected function validations()
    {
        return [];
    }

    protected function getRules()
    {
        return [];
    }

    protected $table = 'prices';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'amount',
        'price',
        'order',
        'pre_charger'
    ];
}
