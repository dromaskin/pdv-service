<?php

namespace App\Model;

use App\Model\Base;
use App\Validate;

class Order extends Base
{

    protected $rules = [
        'price_id' => 'required|exists:prices,id',
        'user_id' => 'required|exists:users,id',
        'plate_id' => 'required|exists:plates,id',
        'user_credit_card_id' => 'exists:user_credit_cards,id',
        'payment_method_id' => 'required|exists:payment_methods,id',
        'amount' => 'required|amount',
        'device_id' => 'required|udid'
    ];

    protected function validations()
    {
        return [
            'amount' => function ($attribute, $value, $parameters, $validator) {
                // credito empresa
                return is_numeric($value) && $value > 0 && ($value < 11 || app()->runningInConsole() || config('app.is_admin', false));
            },
            'udid' => function ($attribute, $value, $parameters, $validator) {
                return Validate::UDID($value);
            }
        ];
    }

    protected function messages()
    {
        return [
            'price_id.required' => 'Valor é obrigatório para compra',
            'price_id.exists' => 'Valor não encontrado na compra',
            'user_id.required' => 'Usuário é obrigatório para compra',
            'user_id.exists' => 'Usuário não encontrado na compra',
            'plate_id.required' => 'Número da placa é obrigatório para compra',
            'plate_id.exists' => 'Número da placa não encontrado na compra',
            'payment_method_id.required' => 'Forma de pagamento é obrigatória',
            'payment_method_id.exists' => 'Forma de pagamento não encontrada',
            'user_credit_card_id.exists' => 'Cartão de crédito não encontrado na compra',
            'amount' => 'Quantidade de crédito inválida',
            'amount.required' => 'Quantidade de crédito é obrigatória',
            'device_id.required' => 'Identificador do dispositivo é obrigatório',
            'device_id.udid' => 'Identificador do dispositivo inválido',
        ];
    }

    protected function getRules()
    {
        return $this->rules;
    }

    protected $table = 'orders';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price_id',
        'user_id',
        'plate_id',
        'user_credit_card_id',
        'payment_method_id',
        'amount',
        'device_id'
    ];

    public function user() {
        return $this->belongsTo('\App\Model\User', 'user_id');
    }

    public function plate() {
        return $this->belongsTo('\App\Model\Plate', 'plate_id');
    }

    public function price() {
        return $this->belongsTo('\App\Model\Price', 'price_id');
    }

    public function method() {
        return $this->belongsTo('\App\Model\PaymentMethod', 'payment_method_id');
    }

    public function creditCard()
    {
        return $this->belongsTo('\App\Model\CreditCard', 'user_credit_card_id');
    }

    public function parking()
    {
        return $this->hasOne('\App\Model\Parking', 'order_id');
    }

    public function parkings()
    {
        return $this->hasMany('\App\Model\Parking', 'order_id');
    }

    public function transactions()
    {
        return $this->hasMany('\App\Model\OrderTransaction', 'order_id');
    }
}
