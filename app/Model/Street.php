<?php

namespace App\Model;

use App\Model\Base;

class Street extends Base
{

    protected function validations()
    {
        return [];
    }

    protected function getRules()
    {
        return [];
    }

    protected $table = 'streets';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rule_id',
        'area_id',
        'sector',
        'face',
        'name',
        'short_name',
        'lat',
        'lon',
        'active'
    ];

    public function rule()
    {
        return $this->belongsTo('\App\Model\Rule', 'rule_id');
    }

    public function area()
    {
        return $this->belongsTo('\App\Model\Area', 'area_id');
    }

    public function spots() {
        return $this->hasMany('\App\Model\ParkingSpot', 'street_id');
    }
}
