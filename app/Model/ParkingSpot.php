<?php

namespace App\Model;

use App\Model\Base;

class ParkingSpot extends Base
{

    protected function validations()
    {
        return [];
    }

    protected function getRules()
    {
        return [];
    }

    protected $table = 'parking_spots';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parking_type_id',
        'street_id',
        'amount'
    ];

    public function street()
    {
        return $this->belongsTo('\App\Model\Street', 'street_id');
    }

    public function type()
    {
        return $this->belongsTo('\App\Model\ParkingType', 'parking_type_id');
    }
}
