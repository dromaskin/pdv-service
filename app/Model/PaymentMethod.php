<?php

namespace App\Model;

use App\Model\Base;

class PaymentMethod extends Base
{

    protected function validations()
    {
        return [];
    }

    protected function messages()
    {
        return [];
    }

    protected function getRules()
    {
        return [];
    }

    protected $table = 'payment_methods';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'style',
        'order'
    ];

    public $hidden = ['created_at', 'updated_at'];
}
