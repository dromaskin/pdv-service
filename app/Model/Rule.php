<?php

namespace App\Model;

use App\Model\Base;

class Rule extends Base
{

    protected function validations()
    {
        return [];
    }

    protected function getRules()
    {
        return [];
    }

    protected $table = 'rules';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'max_time',
        'active'
    ];

    public function schedules()
    {
        return $this->hasMany('\App\Model\RuleSchedule', 'rule_id');
    }
}
