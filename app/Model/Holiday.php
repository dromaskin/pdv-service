<?php

namespace App\Model;

use App\Model\Base;

class Holiday extends Base
{

    protected function validations()
    {
        return [];
    }

    protected function getRules()
    {
        return [];
    }

    protected $table = 'holidays';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'name'
    ];
}
