<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\PurchaseCadService;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;
use ApiResponse;

class AdminController extends BaseController
{

    public function purchaseCad(PurchaseCadService $service, Request $request)
    {
        if (!$request->has('document') || !$request->has('amount')) {
            return ApiResponse::setAsFail()
                ->setStatusMessage('Dados inválidos')
                ->get();
        }

        try {
            $service->handle(
                $request->input('document'),
                $request->input('amount')
            );
        } catch (GenericException $e) {
            ApiResponse::setAsFail()
                ->setStatusMessage($e->getMessage());
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()
                ->setStatusMessage($e->getMessages());
        } catch (\Exception $e) {
            ApiResponse::setAsFail()
                ->setStatusMessage("Erro ao comprar cads");
        }

        return ApiResponse::get();
    }
}
