<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\MessageService;
use App\Exceptions\GenericException;
use ApiResponse;
use Log;

class MessagesController extends BaseController
{

    public $message;

    public function __construct(MessageService $message)
    {
        $this->message = $message;
    }

    public function receive(Request $request)
    {
        try {
            $this->message->receive($request->input());
        } catch (\Exception $e) {
            Log::error('error receiving sms');
            Log::error($e->getMessage());
            Log::error($e);
        }

        return '';
    }

    public function schedule(Request $request)
    {
        if (!$request->has('order') || !$request->has('type')) {
            return ApiResponse::setAsFail()
                ->setStatusMessage('Dados inválidos')
                ->get();
        }

        try {
            $response = $this->message->scheduleParkingMessage(
                $request->input('order'),
                $request->input('type')
            );
        } catch (GenericException $e) {
            ApiResponse::setAsFail()
                ->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error('error scheduling message');
            Log::error($e);
            ApiResponse::setAsFail()
                ->setStatusMessage("Erro ao agendar mensagem");
        }

        return ApiResponse::get();
    }
}
