<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Cet\Date;
use App\Cet\Methods\Balance;
use App\Cet\Methods\Time;
use App\Cet\Methods\Unlock;
use App\Cet\Methods\Activate;
use App\Cet\Methods\ActivateUnlock;
use ApiResponse;

class CadController extends BaseController
{

    public function balance(Balance $balance)
    {
        try {
            $result = $balance->get();
            ApiResponse::setPayload($result);
        } catch (\App\Cet\Exceptions\ConnectException $e) {
            ApiResponse::setPayload($e->getMessage());
        } catch (\App\Cet\Exceptions\InvalidException $e) {
            ApiResponse::setPayload($e->getMessage());
        } catch (\Exception $e) {
            ApiResponse::setPayload($e->getMessage());
        }

        return ApiResponse::get();
    }

    public function time(Time $time)
    {
        try {
            $result = $time->get();
            ApiResponse::setPayload([
                'remote' => $result,
                'local' => Date::get()
            ]);
        } catch (\App\Cet\Exceptions\ConnectException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\App\Cet\Exceptions\InvalidException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        }

        return ApiResponse::get();
    }

    public function unlock(Unlock $unlock, Request $request)
    {
        try {
            $result = $unlock->send($request->input());
            ApiResponse::setPayload($result);
        } catch (\App\Cet\Exceptions\ConnectException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\App\Cet\Exceptions\InvalidException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        }

        return ApiResponse::get();
    }

    public function activate(Activate $activate, Request $request)
    {
        try {
            $result = $activate->send($request->input());
            ApiResponse::setPayload($result);
        } catch (\App\Cet\Exceptions\ConnectException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\App\Cet\Exceptions\InvalidException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        }

        return ApiResponse::get();
    }

    public function activateUnlock(ActivateUnlock $activate, Request $request)
    {
        try {
            $result = $activate->send($request->input());
            ApiResponse::setPayload($result);
        } catch (\App\Cet\Exceptions\ConnectException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\App\Cet\Exceptions\InvalidException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        }

        return ApiResponse::get();
    }
}
