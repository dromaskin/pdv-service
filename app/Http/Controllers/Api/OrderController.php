<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;
use ApiResponse;
use Log;
use DB;

class OrderController extends BaseController
{

    public $order;

    public function __construct(OrderService $order)
    {
        $this->order = $order;
    }

    public function create(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $this->order->create($request);
            if ($data === false) {
                ApiResponse::setAsFail()
                    ->setStatusMessage('Erro ao concluir compra. Tente novamente');
            } else {
                ApiResponse::setPayload($data);
            }
            DB::commit();
        } catch (ValidationException $e) {
            DB::rollBack();
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            DB::rollBack();
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error('error creating order');
            Log::error($e->getMessage());
            Log::error($e);
            DB::rollBack();
            ApiResponse::setAsFail()->setStatusMessage("Erro ao concluir compra");
        }

        return ApiResponse::get();
    }
}
