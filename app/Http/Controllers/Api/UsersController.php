<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\UserService as User;
use App\Repository\UserRepository as Repository;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;
use ApiResponse;
use App\Messages;
use Log;

class UsersController extends BaseController
{

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function create(Request $request)
    {
        if (!$request->has('user')) {
            return ApiResponse::setAsFail()->setStatusMessage("Informe os dados do usuário")->get();
        }

        try {
            $user = $this->user->create($request->input('user'));
            ApiResponse::setPayload($user);
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage('Erro ao cadastrar usuário');
        }

        return ApiResponse::get();
    }

    public function login(Request $request)
    {
        if ($request->has('email') && $request->has('password')) {
            $data = $this->user->login($request->input('email'), $request->input('password'));
            if ($data) {
                ApiResponse::setPayload($data);
            } else {
                ApiResponse::setAsFail()->setStatusMessage('Usuário e senha não conferem');
            }
        } else {
            ApiResponse::setAsFail()->setStatusMessage(Messages::INVALID_DATA);
        }

        return ApiResponse::get();
    }

    public function logout()
    {
        $logout = $this->user->logout();
        return ['user' => $logout];
    }

    public function get()
    {
        $userInfo = $this->user->getInfo();
        return ApiResponse::setPayload($userInfo)->get();
    }

    public function orders(Request $request)
    {
        $data = $this->user->getOrders($request->input('period', 'all'));
        return ApiResponse::setPayload($data)->get();
    }

    public function order($id)
    {
        $data = $this->user->getOrder($id);
        return ApiResponse::setPayload($data)->get();
    }

    public function recover(Request $request)
    {
        if (!$request->has('email')) {
            return ApiResponse::setAsFail()->setStatusMessage("Informe o email")->get();
        }

        try {
            $user = $this->user->recoverEmail($request->input('email'));
            ApiResponse::setPayload($user);
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error('Recover user password');
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro ao enviar email de recuperação");
        }

        return ApiResponse::get();
    }

    public function sendRecover(Request $request)
    {
        if (!$request->has('password') || !$request->has('token')) {
            return ApiResponse::setAsFail()
                ->setStatusMessage("Dados inválidos")
                ->get();
        }

        try {
            $user = $this->user->updatePassword($request->input('password'), $request->input('token'));
            ApiResponse::setPayload($user);
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error('Recover user password');
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro ao enviar email de recuperação");
        }

        return ApiResponse::get();
    }

    public function search(Request $request, Repository $repository)
    {
        try {
            ApiResponse::setPayload($repository->search($request->input('q')));
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error('Recover user password');
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro buscar usuário");
        }

        return ApiResponse::get();
    }
}
