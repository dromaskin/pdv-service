<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\UserService as User;
use App\Services\CreditCardService as CreditCard;
use App\Services\PaymentService as Payment;
use App\Services\TicketService as Ticket;
use App\Services\OrderService as Order;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;
use App\Messages;
use ApiResponse;
use Log;

class ServicesController extends BaseController
{

    public function user(Request $request, User $service)
    {
        if (!$request->has('user')) {
            return ApiResponse::setAsFail()->setStatusMessage("Informe os dados do usuário")->get();
        }

        try {
            $user = $service->save($request->input('user'));
            ApiResponse::setPayload($user);
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage('Erro ao fazer login');
        }

        return ApiResponse::get();
    }

    public function creditCard(Request $request, CreditCard $service)
    {
        if (!$request->has('credit_card')) {
            return ApiResponse::setAsFail()->setStatusMessage("Informe os dados do cartão")->get();
        }

        try {
            $creditCard = $service->create($request->input('credit_card'));
            ApiResponse::setPayload($creditCard);
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro ao ativar vaga");
        }

        return ApiResponse::get();
    }

    public function order(Request $request, Order $service)
    {
        if (!$request->has('order')) {
            return ApiResponse::setAsFail()->setStatusMessage("Informe os dados da compra")->get();
        }

        try {
            $order = $service->create($request->input('order'));
            ApiResponse::setPayload($order);
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro ao ativar vaga");
        }

        return ApiResponse::get();
    }

    public function payment(Request $request, Payment $service)
    {
        if (!$request->has('payment')) {
            return ApiResponse::setAsFail()->setStatusMessage("Informe os dados do pagamento")->get();
        }

        try {
            $payment = $service->send($request->input('payment'));
            ApiResponse::setPayload($payment);
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro validar dados de pagamento");
        }

        return ApiResponse::get();
    }

    public function ticket(Request $request, Ticket $service)
    {
        if (!$request->has('ticket')) {
            return ApiResponse::setAsFail()->setStatusMessage("Informe os dados da vaga")->get();
        }

        try {
            $ticket = $service->create($request->input('ticket'));
            ApiResponse::setPayload($ticket);
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro ao ativar vaga");
        }

        return ApiResponse::get();
    }

    public function userPlate(Request $request, User $service)
    {
        if (!$request->has('plate') || !$request->has('user')) {
            return ApiResponse::setAsFail()->setStatusMessage("Informe o número da placa")->get();
        }

        try {
            $plate = $service->getUserPlate($request->input('user'), $request->input('plate'));
            ApiResponse::setPayload($plate);
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro ao ativar vaga");
        }

        return ApiResponse::get();
    }

    public function removePlate(Request $request, User $service)
    {
        if (!$request->has('plate')) {
            return ApiResponse::setAsFail()->setStatusMessage("Informe o número da placa")->get();
        }

        try {
            $plate = $service->removePlate($request->input('plate'));
            ApiResponse::setPayload($plate);
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro ao remover placa");
        }

        return ApiResponse::get();
    }

    public function validateTicket(Request $request, Ticket $service)
    {
        if (!$request->has('data') || !$request->has('data.order') || !$request->has('data.ticket')) {
            return ApiResponse::setAsFail()->setStatusMessage("Informe os dados da vaga")->get();
        }

        try {
            $validate = $service->validate($request->input('data'));
            ApiResponse::setPayload($validate);
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro ao ativar vaga");
        }

        return ApiResponse::get();
    }

    public function refund(Request $request, Payment $service)
    {
        if (!$request->has('order')) {
            return ApiResponse::setAsFail()->setStatusMessage("Informe os dados da compra")->get();
        }

        try {
            $payment = $service->refund($request->input('order'));
            ApiResponse::setPayload($payment);
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro enviar estorno de pagamento");
        }

        return ApiResponse::get();
    }

    public function updateUser(Request $request, User $service)
    {
        if (!$request->has('user')) {
            return ApiResponse::setAsFail()
                ->setStatusMessage("Informe os dados do usuário")
                ->get();
        }

        try {
            $user = $service->update($request->input('user'));
            ApiResponse::setPayload($user);
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro ao atualizar dados do usuário");
        }

        return ApiResponse::get();
    }

    public function active(Request $request, User $service)
    {
        if (!$request->has('order')) {
            return ApiResponse::setAsFail()
                ->setStatusMessage("Informe os dados do estacionamento")
                ->get();
        }

        try {
            $data = $service->getActiveParking($request->input('order'));
            ApiResponse::setPayload($data);
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro buscar dados da vaga");
        }

        return ApiResponse::get();
    }

    public function cancelScheduled(Request $request, Ticket $service)
    {
        if (!$request->has('order')) {
            return ApiResponse::setAsFail()
                ->setStatusMessage("Informe os dados da vaga")
                ->get();
        }

        try {
            $order = $service->cancelScheduled($request->input('order'));
            ApiResponse::setPayload($order);
        } catch (ValidationException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessages());
        } catch (GenericException $e) {
            ApiResponse::setAsFail()->setStatusMessage($e->getMessage());
        } catch (\Exception $e) {
            Log::error($e);
            ApiResponse::setAsFail()->setStatusMessage("Erro ao cancelar vaga");
        }

        return ApiResponse::get();
    }
}
