<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\StreetService as Service;
use App\Services\UserService as User;
use App\Exceptions\GenericException;
use ApiResponse;
use Log;

class StreetController extends BaseController
{

    public $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function load(User $user, $code, Request $request)
    {
        try {
            $meter = $request->input('meter', '');
            $data = $this->service->loadFromCode($code, $meter);
            $data['user_info'] = $user->getInfo();
            ApiResponse::setPayload($data);
        } catch (\Exception $e) {
            Log::error("error loading {$code}");
            Log::error($e);
            ApiResponse::setAsFail()
                ->setStatusMessage("Erro ao buscar dados");
        }
        return ApiResponse::get();
    }

    public function hoursByDate(Request $request)
    {
        if (!$request->input('date') || !$request->input('rule')) {
            ApiResponse::setAsFail()->setStatusMessage('Dados inválidos');
        } else {
            try {
                $data = $this->service->availableHoursForDate(
                    $request->input('date'),
                    $request->input('rule')
                );
                ApiResponse::setPayload($data);
            } catch (GenericException $e) {
                ApiResponse::setAsFail()
                    ->setStatusMessage($e->getMessage());
            } catch (\Exception $e) {
                ApiResponse::setAsFail()
                    ->setStatusMessage($e->getMessage());
            }
        }

        return ApiResponse::get();
    }
}
