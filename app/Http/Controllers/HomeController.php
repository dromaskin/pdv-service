<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\StreetService as Service;
use App\Services\UserService as User;
use App\Services\PaymentService as Payment;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;
use Session;

class HomeController extends BaseController
{

    protected $service;
    protected $user;
    protected $payment;

    public function __construct(Service $service, User $user, Payment $payment)
    {
        $this->service = $service;
        $this->user = $user;
        $this->payment = $payment;
    }

    public function index(Request $request)
    {
        // $allowedIps = ['177.148.203.221', '172.16.28.153', '172.16.28.249',
        //     '172.16.28.186', '177.69.207.129', '186.203.6.35',
        //     '191.183.101.12', '186.231.133.176', '177.22.136.204', '177.114.75.8'];
        //
        // if (app()->environment() == 'production' && !in_array($_SERVER['REMOTE_ADDR'], $allowedIps)) {
        //     return view('soon');
        // }

        $userData = array_only($request->input(), [
            'email',
            'first_name',
            'last_name',
            'clientID',
            'lat',
            'lng'
        ]);
        $userLogged = false;

        if (!empty($userData)) {
            if ($request->has('email')) {
                if ($this->user->loginByEmail($request->input('email'))) {
                    $userLogged = true;
                }
            }

            if ($request->has('lng')) {
                $userData['lon'] = $userData['lng'];
                unset($userData['lng']);
            }

            if ($request->has('clientID')) {
                if (strrpos($userData['clientID'], 'meter_id') !== false) {
                    $userData['clientID'] = substr(
                        $userData['clientID'],
                        0,
                        strrpos($userData['clientID'], 'meter_id')
                    );
                }

                $userData['device_id'] = preg_replace('/[^0-9aA-zZ]/', '', $userData['clientID']);
                unset($userData['clientID']);
            }

            if (!$userLogged) {
                if ($request->has('first_name')) {
                    $userData['name'] = $userData['first_name'];
                    unset($userData['first_name']);
                }
            }
        }

        if (!isset($userData['device_id'])) {
            $userData['device_id'] = str_random(41);
        }

        Session::put('user', $userData);

        return view('web-app');
    }
}
