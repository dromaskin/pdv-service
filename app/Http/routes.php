<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/zona-azul', function () {
    return view('landing');
});

Route::get('/zona-azul-sao-paulo', function () {
    return view('landing');
});

Route::get('/nao-permitido', function () {
    return 'Acesso negado';
});

Route::get('/recuperar-senha', function () {
    return view('recover');
});

Route::get('/mario-us', function () {
    return view('users');
});

Route::get('/', function () {
    return redirect()->away(\Config::get('cad.parkme.site_url'));
});

Route::get('/saopaulo', ['uses' => 'HomeController@index']);
Route::get('/saopaulo/termos-de-uso', function () {
    return view('termos');
});

Route::get('/sms', function () {
    $message = new App\InfoPib\Message();
    try {
        return ['response' => $message->send('11971070187', 'Inferno')];
    } catch (\App\Exceptions\GenericException $e) {
        return $e->getMessage();
    } catch (\Exception $e) {
        return $e->getMessage();
    }
});

Route::any('/sms/receive', ['uses' => 'Api\MessagesController@receive']);
Route::get('/balance', ['uses' => 'Api\CadController@balance']);
Route::get('/time', ['uses' => 'Api\CadController@time']);

Route::group(['prefix' => 'api'], function () {

    Route::get('/user-info', ['uses' => 'Api\UsersController@get']);
    Route::get('/user/search', ['uses' => 'Api\UsersController@search']);
    Route::post('/new-user', ['uses' => 'Api\UsersController@create']);
    Route::post('/login', ['uses' => 'Api\UsersController@login']);
    Route::post('/recover', ['uses' => 'Api\UsersController@recover']);
    Route::post('/update-password', ['uses' => 'Api\UsersController@sendRecover']);
    Route::any('/logout', ['uses' => 'Api\UsersController@logout']);
    Route::post('/user', ['uses' => 'Api\ServicesController@user']);
    Route::post('/user-plate', ['uses' => 'Api\ServicesController@userPlate']);
    Route::post('/remove-plate', ['uses' => 'Api\ServicesController@removePlate']);
    Route::post('/update-user', ['uses' => 'Api\ServicesController@updateUser']);
    Route::post('/credit-card', ['uses' => 'Api\ServicesController@creditCard']);
    Route::post('/order', ['uses' => 'Api\ServicesController@order']);
    Route::post('/payment', ['uses' => 'Api\ServicesController@payment']);
    Route::post('/refund', ['uses' => 'Api\ServicesController@refund']);
    Route::post('/ticket', ['uses' => 'Api\ServicesController@ticket']);
    Route::post('/ticket/validate', ['uses' => 'Api\ServicesController@validateTicket']);
    Route::get('/{code}', ['uses' => 'Api\StreetController@load']);
    Route::post('/hours', ['uses' => 'Api\StreetController@hoursByDate']);
    Route::post('/active', ['uses' => 'Api\ServicesController@active']);
    Route::post('/cancel-scheduled', ['uses' => 'Api\ServicesController@cancelScheduled']);
    Route::post('/schedule-message', ['uses' => 'Api\MessagesController@schedule']);
    Route::get('/orders', ['uses' => 'Api\UsersController@orders']);
    Route::get('/order/{id}', ['uses' => 'Api\UsersController@order']);

    Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function () {
        Route::post('/purchase-cad', ['uses' => 'Api\AdminController@purchaseCad']);
    });
});
