<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class CheckHeaderAuthentication
{

    public function handle($request, Closure $next)
    {
        if ($request->header('Authorization')) {
            $token = $request->header('Authorization');
            $user = $this->checkUser($token);

            if ($user) {
                Auth::login($user);
                $request->session()->put('user.guest', false);
            } else {
                $guest = $this->checkGuest($token);
                if ($guest) {
                    Auth::login($guest->owner);
                    $request->session()->put('user.guest', $guest->toArray());
                }
            }
        }

        return $next($request);
    }

    protected function checkUser($token)
    {
        $repository = app()->make('\App\Repository\UserRepository');
        $user = $repository->getByToken($token);
        return $user;
    }

    protected function checkGuest($token)
    {
        $repository = app()->make('\App\Repository\GuestRepository');
        $guest = $repository->getByToken($token);
        return $guest;
    }
}
