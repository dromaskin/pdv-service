<?php

namespace App\Http\Middleware;

use Closure;
use Hash;
use Config;

class ValidateAdmin
{

    public function handle($request, Closure $next)
    {
        if ($request->has('secret')) {
            if (Hash::check(Config::get('cad.admin.secret'), $request->input('secret'))) {
                config(['app.is_admin' => true]);
                return $next($request);
            }
        }

        return response('Unauthorized.', 401);
    }
}
