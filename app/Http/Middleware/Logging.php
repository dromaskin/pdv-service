<?php

namespace App\Http\Middleware;

use Closure;
use App;

class Logging {

    protected $time;

    protected function isEnabled()
    {
        return config('app.debug') && !(!config('app.debug_console') && app()->runningInConsole());
    }

    public function handle($request, Closure $next)
    {
        if ($this->isEnabled()) {

        }

        if (config('app.debug')) {

            if (!config('app.debug_console') && app()->runningInConsole()) {
                return;
            }

            $this->time = microtime(true);
            app()->terminating(function () use ($timeStart) {
                $diff = microtime(true) - $timeStart;
                $sec = intval($diff);
                $micro = $diff - $sec;
                Log::info("execution time is {$micro}");
            });

            if (config('database.debug')) {
                DB::enableQueryLog();
                app()->terminating(function () use ($timeStart) {
                    Log::info('query logs');
                    Log::info(DB::getQueryLog());
                });
            }
        }
    }

    public function terminate($request, $response)
    {

    }
}
