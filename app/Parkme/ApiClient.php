<?php

namespace App\Parkme;

use GuzzleHttp\Client as Guzzle;
use Config;

class ApiClient {

    static protected $client;

    public static function instance()
    {
        if (!self::$client) {
            self::$client = new Guzzle([
                'base_uri' => Config::get('cad.parkme.api_url')
            ]);
        }
        return self::$client;
    }

}
