<?php

namespace App\Parkme;

use App\Parkme\ApiTrait as Api;

abstract class ApiAbstract {

    use Api;

    abstract public function getEndpoint();
}
