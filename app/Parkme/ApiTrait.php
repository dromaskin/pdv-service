<?php

namespace App\Parkme;

use GuzzleHttp\Exception\RequestException;
use App\Parkme\ApiClient as Client;

trait ApiTrait {

    protected $clientData = [];

    public function getData()
    {
        return $this->clientData;
    }

    public function setData($key, $value)
    {
        $this->clientData[$key] = $value;
    }

    public function setAuth($user, $password)
    {
        $this->setData('auth', [$user, $password]);
    }

    protected function client()
    {
        return Client::instance();
    }

    public function post($data)
    {
        return $this->call('POST');
    }

    public function get()
    {
        return $this->call('GET');
    }

    protected function call($method = 'GET')
    {
        try {
            $res = $this->client()->request($method, $this->getEndpoint(), $this->getData());
            return $res->getBody()->getContents();
        } catch (RequestException $e) {
            throw new \Exception("Erro ao cnectar ao servidor");
        }
    }
}
