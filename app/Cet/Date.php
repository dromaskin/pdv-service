<?php

namespace App\Cet;

class Date {

    public static function get()
    {
        $date = new \DateTime();
        return $date->format('Y-m-d\TH:i:s');
    }
}
