<?php

namespace App\Cet\Methods;

interface CadInterface {

    function normalize(&$params);

    function validate($params);

    function transform($params);
}
