<?php

namespace App\Cet\Methods;

use Log;
use App\Cet\Api;
use App\Cet\Exceptions\InvalidException;
use GuzzleHttp\Client as Guzzle;

class Time extends Api {

    protected $method = 'consultarRelogio';

    public function get()
    {
        $result = $this->connect($this->method)->call($this->method);
        if (array_key_exists(0, $result)) {
            return (string) $result[0];
        } else {
            Log::error("Balance invalid response");
            Log::error($result);
            throw new InvalidException("Horário inválido");
        }
    }
}
