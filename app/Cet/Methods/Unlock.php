<?php

namespace App\Cet\Methods;

use App\Cet\Methods\CadInterface;
use App\Cet\Api;
use App\Cet\Date;
use App\Cet\Xml;
use GuzzleHttp\Client as Guzzle;
use App\Cet\Exceptions\InvalidException;
use App\Cet\Validator\Unlock as Validator;
use App\Cet\Hydrator;
use Log;

class Unlock extends Api implements CadInterface {

    protected $method = 'processarTransacao';
    protected $validator;
    protected $hydrator;

    public function __construct()
    {
        parent::__construct();
        $this->validator = new Validator();
        $this->hydrator = new Hydrator();
    }

    protected function defaultParams()
    {
        return [
            'codigoDistribuidor' => $this->code,
            'dataEnvio' => Date::get(),
            'tipo' => 1
        ];
    }

    public function normalize(&$params)
    {
        $params = array_only($params, $this->validator->params());
    }

    public function transform($params)
    {
        $hydrated = $this->hydrator->hydrate($params);

        $merge = array_merge($this->defaultParams(), $hydrated);

        return [
            'codigoDistribuidor' => $this->code,
            'transacaoXml' => Xml::toString([
                'transacao' => $merge
            ])
        ];
    }

    public function validate($params)
    {
        $this->validator->setParams($params);
        if (!$this->validator->validate()) {
            Log::error('Unlock invalid');
            Log::info($params);
            Log::info($this->validator->getErrors());
            throw new InvalidException('Dados inválidos');
        }
        return true;
    }

    public function send(array $params)
    {
        $this->normalize($params);
        $this->validate($params);
        $params = $this->transform($params);
        $result = $this->connect()->call($this->method, $params);
        if (array_key_exists('sucesso', $result)) {
            if ($result['sucesso'] == 'true') {
                $result['params'] = $params;
                return $result;
            } else if (!empty($result['mensagem'])) {
                throw new InvalidException($result['mensagem']);
            } else {
                Log::info('Unlock cad empty message');
                Log::info($params);
                Log::info($result);
                throw new InvalidException('Erro ao enviar requisição');
            }
        } else {
            Log::error('Unlock cad error');
            Log::error($params);
            Log::error($result);
            throw new InvalidException('Erro ao verificar serviço CET');
        }
    }
}
