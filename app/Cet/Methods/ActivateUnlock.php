<?php

namespace App\Cet\Methods;

use App\Cet\Methods\Activate;
use App\Cet\Date;
use App\Cet\Validator\ActivateUnlock as Validator;
use App\Cet\Hydrator;

class ActivateUnlock extends Activate {

    protected $method = 'processarTransacao';

    protected function defaultParams()
    {
        return [
            'codigoDistribuidor' => $this->code,
            'dataEnvio' => Date::get(),
            'tipo' => 3
        ];
    }
}
