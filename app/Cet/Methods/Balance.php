<?php

namespace App\Cet\Methods;

use App\Cet\Api;
use App\Cet\Exceptions\InvalidException;
use Log;

class Balance extends Api {

    protected $method = 'consultarSaldoCorrente';

    public function get()
    {
        $result = $this->connect()->call($this->method, [
            'codigoDistribuidor' => $this->code
        ]);
        if (array_key_exists('dataSaldo', $result)) {
            if (!empty($result['mensagem'])) {
                Log::info("Balance error");
                Log::info($result);
                throw new InvalidException("Erro ao verificar saldo");
            } else {
                return $result;
            }
        } else {
            Log::error("Balance invalid response");
            Log::error($result);
            throw new InvalidException("Erro ao validar saldo");
        }
    }
}
