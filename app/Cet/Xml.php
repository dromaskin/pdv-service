<?php

namespace App\Cet;

class Xml {
    public static function toString(array $params, $root = null)
    {
        $xml = '';

        if ($root) {
            $xml.= "<{$root}>";
        }

        while(list($key, $value) = each($params)) {
            if (is_array($value)) {
                $xml.= self::toString($value, $key);
            } else {
                $xml.= "<{$key}>{$value}</{$key}>";
            }
        }

        if ($root) {
            $xml.= "</{$root}>";
        }

        return $xml;
    }

    public static function toArray($object)
    {
        $array = [];
        $object = (array) $object;

        foreach ($object as $key => $value) {

            if (is_object($value)) {
                $value = self::toArray($value);
            }

            $array[$key] = empty($value) ? '' : $value;
        }
        return $array;
    }
}
