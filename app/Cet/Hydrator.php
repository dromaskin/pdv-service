<?php

namespace App\Cet;

class Hydrator {

    protected $hydrated = [];

    protected $fields = [
        'idTransacaoDistribuidor' => 'transactionID',
        ['documentType', 'document'],
        ['deviceIDType', 'deviceID'],
        'area' => 'area',
        'setor' => 'sector',
        'face' => 'side',
        'latitude' => 'lat',
        'longitude' => 'lon',
        'placa' => 'plate',
        'tempoCartao' => 'time',
        'quantidadeCartoes' => 'amount'
    ];

    public function hydrate($params)
    {
        while (list($key, $value) = each($this->fields)) {
            if (is_array($value)) {
                list($key, $v) = $value;
                $value = $v;
                $key = isset($params[$key]) ? $params[$key] : null;
            }

            if (isset($params[$value])) {
                $this->hydrated[$key] = $params[$value];
            }
        }
        return $this->hydrated;
    }
}
