<?php

namespace App\Cet;

use Config;
use Log;
use GuzzleHttp\Client as Guzzle;
use App\Cet\Client;
use App\Cet\Exceptions\ConnectException;
use App\Cet\Exceptions\CallException;

class Api {

    protected $url;
    protected $certificate;
    protected $pass;
    protected $client;
    protected $code;
    protected $name;

    public function __construct()
    {
        $config = Config::get('cad.cet');
        $this->url = $config['api_url'];
        $this->pass = $config['password'];
        $this->certificate = $config['certificate'];
        $this->code = $config['cod_publisher'];
        $this->name = $config['name_publisher'];
    }

    protected function getOptions()
    {
        return [
            'curl' => [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_USERAGENT => 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)',
                CURLOPT_VERBOSE => true,
                CURLOPT_SSLCERT => $this->certificate,
                CURLOPT_SSLCERTPASSWD => $this->pass,
            ],
            'cert' => [$this->certificate, $this->pass]
        ];
    }

    public function connect()
    {
        try {
            $this->client = new Guzzle($this->getOptions());
            return $this;
        } catch (\Exception $e) {
            Log::error("CET connection failed");
            Log::error("Message: {$e->getMessage()}");
            Log::error($e);
            throw new ConnectException("Erro ao conectar ao serviço da CET");
        }
    }

    public function getUrl($method = null)
    {
        return $this->url;
    }

    public function getCertificate()
    {
        return $this->certificate;
    }

    public function getPassword()
    {
        return $this->pass;
    }

    public function call($method, $params = [])
    {
        try {
            $result = $this->client->post("{$this->getUrl()}/$method", [
                'form_params' => $params
            ]);
            if ($result) {
                $object = simplexml_load_string($result->getBody()->getContents());
                return Xml::toArray($object);
            }
            return false;
        } catch (\Exception $e) {
            Log::error("CET call failed");
            Log::error($params);
            Log::error("Message: {$e->getMessage()}");
            Log::error($e);
            throw new CallException("Erro ao executar serviço CET");
        }
    }
}
