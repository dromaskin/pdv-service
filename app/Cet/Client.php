<?php

namespace App\Cet;

use SoapClient;
use Log;

class Client extends SoapClient {

    protected function getMethod($action) {
        if (strrpos($action, '/') !== false) {
            $parts = explode('/', $action);
            return array_pop($parts);
        }
        return false;
    }

    protected function transformRequest($request, $action)
    {
        $method = $this->getMethod($action);
        if ($method !== false) {
            $request = str_replace("<{$method}>", "<{$method} xmlns=\"http://apphml.cetsp.com.br/\">", $request);
        }

        $request = str_replace('env:', 'soap12:', $request);
        $request = str_replace(':env', ':soap12', $request);
        $request = str_replace('<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns1="http://apphml.cetsp.com.br/">', '<soap:Envelope>', $request);
        $request = str_replace('<soap:Envelope', '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"', $request);

        return $request;
    }

    public function __doRequest($request, $location, $action, $version, $one_way = null) {
        $request = $this->transformRequest($request, $action);

        Log::info('Request');
        Log::info($request);
        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }

}
