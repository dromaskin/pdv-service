<?php

namespace App\Cet\Validator;

use App\Cet\Validator\AbstractValidator as Base;
use App\Validate;

class Activate extends Base {

    protected function requiredParams()
    {
        return [
            'transactionID',
            'documentType',
            'document',
            'deviceIDType',
            'plate',
            'time',
            'amount'
        ];
    }

    public function params()
    {
        return [
            'transactionID',
            'documentType',
            'document',
            'deviceIDType',
            'deviceID',
            'area',
            'sector',
            'side',
            'lat',
            'lon',
            'plate',
            'time',
            'amount'
        ];
    }

    public function deviceIDType()
    {
        return $this->params['deviceIDType'] == 'udid' ||
            $this->params['deviceIDType'] == 'imei';
    }

    protected function udid()
    {
        return Validate::UDID($this->params['deviceID']);
    }

    protected function imei()
    {
        return Validate::IMEI($this->params['deviceID']);
    }

    public function deviceID()
    {
        return $this->params['deviceIDType'] == 'udid' ? $this->udid() : $this->imei();
    }

    public function area()
    {
        return Validate::numericString($this->params['area'], 2, 1, 75);
    }

    public function sector()
    {
        return Validate::numericString($this->params['sector'], 2, 1, 11);
    }

    public function side()
    {
        $oneLetter = range('A', 'Z');
        $twoLetters = [];

        array_map(function ($letter) use (&$twoLetters) {
            $twoLetters[] = "{$letter}{$letter}";
        }, $oneLetter);

        $letters = array_merge($oneLetter, $twoLetters);
        return in_array($this->params['side'], $letters);
    }

    public function lat()
    {
        return is_float($this->params['lat'] + 0) || is_float($this->params['lat']);
    }

    public function lon()
    {
        return is_float($this->params['lon'] + 0) || is_float($this->params['lon']);
    }

    public function plate()
    {
        return !!preg_match('/^[A-Z]{3}\d{4}$/', $this->params['plate']);
    }

    public function time()
    {
        $time = (int) $this->params['time'];
        return in_array($time, [30, 60, 120, 180]);
    }

    public function amount()
    {
        return is_numeric($this->params['amount']) &&
            $this->params['amount'] > 0 &&
            $this->params['amount'] < 3;
    }

    public function validate()
    {
        $this->validateRequiredParams();

        if (!empty($this->getErrors())) {
            return false;
        }

        if (!$this->documentType()) {
            $this->errors[] = 'Tipo de documento inválido';
        }

        if (!$this->document()) {
            $this->errors[] = 'Documento inválido';
        }

        if (!$this->plate()) {
            $this->errors[] = 'Placa inválida';
        }

        if (!$this->time()) {
            $this->errors[] = 'Tempo de permanência inválido';
        }

        if (!$this->amount()) {
            $this->errors[] = 'Quantidade inválida';
        }

        if (!$this->transactionID()) {
            $this->errors[] = 'ID da transação inválido';
        }

        return empty($this->getErrors());
    }
}
