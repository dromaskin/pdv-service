<?php

namespace App\Cet\Validator;

use App\Cet\Validator\AbstractValidator as Base;
use App\Validate;

class Unlock extends Base {

    protected function requiredParams()
    {
        return [
            'document',
            'documentType',
            'transactionID',
            'amount'
        ];
    }

    public function params()
    {
        return $this->requiredParams();
    }

    public function amount()
    {
        return is_numeric($this->params['amount']) &&
            $this->params['amount'] > 0 &&
            ($this->params['amount'] < 11 || app()->runningInConsole());
    }

    public function validate()
    {
        $this->validateRequiredParams();

        if (!empty($this->getErrors())) {
            return false;
        }

        if (!$this->document()) {
            $this->errors[] = 'Documento inválido';
        }

        if (!$this->documentType()) {
            $this->errors[] = 'Tipo de documento inválido';
        }

        if (!$this->amount()) {
            $this->errors[] = 'Quantidade inválida';
        }

        if (!$this->transactionID()) {
            $this->errors[] = 'ID da transação inválido';
        }

        return empty($this->getErrors());
    }
}
