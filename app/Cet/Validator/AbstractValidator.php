<?php

namespace App\Cet\Validator;

use App\Validate;

abstract class AbstractValidator {

    protected $params;
    protected $errors = [];

    public function __construct(array $params = [])
    {
        if (!empty($params)) {
            $this->setParams($params);
        }
    }

    public function setParams(array $params)
    {
        $this->params = $params;
    }

    public function validateRequiredParams()
    {
        $this->errors = [];
        $required = $this->requiredParams();

        array_map(function ($param) {
            if (!isset($this->params[$param])) {
                $this->errors[] = "O campo {$param} é obrigatório";
            } else if (empty($this->params[$param])) {
                $this->errors[] = "O campo {$param} é obrigatório";
            }
        }, $required);

        return empty($this->getErrors());
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function documentType()
    {
        return $this->params['documentType'] == 'cpf' || $this->params['documentType'] == 'cnpj';
    }

    public function document()
    {
        return $this->params['documentType'] == 'cpf' ?
            Validate::cpf($this->params['document']) :
            Validate::cnpj($this->params['document']);
    }

    public function transactionID()
    {
        return is_numeric($this->params['transactionID']) &&
            $this->params['transactionID'] > 0;
    }

    abstract protected function requiredParams();

    abstract public function params();

    abstract public function validate();
}
