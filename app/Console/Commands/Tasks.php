<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Container\Container as Application;
use App\Services\TaskService as Task;

class Tasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check tasks queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Application $app)
    {
        parent::__construct();
        $this->app = $app;
        $this->task = new Task($app);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->task->execute();
    }
}
