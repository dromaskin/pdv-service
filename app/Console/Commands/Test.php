<?php

namespace App\Console\Commands;
use GuzzleHttp\Client as Guzzle;

use Illuminate\Console\Command;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:app';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $guzzle = new Guzzle();
        // $response = $guzzle->get(\Config::get('cad.cet.api_url'));
        // $client->setDefaultOption('verify', '/Users/lucas/Sites/parkme/cert/CAROOTCAD.cer');
        // var_dump($response->getContents());
        //
        $url = \Config::get('cad.cet.api_url');
        $cert_file = \Config::get('cad.cet.certificate');
        $cert_password = \Config::get('cad.cet.password');

        $ch = curl_init();

        $url = 'https://apphml.cetsp.com.br/webservices/cad/wsDistribuidora.asmx/processarTransacao';

        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            //CURLOPT_HEADER         => true,
            // CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,

            CURLOPT_USERAGENT => 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)',
            CURLOPT_VERBOSE        => true,
            CURLOPT_URL => $url,
            CURLOPT_SSLCERT => $cert_file,
            CURLOPT_SSLCERTPASSWD => $cert_password,
        );

        curl_setopt_array($ch , $options);

        $output = curl_exec($ch);

        curl_close($ch);

        var_dump($output);
    }
}
