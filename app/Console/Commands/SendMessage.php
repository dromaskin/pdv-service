<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Container\Container as Application;
use App\Repository\MessageRepository as Repository;
use App\Services\MessageService as Service;
use App\Repository\TaskRepository as Task;

class SendMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:sms {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send sms message';

    /**
     * Create a new command instance.
     *
     * @return void
     */
     public function __construct(Application $app)
     {
         parent::__construct();
         $this->app = $app;
         $this->repository = new Repository($app);
         $this->service = new Service($app);
         $this->task = new Task($app);
     }

     protected function getMessage($messageId)
     {
         return $this->repository->find($messageId);
     }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $message = $this->getMessage($this->argument('id'));

        if ($message) {
            $this->service->sendMessageRegister($message);
            $this->task->setAsExecuted($message->id);
        }
    }
}
