<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Container\Container as Application;
use App\Repository\ParkingRepository as Parking;
use App\Repository\TaskRepository as Task;
use App\Services\TicketService as Ticket;

class TicketActivation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:ticket {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send ticket to activation service';

    /**
     * Create a new command instance.
     *
     * @return void
     */
     public function __construct(Application $app)
     {
         parent::__construct();
         $this->app = $app;
         $this->parking = new Parking($app);
         $this->task = new Task($app);
         $this->ticket = new Ticket($app);
     }

     protected function getParking($parkingId)
     {
         return $this->parking->find($parkingId);
     }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $parking = $this->getParking($this->argument('id'));
        if ($parking) {
            $this->ticket->createActivate($parking);
            $this->task->setAsExecuted($parking->id);
        }
    }
}
