<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Container\Container as Application;
use App\Services\PurchaseCadService;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;

class PurchaseCredit extends Command
{
    protected $service;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cad:purchase {document} {amount}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purchase ticket to user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Application $app)
    {
        parent::__construct();
        $this->service = new PurchaseCadService($app);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->service->handle(
                $this->argument('document'),
                $this->argument('amount')
            );
            $this->info('compra efetuada com sucesso');
        } catch (GenericException $e) {
            dd($e->getMessage());
        } catch (ValidationException $e) {
            dd($e->getMessages());
        } catch (\Exception $e) {
            dd($e->getMessage());
            dd($e);
        }
    }
}
