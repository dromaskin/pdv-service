<?php

namespace App\InfoPib;

use GuzzleHttp\Client as Guzzle;
use App\Exceptions\GenericException;
use Config;

class Message {

    private $client;

    private $config;

    private function config()
    {
        if (!$this->config) {
            $this->config = Config::get('cad.sms');
        }

        return $this->config;
    }

    private function getClient()
    {
        if (!$this->client) {
            $this->client = new Guzzle([
                'base_uri' => $this->config()['url'],
                'auth' => [
                    $this->config()['user'],
                    $this->config()['pass']
                ]
            ]);
        }

        return $this->client;
    }

    public function send($to, $message)
    {
        if (empty($message)) {
            throw new GenericException("Mensagem inválida");
        }

        $client = $this->getClient();

        try {
            $response = $client->post('text/single', [
                'json' => [
                    'from' => $this->config()['from'],
                    'to' => $this->config()['country_code'] . $to,
                    'text' => $message
                ]
            ]);
            return $this->handleResponse($response->getBody()->getContents());
        } catch (\Exception $e) {
            Log::error('calling infopib');
            Log::error($e);
            return false;
        }
    }

    public function handleResponse($response)
    {
        try {
            $data = json_decode($response, true);
            if (isset($data['messages'])) {
                if (count($data['messages']) == 1) {
                    $message = $data['messages'][0];
                    if (isset($message['messageId'])) {
                        return $message['messageId'];
                    }
                }
            }
        } catch (\Exception $e) {
            return false;
        }

        return false;
    }
}
