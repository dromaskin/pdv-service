<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Model\CreditCard;
use App\Model\User;
use Log;
use DB;

class AppServiceProvider extends ServiceProvider
{

    protected $time;
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        CreditCard::creating(function ($creditCard) {
            unset($creditCard->number);
        });

        User::creating(function ($user) {
            if (!empty($user['password'])) {
                $user['password'] = bcrypt($user['password']);
            } else {
                $password = str_random(6);
                $user['password'] = bcrypt($password);
                session(['user_new_password' => $password]);
            }
        });

        app()->bind('StreetService', function ($app) {
            return new \App\Services\StreetService($app);
        });

        app()->bind('TicketService', function ($app) {
            return new \App\Services\TicketService($app);
        });

        app()->bind('UserService', function ($app) {
            return new \App\Services\UserService($app);
        });

        app()->bind('UserRepository', function ($app) {
            return new \App\Repository\UserRepository($app);
        });

        if ($this->enableLogging()) {
            $this->time = microtime(true);
            app()->terminating(function () {
                $diff = microtime(true) - $this->time;
                $sec = intval($diff);
                $micro = $diff - $sec;
                Log::info("execution time: {$micro}");
            });

            if (config('database.debug')) {
                DB::enableQueryLog();
                app()->terminating(function () {
                    Log::info('query logs');
                    Log::info(DB::getQueryLog());
                });
            }
        }
    }

    protected function enableLogging()
    {
        return config('app.debug') && !(!config('app.debug_console') && app()->runningInConsole());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
