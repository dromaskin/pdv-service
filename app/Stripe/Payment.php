<?php

namespace App\Stripe;

use Stripe\Stripe;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Refund;
use Stripe\Error\InvalidRequest;
use Stripe\Error\Authentication;
use App\Exceptions\GenericException;
use Stripe\Error\Card;
use Config;
use Log;

class Payment {

    protected $config;

    protected $client;

    public function __construct()
    {
        $this->config = Config::get('cad.stripe');
        Stripe::setApiKey($this->config['secret']);
    }

    public function refund($charge)
    {
        try {
            $response = Refund::create(array(
              "charge" => $charge
            ));
            Log::info('refund');
            Log::info($response);
            return $response->id;
        } catch (\Exception $e) {
            Log::error('creating customer data');
            Log::error($e->getMessage());
            Log::error($e);
            throw $e;
        }
    }

    /**
     * Create a customer token
     * @param  [type] $token       [description]
     * @param  [type] $email       [description]
     * @param  [type] $description [description]
     * @return [type]              [description]
     */
    public function customer($token, $email)
    {
        try {
            $response = Customer::create(array(
              "description" => "Customer for {$email}",
              "source" => $token
            ));
            return $response->id;
        } catch (Card $e) {
            $code = $e->getStripeCode();
            $param = $e->getStripeParam();
            $error = $this->getErrorFromCode($code);

            if (!$error) {
                throw $e;
            } else {
                throw new GenericException($error);
            }
        } catch (GenericException $e) {
            throw $e;
        } catch (\Exception $e) {
            Log::error('creating customer data');
            Log::error($e->getMessage());
            Log::error($e);
            throw $e;
        }
    }

    /**
     * send charge request to stripe
     * @param  [type] $amount [description]
     * @param  [type] $token  [description]
     * @return [type]         [description]
     */
    public function charge($amount, $token, \App\Model\User $user)
    {
        $amount = (int)($amount*100);
        $status = false;
        $invalidCard = false;
        $message = '';
        try {
            $data = [
                'amount' => $amount,
                'customer' => $token,
                'currency' => $this->config['currency'],
                'description' => "charge for {$user->name} <{$user->email}>"
            ];
            $response = Charge::create($data);
            $message = $response->id;
            $status = true;
        } catch (Card $e) {
            $code = $e->getStripeCode();
            $param = $e->getStripeParam();
            $error = $this->getErrorFromCode($code);

            if (!$error) {
                throw $e;
            } else {
                $invalidCard = $this->invalidateCard($code);
                $message = $error;
            }

        }

        return [$status, $message, $invalidCard];
    }

    /**
     * Get error according to response code
     * @param  [type] $code [description]
     * @return [type]       [description]
     */
    protected function getErrorFromCode($code)
    {
        switch ($code) {
            case 'card_declined':
                return "Cartão de crédito recusado";
            case 'incorrect_cvc':
                return "Código de segurança inválido";
            case 'expired_card':
                return "Cartão expirado";
            case 'processing_error':
                return "Erro ao processar cartão de crédito. Tente novamente mais tarde.";
            default:
                return false;
        }
    }

    /**
     * Invalidate a credit card according to response code
     * @param  [type] $code [description]
     * @return [type]       [description]
     */
    protected function invalidateCard($code)
    {
        switch ($code) {
            case 'card_declined':
            case 'incorrect_cvc':
            case 'expired_card':
                return true;
            default:
                return false;
        }
    }

}
