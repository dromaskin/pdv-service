<?php

namespace App\Repository;

use App\Repository\Base;
use App\Validate;

class StreetRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\Street";
    }

    public function getByCode($meter)
    {
        return $this->model
            ->select($this->model->getTable() . '.*')
            ->where('code', $meter)
            ->where($this->model->getTable() . '.active', true)
            ->first();
    }

    public function getByInfo($area, $sector, $face)
    {
        return $this->model
            ->select($this->model->getTable() . '.*')
            ->where('sector', $sector)
            ->where('face', $face)
            ->where($this->model->getTable() . '.active', true)
            ->join('areas', 'areas.id', '=', 'area_id')
            ->where('areas.code', $area)
            ->where('areas.active', true)
            ->first();
    }

    public function getFirstInfo()
    {
        return $this->model
            ->select($this->model->getTable() . '.*')
            ->where($this->model->getTable() . '.active', true)
            ->join('areas', 'areas.id', '=', 'area_id')
            ->where('areas.active', true)
            ->first();
    }
}
