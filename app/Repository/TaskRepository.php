<?php

namespace App\Repository;

use App\Repository\Base;
use App\Repository\ParkingRepository;
use App\Model\Parking;
use DB;

class TaskRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\Task";
    }

    public function save(array $data)
    {
        if ($data['type'] == 'ticket') {
            $repository = new ParkingRepository($this->app);
            $parking = Parking::find($data['target_id']);

            if ($parking) {
                $repository->attachCredits($parking, 'unlocked');
                $repository->updateStatus($parking, 'scheduled');
                return parent::save($data);
            }

            throw new Exception("Erro ao programar CAD");
        }

        return parent::save($data);
    }

    public function toRun()
    {
        $date = new \DateTime();
        // return $this->model->whereNull('executed_at')->get();
        return $this->model
            ->whereNull('executed_at')
            ->whereRaw(DB::raw("date_format(at_time, '%Y-%m-%d %H:%i') <= '{$date->format('Y-m-d H:i')}'"))
            ->get();
    }

    public function getLastTarget($targetId)
    {
        return $this->model
                ->orderBy('id', 'desc')
                ->where('target_id', $targetId)
                ->first();
    }

    public function setAsExecuted($targetId)
    {
        $target = $this->getLastTarget($targetId);

        if ($target) {
            $date = new \DateTime();
            $target->executed_at = $date->format('Y-m-d H:i:s');
            $target->save();
        }
    }

    public function removeTarget($targetId)
    {
        $target = $this->getLastTarget($targetId);

        if ($target) {
            $target->delete();
        }
    }
}
