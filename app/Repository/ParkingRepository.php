<?php

namespace App\Repository;

use App\Repository\Base;
use App\Model\ParkingCredit;
use App\Model\ParkingSpot;
use App\Repository\TaskRepository;
use App\Repository\OrderRepository;
use App\Exceptions\GenericException;
use DB;
use Log;
use DateTime;

class ParkingRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\Parking";
    }

    public function getSpot($parkingSpotId)
    {
        return ParkingSpot::find($parkingSpotId);
    }

    public function attachCredits(\App\Model\Parking $parking, $status, $force = false)
    {
        $whereRaw = "credits.id not in (select credit_id from " .
            "parkings_credits)";
        $credits = DB::table('credits')
            ->where('credits.user_id', $parking->user_id)
            ->where('credits.status', $status)
            ->whereRaw(DB::raw($whereRaw))
            ->take($parking->amount)
            ->pluck('id');

        if ($credits) {
            $parking->credits()->attach($credits);
            return true;
        } elseif ($force) {
            $credits = [];

            for ($a = 0; $a < $parking->amount; $a++) {
                $parking->credits()->create(['status' => $status, 'user_id' => $parking->user_id]);
            }

            return true;
        }

        Log::error("attach parking credits {$status} {$force}");
        Log::error($parking);
        throw new GenericException("Usuário não tem créditos para vincular");
    }

    public function updateStatus(
        \App\Model\Parking $parking,
        $creditStatus,
        $status = false
    ) {
        if (!$status) {
            $status = $creditStatus;
        }

        DB::beginTransaction();
        try {
            if (in_array($status, $this->model->getAllowed())) {
                $this->checkDeviceId($parking, $creditStatus);
            }

            if ($status == 'canceled') {
                $this->removeTask($parking);
            }

            $parking->credits->each(function ($credit) use ($creditStatus) {
                $credit->status = $creditStatus;
                $credit->save();
            });
            $parking->status = $status;
            $parking->save();

            DB::commit();
            return true;
        } catch (\Exception $e) {
            Log::error("change parking status {$creditStatus} - {$status}");
            Log::error($e);
            DB::rollBack();
            return false;
        }
    }

    public function removeTask(\App\Model\Parking $parking)
    {
        $repository = new TaskRepository($this->app);
        $repository->removeTarget($parking->id);
    }

    protected function checkDeviceId(
        \App\Model\Parking $parking,
        $creditStatus
    ) {
        $parkings = false;

        if ($this->isDifferentRule($parking)) {
            $parkings = $this->getTicketForDifferentRule($parking);
        } elseif ($this->userExceededActiveTickets($parking)) {
            $parkings = $this->getTicketsForPlate($parking);
        }

        if ($parkings) {
            $parkings->each(function ($p) {
                $method = $p->status == 'scheduled' ? 'unlocked' : 'losed';
                $this->updateStatus($p, $method, 'canceled');
                if ($method != 'losed') {
                    $this->detachCredits($p);
                }
            });
        }
    }

    protected function getTicketForDifferentRule(\App\Model\Parking $parking)
    {
        $parkings = $this->listActivated([
            'plate' => $parking->order->plate->number,
            'not_rule' => $parking->rule,
            'valid_from' => $parking->valid_from,
            'valid_to' => $parking->valid_to
        ], false);

        return $parkings;
    }

    protected function getTicketsForPlate(\App\Model\Parking $parking)
    {
        $parkings = $this->listActivated([
            'plate' => $parking->order->plate->number,
            'valid_from' => $parking->valid_from,
            'valid_to' => $parking->valid_to
        ], false);

        return $parkings;
    }

    protected function isDifferentRule(\App\Model\Parking $parking)
    {
        $parkings = $this->listActivated([
            'plate' => $parking->order->plate->number,
            'not_rule' => $parking->rule,
            'valid_from' => $parking->valid_from,
            'valid_to' => $parking->valid_to
        ]);

        return $parkings > 0;
    }

    protected function getUserTicketForDevice(\App\Model\Parking $parking)
    {
        return $this->listActivated([
            'device_id' => $parking->order->device_id,
            'valid_from' => $parking->valid_from,
            'valid_to' => $parking->valid_to
        ], false);
    }

    protected function userExceededActiveTickets(\App\Model\Parking $parking)
    {
        $data = [
            'plate' => $parking->order->plate->number,
            'valid_from' => $parking->valid_from,
            'valid_to' => $parking->valid_to
        ];
        $parkings = $this->listActivated($data);

        return ($parking->amount + $parkings) > 2;
    }

    public function updateValidFrom(\App\Model\Parking $parking)
    {
        $repository = new OrderRepository($this->app);
        $lastUpdate = $repository->getLastCreditTransactionUpdate($parking->order);
        $extension = $repository->getLastCreditTransactionExtension($parking->order);

        if (is_bool($extension)) {
            if (!$extension) {
                $parking->extension_id = null;
            } elseif (!$parking->extension_id) {
                $parking->extension_id = $parking->id;
            }
        }

        $validFrom = $lastUpdate ?
            $lastUpdate :
            (new DateTime())->format('Y-m-d H:i:s');
        $time = $parking->getTimeForRule($parking->rule)*$parking->amount;
        $parking->valid_from = $validFrom;

        if (!$parking->extension_id) {
            $parking->valid_to = (new DateTime($validFrom))->modify("+{$time} minutes")
                ->format('Y-m-d H:i:s');
        }

        $parking->save();
    }

    public function detachCredits(\App\Model\Parking $parking)
    {
        $parking->credits()->detach();
    }

    public function isAllowedToCreate(\App\Model\Order $order)
    {
        if ($order->parking) {
            return $order->parkings()->whereIn('status', $this->model->getAllowed())->count() == 0;
        }

        return true;
    }

    public function listActivated(array $data, $sum = true)
    {
        $validFrom = new DateTime();
        $validFrom->setTimestamp(strtotime($data['valid_from']));
        $data['valid_from'] = $validFrom->modify('+1 minutes')
            ->format('Y-m-d H:i:s');
        $raw = "('{$data['valid_from']}' between valid_from and valid_to or " .
            "'{$data['valid_to']}' between valid_from and valid_to)";
        $resultSet = $this->model
            ->join('orders', 'order_id', '=', "orders.id")
            ->whereIn('status', $this->model->getAllowed())
            ->whereRaw(DB::raw($raw));

        if (isset($data['device_id'])) {
            $resultSet->where('device_id', $data['device_id']);
        }

        if (isset($data['rule'])) {
            $resultSet->where('rule', $data['rule']);
        }

        if (isset($data['not_rule'])) {
            $resultSet->where('rule', '<>', $data['not_rule']);
        }

        if (isset($data['plate'])) {
            $resultSet->join('plates', 'plate_id', '=', "plates.id")
                ->where('plates.number', $data['plate']);
        }

        if (isset($data['parking_spot_id'])) {
            $spot = $this->getSpot($data['parking_spot_id']);

            if ($spot) {
                $resultSet->join('parking_spots', 'parking_spot_id', '=', "parking_spots.id")
                    ->where('street_id', $spot->street_id);
            }
        }

        if ($sum) {
            return $resultSet->sum("{$this->model->getTable()}.amount");
        }

        return $resultSet
                ->orderBy("{$this->model->getTable()}.id")
                ->select("{$this->model->getTable()}.*")
                ->get();
    }

    public function calculateTime(\App\Model\Parking $parking)
    {
        $from = new DateTime($parking->valid_from);
        $to = new DateTime($parking->valid_to);
        $interval = $from->diff($to);
        $hours = (int) $interval->format('%h');
        return ($hours*60)/$parking->amount;
    }

    public function activatedPlatesForDevice(array $data)
    {
        $raw = "('{$data['valid_from']}' between valid_from and valid_to or " .
            "'{$data['valid_to']}' between valid_from and valid_to)";
        $plates = $this->model
            ->join('orders', 'order_id', '=', "orders.id")
            ->whereIn('status', $this->model->getAllowed())
            ->join('plates', 'plate_id', '=', "plates.id")
            ->where('plates.number', '<>', $data['plate'])
            ->where('device_id', $data['device_id'])
            ->whereRaw(DB::raw($raw))
            ->groupBy('plates.id')
            ->get();
        return count($plates);
    }
}
