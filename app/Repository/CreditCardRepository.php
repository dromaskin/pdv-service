<?php

namespace App\Repository;

use App\Repository\Base;
use DB;

class CreditCardRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\CreditCard";
    }

    public function getCardCompany($creditCardNumber)
    {
        $firstDigits = substr($creditCardNumber, 0, 6);
        $company = \DB::table('card_company_bins')
            ->select('card_companies.*')
            ->join(
                'card_companies',
                'card_companies.id',
                '=',
                'card_company_bins.card_company_id'
            )
            ->where('number', $firstDigits)
            ->first();
        return $company;
    }

    public function getCardDiscount($cardId)
    {
        $card = $this->model->find($cardId);

        if ($card) {
            if ($card->card_company_id) {
                $company = DB::table('card_companies')->find($card->card_company_id);
                if ($company) {
                    return $company->discount;
                }
            }
        }

        return 0;
    }
}
