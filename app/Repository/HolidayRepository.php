<?php

namespace App\Repository;

use App\Repository\Base;
use DateTime;

class HolidayRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\Holiday";
    }

    public function isHolidayDate(DateTime $date)
    {
        return $this->model->where('date', $date->format('Y-m-d'))->count() > 0;
    }
}
