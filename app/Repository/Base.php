<?php

namespace App\Repository;

use Illuminate\Container\Container as Application;
use Illuminate\Database\Eloquent\Model;

abstract class Base
{

    protected $model;
    protected $app;
    protected $object;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    abstract protected function model();

    public function makeModel()
    {

        if (!class_exists($this->model())) {
            throw new \Exception("The model class {$this->model()} not exists");
        }

        $className = $this->model();
        $model = new $className();

        if (!$model instanceof Model) {
            throw new \Exception("Class {$this->model()} must be an instance " .
                "of Illuminate\\Database\\Eloquent\\Model");
        }

        $this->model = $model;
        return $this;
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function make(array $data = [])
    {
        $className = $this->model();
        $this->object = new $className($data);
        return $this;
    }

    public function removeRule($rule) {
        if (!$this->object) {
            $this->make();
        }

        $this->object->removeRule($rule);
    }

    public function create()
    {
        $this->object->save();
        return $this->object;
    }

    public function save(array $data)
    {
        if (isset($data['id'])) {
            $this->model->where('id', $data['id'])->update($data);
            return $this->model->find($data['id']);
        }

        return $this->model->create($data);
    }

    public function validate()
    {
        return $this->object->isValid();
    }

    public function errors()
    {
        return $this->object->errors()->all();
    }
}
