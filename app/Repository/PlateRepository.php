<?php

namespace App\Repository;

use App\Repository\Base;
use DB;

class PlateRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\Plate";
    }

    public function save(array $data)
    {
        if (isset($data['user_id']) && isset($data['number'])) {
            $plate = $this->model
                ->where('user_id', $data['user_id'])
                ->whereRaw(DB::raw("upper(number) = '" . strtoupper($data['number'] . "'")))
                ->first();
            if ($plate) {
                if (!$plate->active) {
                    $plate->active = true;
                    $plate->save();
                }
                return $plate;
            }
        }

        return parent::save($data);
    }
}
