<?php

namespace App\Repository;

use App\Repository\Base;

class PaymentMethodRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\PaymentMethod";
    }

    public function all()
    {
        return $this->model
            ->where('active', true)
            ->orderBy('order')
            ->get();
    }

    public function allButPrePaid()
    {
        return $this->model
            ->where('active', true)
            ->where('pre_paid', false)
            ->orderBy('order')
            ->get();
    }

    public function getByType($type)
    {
        return $this->model->where('slug', $type)->first();
    }
}
