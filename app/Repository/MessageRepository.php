<?php

namespace App\Repository;

use App\Repository\Base;

class MessageRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\Message";
    }
}
