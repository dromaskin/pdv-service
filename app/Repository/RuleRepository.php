<?php

namespace App\Repository;

use App\Repository\Base;
use DB;
use Config;

class RuleRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\Rule";
    }

    public function getHoursByDay($ruleId, $dayInteger)
    {
        $rule = $this->model
            ->where('rules.id', $ruleId)
            ->where('rules.active', true)
            ->where('rule_schedules.active', true)
            ->select('rule_schedules.*')
            ->join('rule_schedules', 'rule_schedules.rule_id', '=', 'rules.id')
            ->whereRaw(DB::raw("days & {$dayInteger}"))
            ->first();
        return $rule;
    }

    public function getTime(\App\Model\ParkingSpot $spot)
    {
        if ($spot->type->slug == 'caminhao') {
            return 30;
        } else {
            return $spot->street->rule->max_time / 2;
        }
    }
}
