<?php

namespace App\Repository;

use App\Repository\Base;

class OrderTransactionRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\OrderTransaction";
    }
}
