<?php

namespace App\Repository;

use App\Repository\Base;
use App\Model\Credit;
use App\Model\Parking;
use DB;

class OrderRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\Order";
    }

    public function create()
    {
        $return = parent::create();

        if ($return->method->pre_paid) {
            return $return;
        }

        for ($a = 0; $a < $this->object->amount; $a++) {
            Credit::create([
                'user_id' => $this->object->user_id
            ]);
        }

        return $return;
    }

    protected function changeCreditStatus(
        \App\Model\Order $order,
        $status,
        $previousStatus = 'created',
        $amount = null
    ) {
        $amount = $amount ? $amount : $order->amount;
        $credits = $order->user->credits()
            ->where('status', $previousStatus)
            ->orderBy('id')
            ->take($amount)
            ->get();
        $credits->each(function ($credit) use ($status) {
            $credit->status = $status;
            $credit->save();
        });
    }

    public function cancelCredits(\App\Model\Order $order, $amount = null, $previousStatus = 'created')
    {
        $this->changeCreditStatus($order, 'canceled', $previousStatus, $amount);
    }

    public function unlockCredits(\App\Model\Order $order, $amount = null, $previousStatus = 'created')
    {
        $this->changeCreditStatus($order, 'unlocked', $previousStatus, $amount);
    }

    public function activateCredits(
        \App\Model\Order $order,
        $amount = null,
        $previousStatus = 'unlocked'
    ) {
        $this->changeCreditStatus($order, 'activated', $previousStatus, $amount);
    }

    public function scheduleCredits(\App\Model\Order $order)
    {
        $this->changeCreditStatus($order, 'scheduled', 'unlocked', $order->parking->amount);
    }

    public function saveToken(\App\Model\Order $order, $token)
    {
        if (!$token) {
            return;
        }

        $order->token = $token;
        $order->save();
    }

    public function setCreditCardAsInactive(\App\Model\Order $order)
    {
        if ($order->creditCard) {
            $creditCard = $order->creditCard;
            $creditCard->active = false;
            $creditCard->save();
        }
    }

    public function getChargedPaymentTransaction(\App\Model\Order $order)
    {
        return $order->transactions()->where('type', 'payment')
            ->where('message', 'like', 'ch%')->orderBy('id', 'desc')->first();
    }

    public function getLastCreditTransactionUpdate(\App\Model\Order $order)
    {
        $transaction = $order->transactions()
            ->orderBy('id', 'desc')
            ->whereNotNull('data')
            ->first();
        if ($transaction) {
            $data = json_decode($transaction->data, true);
            if ($data) {
                if (array_key_exists('dataProcessamento', $data)) {
                    return (new \DateTime($data['dataProcessamento']))->format('Y-m-d H:i:s');
                }
            }
        }

        return false;
    }

    public function getLastCreditTransactionExtension(\App\Model\Order $order)
    {
        $transaction = $order->transactions()
            ->orderBy('id', 'desc')
            ->whereNotNull('data')
            ->first();
        if ($transaction) {
            $data = json_decode($transaction->data, true);
            if ($data) {
                if (array_key_exists('ehExtensao', $data)) {
                    return $data['ehExtensao'] == 'true';
                }
            }
        }

        return null;
    }

    protected function getCardCompany($cardCompanyId)
    {
        return DB::table('card_companies')->find($cardCompanyId);
    }

    protected function getDiscount(\App\Model\Order $order)
    {
        if (!empty($order->creditCard->card_company_id)) {
            $cardCompany = $this->getCardCompany($order->creditCard->card_company_id);

            if ($cardCompany) {
                return $cardCompany->discount;
            }
        }

        return 0;
    }

    protected function calculateDiscount($amount, $discount)
    {
        return ($amount*$discount)/100;
    }

    public function getPrice(\App\Model\Order $order)
    {
        $amount = $order->price->price;

        if ($order->amount < 10) {
            return $amount - $this->calculateDiscount(
                $amount,
                $this->getDiscount($order)
            );
        }

        return $amount;
    }
}
