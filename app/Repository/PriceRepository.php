<?php

namespace App\Repository;

use App\Repository\Base;

class PriceRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\Price";
    }

    public function get()
    {
        return $this->model
            ->select('id', 'title', 'amount', 'pre_charger', 'price')
            ->where('active', true)
            ->orderBy('order')
            ->get();
    }

    public function getByAmount($amount)
    {
        return $this->model
            ->where('amount', $amount)
            ->first();
    }
}
