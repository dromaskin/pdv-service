<?php

namespace App\Repository;

use App\Repository\Base;
use App\Exceptions\GenericException;
use DB;

class UserRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\User";
    }

    public function hasCredit($userId)
    {
        return $this->credits($userId) > 0;
    }

    public function credits($userId)
    {
        $user = $this->model->find($userId);

        if (!$user) {
            return false;
        }

        return $user->credits()->where('status', 'unlocked')->count();
    }

    public function save(array $data)
    {
        if (isset($data['document']) && isset($data['email'])) {
            $resultSet = $this->model
                ->whereRaw(DB::raw("(document = '{$data['document']}' or email = '{$data['email']}')"));

            if (isset($data['id'])) {
                $resultSet->where('id', '<>', $data['id']);
            }

            $found = $resultSet->count();

            if ($found > 0) {
                throw new GenericException("Número do documento ou email já cadastrado");
            }
        }

        return parent::save($data);
    }

    public function getFillable()
    {
        return $this->model->getFillable();
    }

    public function getByEmail($email)
    {
        return $this->model->where('email', $email)->where('active', true)->first();
    }

    public function getByDocument($document)
    {
        return $this->model->where('document', $document)
            ->where('active', true)->first();
    }

    public function getByRecoverToken($token)
    {
        return $this->model->where('recover_token', $token)
            ->where('active', true)->first();
    }

    public function getByToken($token)
    {
        return $this->model->where('token', $token)
            ->where('active', true)
            ->first();
    }

    public function updatePassword($password, $token)
    {
        $user = $this->getByRecoverToken($token);

        if (!$user) {
            throw new GenericException("Token de recuperação não encontrado" .
                "<br />Usuário deve fazer uma nova solicitação de alteração " .
                "de senha");
        }

        $user->password = bcrypt($password);
        $user->recover_token = null;
        $user->save();
        return true;
    }

    public function scheduledParkings(\App\Model\User $user)
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');
        $parkings = $this->model
            ->join('parkings', 'parkings.user_id', '=', "{$this->model->getTable()}.id")
            ->join('orders', 'parkings.order_id', '=', "orders.id")
            ->join('plates', 'orders.plate_id', '=', "plates.id")
            ->where('parkings.status', 'scheduled')
            ->where('valid_from', '>', $now)
            ->where('parkings.user_id', $user->id)
            ->select(
                'valid_from',
                'parkings.amount',
                'valid_to',
                'order_id',
                'plates.number as plate'
            )
            ->get();
        return $parkings;
    }

    public function scheduledParking($orderId)
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');
        $parking = $this->model
            ->join('parkings', 'parkings.user_id', '=', "{$this->model->getTable()}.id")
            ->join('orders', 'parkings.order_id', '=', "orders.id")
            ->join('plates', 'orders.plate_id', '=', "plates.id")
            ->where('parkings.status', 'scheduled')
            ->where('valid_from', '>', $now)
            ->where('orders.id', $orderId)
            ->select(
                'valid_from',
                'parkings.amount',
                'valid_to',
                'order_id',
                'plates.number as plate'
            )
            ->first();
        return $parking;
    }

    public function activeParkings($deviceId)
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');
        $raw = "((valid_from < '{$now}' and valid_to > '{$now}') or " .
            "(valid_from > '{$now}' and extension_id is not null))";
        $parkings = $this->model
            ->join('parkings', 'parkings.user_id', '=', "{$this->model->getTable()}.id")
            ->join('orders', 'parkings.order_id', '=', "orders.id")
            ->join('plates', 'orders.plate_id', '=', "plates.id")
            ->where('parkings.status', 'activated')
            ->whereRaw(DB::raw($raw))
            ->where('orders.device_id', $deviceId)
            ->select(
                'parkings.id',
                'extension_id',
                'valid_from',
                'parkings.amount',
                'valid_to',
                'order_id',
                'plates.number as plate'
            )
            ->get();
        $remove = [];
        $parkings->each(function ($parking) use (&$remove) {
            $parking->valid_to = strtotime($parking->valid_to)*1000;
            $parking->valid_from = strtotime($parking->valid_from)*1000;
            if ($parking->extension_id) {
                $remove[] = $parking->extension_id;
            }
        });

        $parkings = $parkings->filter(function ($parking) use ($remove) {
            return !in_array($parking->id, $remove);
        });

        return array_values($parkings->toArray());
    }

    public function activeParking($orderId)
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');
        $raw = "((valid_from < '{$now}' and valid_to > '{$now}') or " .
            "(valid_from > '{$now}' and extension_id is not null))";
        $parking = $this->model
            ->join('parkings', 'parkings.user_id', '=', "{$this->model->getTable()}.id")
            ->join('orders', 'parkings.order_id', '=', "orders.id")
            ->join('plates', 'orders.plate_id', '=', "plates.id")
            ->where('parkings.status', 'activated')
            ->where('orders.id', $orderId)
            ->select(
                'parkings.rule',
                'valid_from',
                'extension_id',
                'parkings.amount',
                'valid_to',
                'order_id',
                'plates.number as plate'
            )
            ->first();
        if ($parking) {
            if ($parking->extension_id) {
                $extension = \App\Model\Parking::find($parking->extension_id);
                $validFrom = new \DateTime($extension->valid_from);
                $parking->valid_from = strtotime($extension->valid_from)*1000;
                $time = $extension->getTimeForRule($parking->rule) *
                    ($extension->amount+$parking->amount);
                $parking->valid_to = $validFrom->modify("+{$time} minutes")->getTimestamp()*1000;
            } else {
                $parking->valid_to = strtotime($parking->valid_to)*1000;
                $parking->valid_from = strtotime($parking->valid_from)*1000;
            }
        }
        return $parking;
    }

    public function removePlate(\App\Model\User $user, $plateId)
    {
        $plate = $user->plates()->find($plateId);

        if (!$plate) {
            throw new GenericException("Placa não encontrada");
        }

        $plate->active = 0;
        $plate->save();
        return true;
    }

    public function getOrders(\App\Model\User $user, $period)
    {
        $orders = $user->orders()->whereNotNull('token')->orderBy('id', 'desc');
        switch ($period) {
            case 'all':
                break;
            case 'today':
                $orders->whereRaw(DB::raw("orders.created_at between " .
                    "date_sub(current_timestamp, interval 1 day) and current_timestamp"));
                break;
            case 'yesterday':
                $orders->whereRaw(DB::raw("orders.created_at between " .
                    "date_sub(current_timestamp, interval 2 day) " .
                    " and date_sub(current_timestamp, interval 1 day)"));
                break;
            case 'last_3':
                $orders->whereRaw(DB::raw("orders.created_at between " .
                    "date_sub(current_timestamp, interval 3 day) " .
                    " and current_timestamp"));
                break;
            case 'last_7':
                $orders->whereRaw(DB::raw("orders.created_at between " .
                    "date_sub(current_timestamp, interval 7 day) " .
                    " and current_timestamp"));
                break;
            case 'last_30':
                $orders->whereRaw(DB::raw("orders.created_at between " .
                    "date_sub(current_timestamp, interval 30 day) " .
                    " and current_timestamp"));
                break;
            case 'month':
                $orders->whereRaw(DB::raw("date_format(orders.created_at, '%Y%m') = " .
                    "date_format(current_timestamp, '%Y%m')"));
                break;
            case 'last_month':
                $orders->whereRaw(DB::raw("date_format(orders.created_at, '%Y%m') = " .
                    "date_format(date_sub(current_timestamp, interval 1 month), '%Y%m')"));
                break;
            case 'year':
                $orders->whereRaw(DB::raw("date_format(orders.created_at, '%Y') = " .
                    "date_format(current_timestamp, '%Y')"));
                break;
            default:
                throw new GenericException("Período de busca inválido");
        }
        return $orders->get();
    }

    public function createTokenRecover(\App\Model\User $user)
    {
        $token = str_random(32);
        $user->recover_token = $token;
        $user->save();
        return $token;
    }

    public function addPlate(\App\Model\User $user, $plateNumber)
    {
        $plate = $user->plates()->where('number', $plateNumber)->first();

        if (!$plate) {
            throw new GenericException("Placa não encontrada");
            return \App\Model\Plate::create([
                'number' => $plateNumber,
                'user_id' => $user->id
            ]);
        }

        return $plate;
    }

    public function getParkingByOrder(\App\Model\User $user, $orderId)
    {
        $order = $user->orders()->where('id', $orderId)->first();

        if ($order) {
            return $order->parking;
        }

        return false;
    }

    public function createToken(\App\Model\User $user)
    {
        $user->token = str_random(60);
        $user->save();
    }

    public function search($query)
    {
        $resultSet = $this->model->orderBy('name');

        if (!empty($query)) {
            $query = strtolower($query);
            $resultSet->orWhere('name', 'like', "%{$query}%")
                ->orWhere('last_name', 'like', "%{$query}%")
                ->orWhere('document', 'like', "%{$query}%")
                ->orWhere('email', 'like', "%{$query}%");
        }

        return $resultSet->get();
    }
}
