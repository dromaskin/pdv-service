<?php

namespace App\Repository;

use App\Repository\Base;

class GuestRepository extends Base
{

    protected function model()
    {
        return "\\App\\Model\\Guest";
    }

    public function findByLogin($login)
    {
        return $this->model->where('login', $login)->where('active', true)->get();
    }

    public function createToken(\App\Model\Guest $guest)
    {
        $guest->token = str_random(60);
        $guest->save();
    }

    public function getByToken($token)
    {
        return $this->model->where('token', $token)
            ->where('active', true)
            ->first();
    }
}
