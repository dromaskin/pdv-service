<?php

namespace App\Services;

use Illuminate\Container\Container as Application;
use App\Repository\CreditCardRepository as CreditCard;
use App\Repository\UserRepository as User;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;
use App\Stripe\Payment;
use App\Validate;
use Config;
use Log;
use DB;

class CreditCardService {

    protected $app;
    protected $user;
    protected $payment;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->creditCard = new CreditCard($this->app);
        $this->user = new User($this->app);
        $this->payment = new Payment();
    }

    protected function validate(array $data)
    {
        if (!$this->creditCard->make($data)->validate()) {
            $error = new ValidationException();
            $error->setMessages($this->creditCard->errors());
            throw $error;
        }

        if (!isset($data['token'])) {
            throw new GenericException("Token do cartão obrigatório");
        }
    }

    protected function createCustomerToken($token, $email)
    {
        if (empty($email) || !Validate::email($email)) {
            throw new GenericException("Email do cartão obrigatório para token");
        }

        return $this->payment->customer($token, $email);
    }

    protected function getCardCompany($cardNumber)
    {
        return $this->creditCard->getCardCompany($cardNumber);
    }

    protected function getUser($userId)
    {
        return $this->user->find($userIid);
    }

    public function create(array $data)
    {
        $this->validate($data);
        $user = $this->user->find($data['user_id']);
        DB::beginTransaction();
        try {
            // descomentar
            $data['token'] = $this->createCustomerToken($data['token'], $user->email);
            $cardCompany = $this->getCardCompany($data['number']);
            $message = "Cartão salvo com sucesso";

            if ($cardCompany) {
                $message = $cardCompany->message;
                $data['card_company_id'] = $cardCompany->id;
            }

            $result = $this->creditCard->save($data);
            DB::commit();
            return [
                'message' => $message,
                'id' => $result->id
            ];
        } catch (ValidationException $e) {
            DB::rollBack();
            throw $e;
        } catch (GenericException $e) {
            DB::rollBack();
            throw $e;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Error saving Credit Card');
            Log::error($e);
            throw new GenericException("Erro ao salvar dados do cartão");
        }
    }
}
