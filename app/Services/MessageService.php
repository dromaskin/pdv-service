<?php

namespace App\Services;

use Sendinblue\Mailin;
use App\InfoPib\Message as SMS;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;
use App\Model\Message;
use App\Util;
use App\Validate;
use Log;
use Auth;
use Config;
use DateTime;

class MessageService {

    private static $instance;
    protected $config;
    protected $client;
    protected $from;
    protected $to;
    protected $subject;
    protected $text;
    protected $html;
    protected $token = null;
    protected $type = null;
    protected $userId = null;
    protected $sentAt = null;
    protected $receivedAt = null;

    public function __construct()
    {
        $this->config = Config::get('cad');
    }

    public static function instance()
    {
        if (!static::$instance) {
            static::$instance = new static;
        }

        return static::$instance;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function to($to, $name = null)
    {
        if ($name) {
            $this->to = [$to => $name];
        } else {
            $this->to = $to;
        }

        return $this;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function subject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function text($text)
    {
        $this->text = $text;
        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function message($html)
    {
        $this->html = $html;
        return $this;
    }

    public function getMessage()
    {
        return $this->html;
    }

    protected function getTextParts($text)
    {
        list($key, $document, $plate, $amount, $rule) = explode(',', $text);

        $document = Util::onlyNumbers($document);

        if (!Validate::document($document)) {
            throw new GenericException('Número do documento inválido');
        }

        $plate = Util::spaces(strtoupper($plate));

        if (!Validate::plate($plate)) {
            throw new GenericException('Número da placa inválido');
        }

        $amount = (int) Util::spaces($amount);

        if ($amount < 1 || $amount > 2) {
            throw new GenericException('Quantidade de CADs inválida');
        }

        $rule = (int) Util::spaces($rule);

        if ($rule < 1 || $rule > 4) {
            throw new GenericException('Regra de estacionamento inválido');
        }

        return [$document, $plate, $amount, $rule];
    }

    protected function validateReceived(array $data)
    {
        if (!array_key_exists('sender', $data) ||
            !array_key_exists('when', $data) ||
            !array_key_exists('text', $data)) {
            throw new Exception('Formato da mensagem inválido');
        }

        $sender = $data['sender'];

        if (starts_with($sender, '55')) {
            $sender = substr($sender, 2);
        }

        $this->to($sender);
        $parts = explode(',', $data['text']);

        if (count($parts) != 5) {
            throw new \Exception(['Formato da mensagem inválido']);
        }
    }

    public function receive(array $data)
    {
        try {
            $this->validateReceived($data);
            list($document, $plate, $amount, $rule) = $this->getTextParts($data['text']);
            $this->validateParking($rule, $amount);
            list($user, $plate) = $this->validateUser($document, $plate, $amount);
            $order = $this->makeOrder($user, $plate, $amount);
            $parking = $this->makeParking($order, $rule);
            $this->createTicket($parking);
            $this->text($data['text']);
            $this->sentAt = (new DateTime())->format('Y-m-d H:i:s');
            $this->save();
            $this->text('CAD ativado');
            $token = $this->sms();
        }  catch (GenericException $e) {
            $this->text($e->getMessage());
            $token = $this->sms();

            if ($token) {
                $this->token = $token;
                $this->sentAt = (new DateTime())->format('Y-m-d H:i:s');
                $this->save();
            }
        } catch (ValidationException $e) {
            $this->text(implode(', ', $e->getMessages()));
            $token = $this->sms();

            if ($token) {
                $this->token = $token;
                $this->sentAt = (new DateTime())->format('Y-m-d H:i:s');
                $this->save();
            }
        } catch (\Exception $e) {
            Log::error('error receive sms');
            Log::error($data);
            Log::error($e);
            dd($e->getMessage());
        }
    }

    protected function validateParking($rule, $amount)
    {
        $service = app()->make('TicketService');
        $time = $this->getTimeForRule($rule) * $amount;
        $now = new DateTime();
        $data = [
            'valid_from' => $now->format('Y-m-d H:i:s'),
            'valid_to' => $now->modify("+{$time} minutes")->format('Y-m-d H:i:s'),
            'amount' => $amount,
            'parking_spot_id' => 1,
            'rule' => $rule
        ];
        $service->validateParking($data);
    }

    protected function validateUser($document, $plate, $amount)
    {
        $repository = app()->make('UserRepository');
        $user = $repository->getByDocument($document);

        if (!$user) {
            throw new GenericException("Nenhum usuário encontrado com o documento {$document}");
        }

        $credits = $repository->credits($user->id);

        if ($amount > $credits) {
            throw new GenericException("Saldo indisponível");
        }

        $plate = $repository->addPlate($user, $plate);
        $this->userId = $user->id;

        return [$user, $plate];
    }

    protected function makeOrder(
        \App\Model\User $user,
        \App\Model\Plate $plate,
        $amount
    ) {
        $paymentMethod = \App\Model\PaymentMethod::where('pre_paid', true)->first();
        $price = \App\Model\Price::where('amount', $amount)->first();
        return \App\Model\Order::create([
            'user_id' => $user->id,
            'plate_id' => $plate->id,
            'price_id' => $price->id,
            'payment_method_id' => $paymentMethod->id,
            'device_id' => str_random(40),
            'amount' => $amount
        ]);
    }

    protected function makeParking(
        \App\Model\Order $order,
        $rule
    ) {
        $time = $this->getTimeForRule($rule) * $order->amount;
        $now = new DateTime();
        $model = new \App\Model\Parking;
        return $model->create([
            'order_id' => $order->id,
            'user_id' => $order->user->id,
            'valid_from' => $now->format('Y-m-d H:i:s'),
            'valid_to' => $now->modify("+{$time} minutes")->format('Y-m-d H:i:s'),
            'amount' => $order->amount,
            'rule' => $rule
        ]);
    }

    protected function createTicket(\App\Model\Parking $parking)
    {
        $service = app()->make('TicketService');
        $service->createActivate($parking, false);
    }

    protected function getTimeForRule($rule)
    {
        $rule = (int) $rule;
        switch ($rule) {
            case 1:
                return 30;
            case 2:
                return 60;
            case 3:
                return 120;
            case 4:
                return 180;
            default:
                throw new ValidationException("Erro ao verificar regra de estacionamenbto", 1);
        }
    }

    public function sms()
    {
        $client = new SMS();

        try {
            return $client->send($this->getTo(), $this->getText());
        } catch (\App\Exceptions\GenericException $e) {
            return $e->getMessage();
        } catch (\Exception $e) {
            Log::error('sending sms');
            Log::error($e);
        }

        return false;
    }

    public function sendMessageRegister(\App\Model\Message $message)
    {
        $this->to($message->number);
        $this->text($message->text);
        $token = $this->sms();
        $message->token = $token;
        $message->sent_at = date('Y-m-d H:i:s');
        $message->save();
    }

    public function scheduleParkingMessage($orderId, $type)
    {
        $user = Auth::user();
        $repository = app()->make('UserRepository');
        $parking = $repository->getParkingByOrder($user, $orderId);

        if (!$parking) {
            throw new GenericException("Vaga não encontrada");
        }

        $time = $parking->valid_to;
        $plate = $parking->order->plate->number;
        switch ($type) {
            case 'end':
                $message = "O CAD para placa {$plate} expirou. " .
                    "Caso deseje, ative um novo CAD no app.";
                break;
            case 'almost':
                $time = (new DateTime($time))
                    ->modify("-10 minutes")
                    ->format('Y-m-d H:i:s');
                $message = "O CAD para placa {$plate} vai expirar em 10 " .
                    "minutos. Caso deseje, ative um novo CAD no app.";
                break;
            default:
                throw new GenericException("Tipo de alarme inválido");
        }

        $exists = \App\Model\Task::where('type', 'sms')
            ->where('at_time', $time)
            ->first();

        if ($exists) {
            throw new GenericException("Já existe um alarme cadastrada");
        }

        $data = [
            'text' => $message,
            'user_id' => $parking->user->id,
            'type' => 'alarm',
            'number' => $parking->user->phone_number
        ];
        $message = \App\Model\Message::create($data);
        $task = \App\Model\Task::create([
            'type' => 'sms',
            'at_time' => $time,
            'target_id' => $message->id
        ]);

        return true;
    }

    public function save()
    {
        $data = [
            'text' => $this->getText(),
            'number' => $this->getTo(),
            'type' => $this->type,
            'sent_at' => $this->sentAt,
            'received_at' => $this->receivedAt,
            'user_id' => $this->userId,
            'token' => $this->token
        ];
        Message::create($data);
    }

    public function email()
    {
        $client = new Mailin($this->config['email']['url'], $this->config['email']['key']);
        $this->from = [$this->config['email']['from'], $this->config['email']['name']];
        $data = [
            'to' => $this->getTo(),
            'from' =>  $this->getFrom(),
            'subject' => $this->getSubject(),
            'text' => $this->getText(),
            'html' => $this->getMessage()
        ];
        $result = $client->send_email($data);
        if (array_key_exists('code', $result)) {
            if ($result['code'] == 'success') {
                return true;
            }
            Log::error('Error sending email');
            Log::error($result);
        }
        return false;
    }
}
