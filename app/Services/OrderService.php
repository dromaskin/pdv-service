<?php

namespace App\Services;

use Illuminate\Container\Container as Application;
use Illuminate\Http\Request;
use App\Repository\OrderRepository as Order;
use App\Repository\PriceRepository as Price;
use App\Repository\PaymentMethodRepository as PaymentMethod;
use App\Repository\UserRepository;
use App\Services\UserService as User;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;
use DB;
use Log;

class OrderService
{

    protected $order;
    protected $price;
    protected $user;
    protected $userRepository;
    protected $paymentMethod;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->order = new Order($this->app);
        $this->price = new Price($this->app);
        $this->user = new User($this->app);
        $this->paymentMethod = new PaymentMethod($this->app);
        $this->userRepository = new UserRepository($this->app);
    }

    public function create(array $data)
    {
        $this->validate($data);
        DB::beginTransaction();
        try {
            $result = $this->order->create();
            DB::commit();
            return $result->id;
        } catch (ValidationException $e) {
            DB::rollBack();
            throw $e;
        } catch (GenericException $e) {
            DB::rollBack();
            throw $e;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Error saving Order');
            Log::error($e);
            throw new GenericException("Erro ao salvar compra");
        }
    }

    protected function validate(array $data)
    {
        $data['amount'] = $this->getAmount($data);
        $this->validateUserCredits($data);

        if (empty($data['device_id'])) {
            $data['device_id'] = str_random(41);
        }

        if (!$this->order->make($data)->validate()) {
            $error = new ValidationException();
            $error->setMessages($this->order->errors());
            throw $error;
        }
    }

    protected function getAmount(array $data)
    {
        if (!isset($data['price_id'])) {
            throw new GenericException("Valor da compra obrigatório");
        }

        $price = $this->price->find($data['price_id']);

        if (!$price) {
            throw new GenericException("Valor da compra não encontrado");
        }

        // credito empresa
        if (app()->runningInConsole() || config('app.is_admin', false)) {
            return $data['amount'];
        }

        return $price->amount;
    }

    public function validateUserCredits(array $data)
    {
        $method = $this->getPaymentMethod($data);
        $user = $this->getUser($data);

        if ($method->pre_paid) {
            if (!(isset($data['amount']) && is_numeric($data['amount']))) {
                throw new GenericException("Informe a quantidade de cad para a compra");
            }

            if ($this->user->credits($user->id) < $data['amount']) {
                throw new GenericException("Saldo insuficiente para a compra");
            }
        }
    }

    protected function getUser(array $data)
    {
        if (!isset($data['user_id'])) {
            throw new GenericException("Usuário inválido");
        }

        $user = $this->userRepository->find($data['user_id']);

        if (!$user) {
            throw new GenericException("Usuário não encontrado");
        }

        return $user;
    }

    protected function getPaymentMethod(array $data)
    {
        if (!isset($data['payment_method_id'])) {
            throw new GenericException("Método de pagamento inválido");
        }

        $method = $this->paymentMethod->find($data['payment_method_id']);

        if (!$method) {
            throw new GenericException("Método de pagamento não encontrado");
        }

        return $method;
    }
}
