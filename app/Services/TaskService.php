<?php

namespace App\Services;

use Illuminate\Container\Container as Application;
use App\Repository\TaskRepository as Task;
use App\Services\TicketService as Ticket;
use Artisan;

class TaskService {

    protected $app;
    protected $task;
    protected $ticket;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->task = new Task($this->app);
        $this->ticket = new Ticket($this->app);
    }

    public function execute()
    {
        $tasks = $this->task->toRun();
        $tasks->each(function ($task) {
            $command = "php " . base_path('artisan') .
                " tasks:%s {$task->target_id} > /dev/null 2>/dev/null &";
            shell_exec(sprintf($command, $task->type));
        });
    }
}
