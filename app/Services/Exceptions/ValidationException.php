<?php

namespace App\Services\Exceptions;

class ValidationException extends \Exception {

    protected $messages = [];

    public function setMessages(array $messages)
    {
        $this->messages = $messages;
    }

    public function getMessages()
    {
        return $this->messages;
    }

}
