<?php

namespace App\Services;

use Illuminate\Container\Container as Application;
use App\Repository\OrderTransactionRepository as OrderTransaction;
use App\Repository\OrderRepository as Order;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;
use App\Stripe\Payment;
use Config;
use Log;
use DB;

class PaymentService {

    protected $app;
    protected $transaction;
    protected $order;
    protected $payment;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->transaction = new OrderTransaction($this->app);
        $this->order = new Order($this->app);
        $this->payment = new Payment();
    }

    public function refund($orderId)
    {
        $order = $this->getOrder($orderId);
        $transaction = $this->order->getChargedPaymentTransaction($order);
        if ($transaction) {
            $refundId = $this->payment->refund($transaction->message);
            $this->saveTransaction(true, $refundId, $order->id);
            return true;
        }

        throw new GenericException("Cobrança não encontrada");
    }

    protected function getOrder($orderId)
    {
        $order = $this->order->find($orderId);

        if (!$order) {
            throw new GenericException("Compra não encontrada");
        }

        return $order;
    }

    protected function getAmount(\App\Model\Order $order)
    {
        return $this->order->getPrice($order);
    }

    protected function make($amount, $customer, \App\Model\User $user)
    {
        return $this->payment->charge($amount, $customer, $user);
    }

    protected function saveTransaction($status, $message, $orderId)
    {
        $data = [
            'status' => $status,
            'message' => $message,
            'order_id' => $orderId,
            'type' => 'payment'
        ];

        return $this->transaction->make($data)->create();
    }

    /**
     * send data to payment gateway
     * save order transaction
     * @param  AppModelOrder $order       [description]
     * @param  [type]        $paymentData [description]
     * @return booelan
     */
    public function send($orderId)
    {
        $order = $this->getOrder($orderId);
        $this->validateOrder($order);

        try {
            list($status, $message, $invalidateCard) = $this->make(
                $this->getAmount($order),
                $order->creditCard->token,
                $order->user
            );
            $this->saveTransaction($status, $message, $order->id);

            if (!$status) {
                // invalidate card if necessary
                if ($invalidateCard) {
                    $this->order->setCreditCardAsInactive($order);
                }

                throw new GenericException($message);
            }
        } catch (GenericException $e) {
            throw $e;
        } catch (\Exception $e) {
            Log::error('saving payment transaction');
            Log::error($e);
            $this->saveTransaction(false, $e->getMessage(), $order->id);
            throw new GenericException("Erro ao enviar pagamento");
        }

        return true;
    }

    /**
     * check if order is valid to bill
     * @param  [type] $order [description]
     * @return [type]        [description]
     */
    protected function validateOrder(\App\Model\Order $order = null)
    {
        if (empty($order)) {
            throw new GenericException("Compra não encontrada");
        }

        if ($order->method->pre_paid) {
                throw new GenericException("Compra feita com créditos pré-pagos");
        }

        if (!$order->creditCard->active) {
            throw new GenericException("Cartão de crédito inválido");
        }
    }
}
