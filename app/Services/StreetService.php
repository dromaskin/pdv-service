<?php

namespace App\Services;

use Illuminate\Container\Container as Application;
use App\Repository\StreetRepository as Street;
use App\Repository\RuleRepository;
use App\Repository\ParkingRepository;
use App\Repository\PaymentMethodRepository;
use App\Services\PaymentService;
use App\Services\PriceService;
use App\Services\UserService;
use App\Exceptions\GenericException;
use App\Constants;
use DateTime;
use App\Lucandrade\Week;
use Auth;
use Config;

class StreetService
{

    protected $app;
    protected $street;
    protected $price;
    protected $rule;
    protected $user;
    protected $paymentMethod;
    protected $parking;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->street = new Street($this->app);
        $this->rule = new RuleRepository($this->app);
        $this->price = new PriceService($this->app);
        $this->user = new UserService($this->app);
        $this->paymentMethod = new PaymentMethodRepository($this->app);
        $this->parking = new ParkingRepository($this->app);
    }

    /**
     * extract parts from code
     * @param  sring $code
     * @return array
     */
    protected function sliceCode($code)
    {
        preg_match('/' . Constants::STREET_CODE . '/', $code, $match);
        array_shift($match);
        return $match;
    }

    public function getSpot($spotId)
    {
        return $this->parking->getSpot($spotId);
    }

    /**
     * get types available for the street
     * @param  AppModelStreet $street [description]
     * @return array
     */
    public function loadParkingTypes(\App\Model\Street $street)
    {
        $parkingTypes = \App\Model\ParkingType::where('active', true)->get();
        $spotId = 1;

        if ($street->spots) {
            if ($street->spots->count() > 0) {
                $spotId = $street->spots->first()->id;
            }
        }

        $parkingTypes->each(function (&$type) use ($spotId) {
            $type->id = $spotId;
            $type->prices = [];
        });
        return $parkingTypes;
        $parkingTypes = [];
        $street->spots->each(function ($spot) use (&$parkingTypes) {
            if ($spot->amount > 0) {
                $parkingTypes[] = [
                    'id' => $spot->id,
                    'name' => $spot->type->name,
                    'prices' => $this->price->getForParkingType($spot->type)
                ];
            }
        });
        return $parkingTypes;
    }

    protected function getPaymentOptions()
    {
        if ($this->user->prePaidAvaiable()) {
            return $this->paymentMethod->all();
        }

        return $this->paymentMethod->allButPrePaid();
    }

    protected function getRules()
    {
        return Config::get('cad.cet.rules');
    }

    /**
     * Get street information for street
     * @param  string $code
     * @return array
     */
    public function loadFromCode($code, $meter)
    {
        $data = [];

        if (!empty($meter)) {
            $street = $this->street->getByCode($meter);
        } else {
            list($area, $sector, $face) = $this->sliceCode($code);
            $street = $this->street->getByInfo($area, $sector, $face);
        }

        if (!$street) {
            $street = $this->street->getFirstInfo();
        }

        if ($street) {
            $data['street'] = [
                'id' => $street->id,
                'name' => $street->name,
                'rule' => $street->rule->id,
                'area' => property_exists($street, 'area') ? $street->area->name : '',
                'lat' => session('user.lat'),
                'lon' => session('user.lon'),
            ];
            $data['parking_options'] = $this->loadParkingTypes($street);
            $data['payment_options'] = $this->getPaymentOptions();
            $data['rules'] = $this->getRules();
            $data['prices'] = $this->price->getForStreet($street);
            $data['stripe_code'] = Config::get('cad.stripe.publishable');
        } else {
            throw new \Exception("Rua não encontrada");
        }

        return $data;
    }

    protected function formatRuleForHours($rule)
    {
        $data = [
            'allowed' => false,
            'message' => '',
            'rule' => []
        ];

        if ($rule) {
            if ($rule->free || $rule->forbidden) {
                $reason = $rule->free ? 'livre' : 'proibida';
                $data['message'] = "Vaga {$reason} nesta data";
            } else {
                $data['allowed'] = true;
                $data['rule'] = [
                    'from' => $rule->from,
                    'to' => $rule->to
                ];
            }
        } else {
            $data['message'] = 'Regra não encontrada para a data';
        }

        return $data;
    }

    public function availableHoursForDate($timestamp, $ruleId)
    {
        $date = new DateTime();
        $today = new DateTime();
        $timestamp = $timestamp;
        $date->setTimestamp($timestamp);
        $interval = $today->diff($date);
        $difference = (int)$interval->format('%R%a');

        if ($date->getTimestamp() != $timestamp) {
            throw new GenericException($date->getTimestamp());
        } elseif ($difference < 0) {
            throw new GenericException("Data inválida");
        }

        $week = new Week();
        $dayInteger = $week->getDayIntegerFromDate($date);
        $rule = $this->rule->getHoursByDay($ruleId, $dayInteger);

        return $this->formatRuleForHours($rule);
    }
}
