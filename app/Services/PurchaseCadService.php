<?php

namespace App\Services;

use Illuminate\Container\Container as Application;
use App\Repository\UserRepository;
use App\Repository\PaymentMethodRepository;
use App\Repository\PriceRepository;
use App\Services\OrderService;
use App\Services\TicketService;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;

class PurchaseCadService
{
    protected $userRepository;
    protected $paymentMethodRepository;
    protected $priceRepository;
    protected $orderService;
    protected $ticketService;

    public function __construct(Application $app)
    {
        $this->userRepository = new UserRepository($app);
        $this->paymentMethodRepository = new PaymentMethodRepository($app);
        $this->priceRepository = new PriceRepository($app);
        $this->orderService = new OrderService($app);
        $this->ticketService = new TicketService($app);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle($document, $amount)
    {
        $orderData = $this->makeData($document, $amount);
        $orderId = $this->orderService->create($orderData);
        $ticket = $this->ticketService->create([
            'order_id' => $orderId,
            'amount' => 0
        ]);
    }

    protected function makeData($document, $amount)
    {
        $user = $this->getUser($document);
        $paymentMethod = $this->getPaymentMethod();
        $price = $this->getPrice();
        $plate = $this->getPlate($user);
        $creditCard = $this->getCreditCard($user);
        $data = [
            'payment_method_id' => $paymentMethod->id,
            'user_id' => $user->id,
            'plate_id' => $plate->id,
            'device_id' => str_random(40),
            'price_id' => $price->id,
            'user_credit_card_id' => $creditCard->id,
            'amount' => $amount
        ];
        return $data;
    }

    protected function getUser($document)
    {
        $user = $this->userRepository->getByDocument($document);

        if (!$user) {
            throw new GenericException("Usuário não encontrado");
        }

        return $user;
    }

    protected function getPaymentMethod()
    {
        $paymentMethod = $this->paymentMethodRepository->getByType('credit_card');

        if (!$paymentMethod) {
            throw new GenericException("Erro ao buscar forma de pagamento");
        }

        return $paymentMethod;
    }

    protected function getPrice()
    {
        $priceMethod = $this->priceRepository->getByAmount(10);

        if (!$priceMethod) {
            throw new GenericException("Erro ao buscar preço");
        }

        return $priceMethod;
    }

    protected function getPlate(\App\Model\User $user)
    {
        if ($user->plates->count() > 0) {
            return $user->plates->first();
        }

        $plateData = [
            'user_id' => $user->id,
            'number' => 'AAA1234'
        ];

        $user->plates()->create($plateData);
        return $user->plates->first();
    }

    protected function getCreditCard(\App\Model\User $user)
    {
        if ($user->cards->count() > 0) {
            return $user->cards->first();
        }

        $creditCardData = [
            'user_id' => $user->id,
            'last_digits' => '0000',
            'name' => 'Boleto',
            'type' => 'fa-cc-mastercard'
        ];

        $user->cards()->create($creditCardData);
        return $user->cards->first();
    }
}
