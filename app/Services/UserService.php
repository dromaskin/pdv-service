<?php

namespace App\Services;

use Illuminate\Container\Container as Application;
use App\Repository\UserRepository as User;
use App\Repository\GuestRepository as Guest;
use App\Repository\PlateRepository as Plate;
use App\Repository\OrderRepository as Order;
use App\Repository\ParkingRepository as Parking;
use App\Repository\CreditCardRepository as CreditCard;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;
use Config;
use Session;
use Auth;
use DB;
use Log;
use Sendinblue\Mailin;
use DateTime;

class UserService
{

    protected $user;
    protected $guest;
    protected $plate;
    protected $order;
    protected $parking;
    protected $card;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->user = new User($this->app);
        $this->plate = new Plate($this->app);
        $this->guest = new Guest($this->app);
        $this->card = new CreditCard($this->app);
    }

    public function order()
    {
        if (!$this->order) {
            $this->order = new Order($this->app);
        }

        return $this->order;
    }

    public function parking()
    {
        if (!$this->parking) {
            $this->parking = new Parking($this->app);
        }

        return $this->parking;
    }

    public function validateUpdateUser(array $data)
    {
        if (!$this->user->make($data)->validate()) {
            throw new ValidationException($this->user->errors());
        }
    }

    public function validate(array $data)
    {
        if (!$this->user->make($data)->validate()) {
            $error = new ValidationException();
            $error->setMessages($this->user->errors());
            throw $error;
        }
    }

    protected function validatePlate(array $data)
    {
        if (!$this->plate->make($data)->validate()) {
            $error = new ValidationException();
            $error->setMessages($this->plate->errors());
            throw $error;
        }
    }

    protected function savePlate($userId, $plate)
    {
        $data = [
            'user_id' => $userId,
            'number' => $plate
        ];

        $this->validatePlate($data);
        return $this->plate->save($data);
    }

    protected function sendPasswordEmail(\App\Model\User $user)
    {
        if (Session::has('user_new_password')) {
            $password = Session::pull('user_new_password');
            $email = \App\Services\MessageService::instance();
            $email->to($user->email, $user->name);
            $email->subject('Novo usuário');
            $email->message("Olá {$user->name}<br />Sua nova senha é <strong>{$password}</strong>");
            $email->email();
        }
    }

    protected function transformInputDataForUpdate(array $data)
    {
        if (empty($data['password'])) {
            unset($data['password']);
        } else {
            $data['password'] = bcrypt($data['password']);
        }

        $allowedFields = array_merge(['id'], $this->user->getFillable());
        return array_only($data, $allowedFields);
    }

    protected function transformInputData(array $data)
    {
        $allowedFields = $this->user->getFillable();
        return array_only($data, $allowedFields);
    }

    public function create(array $data)
    {
        $data = $this->transformInputData($data);
        $this->validate($data);
        DB::beginTransaction();
        try {
            $user = $this->user->save($data);
            $this->user->createToken($user);
            Auth::login($user, true);
            DB::commit();
            return $user;
        } catch (ValidationException $e) {
            DB::rollBack();
            throw $e;
        } catch (GenericException $e) {
            DB::rollBack();
            throw $e;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Error saving user');
            Log::error($e);
            throw new GenericException("Erro ao salvar usuário");
        }
    }

    public function save(array $data)
    {
        $data = $this->transformInputData($data);
        $this->validate($data);
        DB::beginTransaction();
        try {
            $plate = $data['plate'];
            unset($data['plate']);
            $user = $this->user->save($data);
            $plate = $this->savePlate($user->id, $plate);
            $this->sendPasswordEmail($user);
            Auth::login($user, true);
            DB::commit();

            return [
                'user_id' => $user->id,
                'plate_id' => $plate->id
            ];
        } catch (ValidationException $e) {
            DB::rollBack();
            throw $e;
        } catch (GenericException $e) {
            DB::rollBack();
            throw $e;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Error saving user');
            Log::error($e);
            throw new GenericException("Erro ao salvar usuário");
        }
    }

    public function update(array $data)
    {
        $data = $this->transformInputDataForUpdate($data);
        $this->validateUpdateUser($data);
        DB::beginTransaction();
        try {
            $user = $this->user->save($data);
            DB::commit();
            return true;
        } catch (ValidationException $e) {
            DB::rollBack();
            throw $e;
        } catch (GenericException $e) {
            DB::rollBack();
            throw $e;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Error updating user');
            Log::error($e);
            throw new GenericException("Erro ao atualizar usuário");
        }
    }

    protected function getGuestInfo(\App\Model\User &$user)
    {
        if (Session::has('user.guest.id')) {
            $user->name = Session::get('user.guest.name');
            $user->token = Session::get('user.guest.token');
            $user->last_name = '';
            $user->guest = true;
        }
    }

    public function getInfo()
    {
        $user = [];

        if (Auth::check()) {
            $user = Auth::user();
            $this->getGuestInfo($user);
            $user->plates = $user->plates()
                ->where('active', true)
                ->orderBy('number', 'desc')
                ->get();
            $user->cards->each(function (&$card) {
                $card->discount = $this->card->getCardDiscount($card->id);
            });
            $user->credits = $this->user->credits($user->id);
            $user->device_id = Session::get('user.device_id');
            $user->actives = $this->getActiveParkings(Session::get('user.device_id'));
            $user->schedules = $this->getScheduledParkings();
            $user = $user->toArray();
            $needs = [];
        } elseif (Session::has('user')) {
            $user = Session::get('user');
        } elseif (Session::has('email')) {
            $user = ['email' => Session::get('email')];
        }

        $fillable = array_filter($this->user->getFillable(), function ($field) {
            return $field != 'password';
        });

        if (!isset($needs)) {
            $needs = array_filter($fillable, function ($field) use ($user) {
                if (array_key_exists($field, $user)) {
                    if (!empty($user[$field]) || is_numeric($user[$field])) {
                        return false;
                    }
                }
                return true;
            });
        }

        $data = [
            'user' => $user,
            'needs' => array_values($needs)
        ];

        return $data;
    }

    protected function authGuest($login, $password)
    {
        $guests = $this->guest->findByLogin($login);

        if ($guests) {
            foreach ($guests as $guest) {
                if (\Hash::check($password, $guest->password)) {
                    return $guest;
                }
            }
        }

        return false;
    }

    public function login($email, $password)
    {
        if (Auth::attempt([
            'email' => $email,
            'password' => $password,
            'active' => true
        ], true)) {
            Session::put('user.guest', false);
            $this->user->createToken(Auth::user());
            return $this->getInfo();
        } else {
            $guest = $this->authGuest($email, $password);

            if ($guest) {
                $this->guest->createToken($guest);
                Auth::login($guest->owner);
                Session::put('user.guest', $guest->toArray());
                return $this->getInfo();
            }
        }

        return false;
    }

    public function logout()
    {
        Auth::logout();
        Session::forget('user');
        return Auth::check();
    }

    public function prePaidAvaiable()
    {
        if (Auth::check()) {
            return $this->user->hasCredit(Auth::user()->id);
        }

        return false;
    }

    public function getUserPlate($userId, $plate)
    {
        $plate = $this->savePlate($userId, $plate);
        return $plate->id;
    }

    public function loginByEmail($email)
    {
        $user = $this->user->getByEmail($email);
        if ($user) {
            Auth::login($user, true);
            return true;
        }
        return false;
    }

    protected function calculateTimeForParking(\App\Model\Parking $parking)
    {
        $from = strtotime($parking->valid_from);
        $to = strtotime($parking->valid_to);
        return (($to - $from)/60)/$parking->amount;
    }

    public function getOrders($period)
    {
        $user = Auth::user();
        $orders = $this->user->getOrders($user, $period);
        $orderList = [];
        $orders->each(function ($o) use (&$orderList) {
            $order['id'] = $o->id;
            $order['price'] = $o->price->price;
            $order['method'] = $o->method->title;
            $order['amount'] = $o->amount;
            $order['created'] = $o->created_at->format('Y-m-d H:i:s');
            $order['pre_charger'] = $o->price->pre_charger;

            if (!$o->price->pre_charger && $o->plate) {
                $order['plate'] = $o->plate->number;
            }

            if ($o->parking) {
                $order['date'] = $o->parking->valid_from;
                $order['amount'] = $o->parking->amount;
                $order['time'] = $this->calculateTimeForParking($o->parking);
            }

            $orderList[] = $order;
        });
        return $orderList;
    }

    protected function getTimeForRule($rule)
    {
        $rule = (int) $rule;
        switch ($rule) {
            case 1:
                return 30;
            case 2:
                return 60;
            case 3:
                return 120;
            case 4:
                return 180;
            default:
                throw new ValidationException("Erro ao verificar regra de estacionamenbto", 1);
        }
    }

    public function getOrder($id)
    {
        $order = Auth::user()->orders()->find($id);

        if (!$order) {
            return false;
        }

        $config = Config::get('cad.receipt');
        $config['auth'] = $order->token;
        $config['plate'] = $order->plate->number;
        $config['price'] = $this->order()->getPrice($order);
        $config['method'] = $order->method->title;
        $config['amount'] = $order->amount;
        $config['is_discount'] = !empty($order->creditCard->card_company_id);

        if ($order->parking) {
            $config['street'] = $order->parking->spot ?
                $order->parking->spot->street->name :
                false;
            $config['amount'] = $order->parking->amount;
            $config['hours_changed'] = $order->parking->hours_changed;
            $config['cancel_another'] = $order->parking->cancel_another;
            $config['parking_status'] = $order->parking->status;
            $config['extension_id'] = $order->parking->extension_id;

            if ($order->parking->extension_id && false) {
                $extension = $this->parking()->find($order->parking->extension_id);
                $config['date'] = $config['from'] = $extension->valid_from;
                $config['to'] = $extension->valid_to;
            } else {
                $config['date'] = $config['from'] = $order->parking->valid_from;
                $config['to'] = $order->parking->valid_to;
            }
            $config['time'] = $this->getTimeForRule($order->parking->rule);
        }
        return $config;
    }

    public function credits($userId)
    {
        return $this->user->credits($userId);
    }

    public function checkParking(array $data)
    {
        $response = [
            'allowed' => false,
            'message' => '',
            'blocked' => false,
            'cancel_another' => false
        ];

        if ($this->userExceededActivePlates($data)) {
            $response['blocked'] = true;
            $response['message'] = "Você já possui 3 placas com CADs ativos. " .
                "Não é permitido fazer uma nova ativação";
        } elseif ($this->userExceededActiveTickets($data)) {
            $response['message'] = "Você ultrapassou o limite de cads ativos para " .
                "a mesma placa. Os CADs anteriores serão cancelados, confirma?";
            $response['cancel_another'] = true;
        } elseif ($this->isDifferentRule($data)) {
            $response['message'] = "Existe um CAD ativado em outra regra " .
                "de estacionamento. Esta compra será cancelada para ativar o novo " .
                "CAD, confirma?";
            $response['cancel_another'] = true;
        } elseif ($this->userHasActiveTicketForPlate($data)) {
            $order = $this->getUserActiveTicketForPlate($data)->first();
            $validFrom = (new DateTime($order->getValidTo('Y-m-d H:i:s')))
                ->getTimestamp() * 1000;
            $response['message'] = "Você tem um CAD ativado. " .
                "O novo CAD será ativado em seguida. Confirma?";
            $response['valid_from'] = $validFrom;
            $response['extension_id'] = $order->id;
        } else {
            $response['allowed'] = true;
        }

        return $response;
    }

    public function isDifferentRule(array $data)
    {
        $orders = $this->parking()->listActivated([
            'plate' => $data['plate'],
            'not_rule' => $data['rule'],
            'valid_from' => $data['valid_from'],
            'valid_to' => $data['valid_to']
        ]);

        return $orders > 0;
    }

    public function userHasActiveTicketForPlate(array $data)
    {
        $orders = $this->parking()->listActivated([
            'plate' => $data['plate'],
            'rule' => $data['rule'],
            'valid_from' => $data['valid_from'],
            'valid_to' => $data['valid_to']
        ]);

        return $orders > 0;
    }

    public function getUserActiveTicketForPlate(array $data)
    {
        return $this->parking()->listActivated([
            'plate' => $data['plate'],
            'rule' => $data['rule'],
            'valid_from' => $data['valid_from'],
            'valid_to' => $data['valid_to']
        ], false);
    }

    public function userExceededActiveTickets(array $data)
    {
        $options = [
            'plate' => $data['plate'],
            'valid_from' => $data['valid_from'],
            'valid_to' => $data['valid_to']
        ];

        $amount = $this->parking()->listActivated($options);
        $parkings = $this->parking()->listActivated($options, false);

        $parkings->each(function ($parking) use (&$amount) {
            if ($parking->extension_id) {
                $extension = $this->parking()->find($parking->extension_id);
                if ($extension) {
                    $amount+= $parking->amount;
                }
            }
        });

        return ($data['amount'] + $amount) > 2;
    }

    public function userExceededActivePlates(array $data)
    {
        $plates = $this->parking()->activatedPlatesForDevice([
            'device_id' => $data['device_id'],
            'valid_from' => $data['valid_from'],
            'valid_to' => $data['valid_to'],
            'plate' => $data['plate']
        ]);

        return $plates >= 3;
    }

    public function getActiveParkings($deviceId)
    {
        return $this->user->activeParkings($deviceId);
    }

    public function getScheduledParkings()
    {
        return $this->user->scheduledParkings(Auth::user());
    }

    public function getActiveParking($orderId)
    {
        $parking = $this->user->activeParking($orderId);

        if (!$parking) {
            throw new GenericException("Esta vaga não está mais ativa");
        }

        return $parking;
    }

    public function removePlate($plateId)
    {
        $user = Auth::user();
        return $this->user->removePlate($user, $plateId);
    }

    protected function sendRecoverPasswordEmail($to, $token)
    {
        $url = url("/recuperar-senha?tk={$token}");
        $message = "Olá. <br />Uma solicitação de recuperação de senha foi feita.";
        $message.= "<br />Clique <a href='{$url}'>aqui</a> para recuperá-la.";
        $text = "Caso o link de recuração de senha não funcione, copie e cole";
        $text.= "o link abaixo no navegador\n{$url}";
        $email = \App\Services\MessageService::instance();
        $email->to($to, 'Zona Azul Digital');
        $email->subject('Recuperação de senha');
        $email->message($message);
        $email->text($text);
        $email->email();
    }

    public function recoverEmail($email)
    {
        $user = $this->user->getByEmail($email);

        if (!$user) {
            throw new GenericException("Email não encontrado");
        }

        $token = $this->user->createTokenRecover($user);

        $this->sendRecoverPasswordEmail($email, $token);

        return true;
    }

    public function updatePassword($password, $token)
    {

        return $this->user->updatePassword($password, $token);
    }
}
