<?php

namespace App\Services;

use Illuminate\Container\Container as Application;
use App\Repository\PriceRepository as Price;
use App\Validate;
use Config;

class PriceService {

    protected $app;
    protected $price;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->price = new Price($this->app);
    }

    /**
     * Return price table
     * @return array
     */
    public function getPriceTable()
    {
        return $this->price->get();
    }

    /**
     * return the time per ticket
     * @param  AppModelStreet $street
     * @return integer
     */
    public function getTimeForTicket(\App\Model\Street $street)
    {
        $rule = $street->rule ? $street->rule : $street->area->rule;

        if (!$rule) {
            throw new Exception("Nenhuma regra foi aplicada para essa rua");
        }

        $maxHours = $rule->max_time/60;
        return $maxHours/Config::get('cad.cet.max_cad');
    }

    protected function getHoursForPrice(
        \App\Model\Price $price,
        $timeForTicket
    ) {
        return ($timeForTicket * 60) * $price->amount;
    }

    /**
     * return the title description for a price
     * @param  AppModelPrice $price         [description]
     * @param  [type]        $maximumTime   [description]
     * @param  [type]        $timeForTicket [description]
     * @return string
     */
    public function getTitleForPrice(
        \App\Model\Price $price,
        $timeForTicket
    ) {
        $title = 'Até ';
        $type = 'hora';

        $time = ($timeForTicket * $price->amount);

        if ($time < 1) {
            $time*= 60;
            $type = 'minutos';
        }

        // if ($price->amount > 1) {
        //     $previousTime = ($timeForTicket * ($price->amount-1));
        //
        //     if ($previousTime < 1) {
        //         $previousTime*= 60;
        //         $previousTime.= " minutos";
        //     }
        //
        //     $title = "De {$previousTime} até ";
        // }

        $title.= "{$time} {$type}" . ($time > 1 && $type == 'hora' ? 's' : '');
        return $title;
    }

    /**
     * return Ticket options for street
     * @param  AppModelStreet $street
     * @return array
     */
    public function getForStreet(\App\Model\Street $street)
    {
        $prices = $this->getPriceTable();
        $timeForTicket = $this->getTimeForTicket($street);

        $prices->each(function (&$price) use ($timeForTicket) {
            if (empty($price->title)) {
                $price->title = $this->getTitleForPrice($price, $timeForTicket);
                $price->time = $this->getHoursForPrice($price, $timeForTicket);
            }
        });

        return $prices;
    }

    public function getForParkingType(\App\Model\ParkingType $type)
    {
        if ($type->slug == 'caminhao') {
            $prices = $this->getPriceTable();
            $timeForTicket = 0.5;

            $prices->each(function (&$price) use ($timeForTicket) {
                if (empty($price->title)) {
                    $price->title = $this->getTitleForPrice($price, $timeForTicket);
                    $price->time = $this->getHoursForPrice($price, $timeForTicket);
                }
            });

            return $prices;
        } else {
            return [];
        }
    }

    public function getTicketTimeForParkingSpot(\App\Model\ParkingSpot $spot)
    {
        if ($spot->type->slug == 'caminhao') {
            return 30;
        }

        return $spot->street->rule->max_time/Config::get('cad.cet.max_cad');
    }
}
