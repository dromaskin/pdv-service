<?php

namespace App\Services;

use Illuminate\Container\Container as Application;
use App\Repository\OrderTransactionRepository as OrderTransaction;
use App\Repository\ParkingRepository as Parking;
use App\Repository\OrderRepository;
use App\Repository\RuleRepository as Rule;
use App\Repository\TaskRepository as Task;
use App\Repository\UserRepository;
use App\Repository\HolidayRepository;
use App\Repository\PaymentMethodRepository as PaymentMethod;
use App\Services\PriceService as Price;
use App\Services\UserService as User;
use App\Services\OrderService as Order;
use App\Cet\Methods\Unlock;
use App\Cet\Methods\Activate;
use App\Cet\Methods\ActivateUnlock;
use App\Exceptions\GenericException;
use App\Exceptions\ValidationException;
use App\Validate;
use DB;
use Log;
use Session;

class TicketService
{

    protected $app;
    protected $transaction;
    protected $parking;
    protected $order;
    protected $orderService;
    protected $rule;
    protected $task;
    protected $price;
    protected $user;
    protected $userRepository;
    protected $holiday;
    protected $paymentMethod;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->transaction = new OrderTransaction($this->app);
        $this->parking = new Parking($this->app);
        $this->order = new OrderRepository($this->app);
        $this->orderService = new Order($this->app);
        $this->rule = new Rule($this->app);
        $this->task = new Task($this->app);
        $this->price = new Price($this->app);
        $this->user = new User($this->app);
        $this->paymentMethod = new PaymentMethod($this->app);
        $this->userRepository = new UserRepository($this->app);
        $this->holiday = new HolidayRepository($this->app);
    }

    public function cancelScheduled($orderId)
    {
        $order = $this->order->find($orderId);

        if (!$order) {
            throw new GenericException("Erro ao buscar dados da vaga");
        } elseif (!$order->parking) {
            throw new GenericException("Erro ao buscar dados de estacionamento");
        }

        $scheduled = $this->userRepository->scheduledParking($orderId);

        if (!$scheduled) {
            throw new GenericException("Essa vaga não pode ser cancelada");
        }

        $parking = $order->parking;
        $this->parking->updateStatus($parking, 'unlocked', 'canceled');
        $this->parking->detachCredits($parking);
        $this->parking->removeTask($parking);
        return true;
    }

    public function validate(array $data)
    {
        if (isset($data['user_id'])) {
            $data['amount'] = $data['order']['amount'];
            $this->orderService->validateUserCredits($data);
        }

        $dataParking = array_merge($data, $data['ticket']);

        if ($dataParking['amount'] > 0) {
            $this->validateParking($dataParking);
            $dataParking['amount'] = $data['order']['amount'];
            return $this->user->checkParking($dataParking);
        }

        return ['allowed' => true];
    }

    protected function validateHoliday(array $data)
    {
        $date = new \DateTime($data['valid_from']);
        $isHoliday = $this->holiday->isHolidayDate($date);

        if ($isHoliday) {
            throw new GenericException("Não há necessidade ativação de CADs " .
                "para esta quadra no período selecionado.<br /> Verifique a " .
                "sinalização local para certificação de que a ativação está " .
                "sendo realizada dentro do horário de funcionamento correto.");
        }
    }

    protected function getUser(array $data)
    {
        if (!isset($data['user_id'])) {
            throw new GenericException("Ativação da vaga - Usuário inválido");
        }

        $user = $this->userRepository->find($data['user_id']);

        if (!$user) {
            throw new GenericException("Ativação da vaga - Usuário não encontrado");
        }

        return $user;
    }

    protected function getPaymentMethod(array $data)
    {
        if (!isset($data['payment_method_id'])) {
            throw new GenericException("Ativação da vaga - Método de pagamento inválido");
        }

        $method = $this->paymentMethod->find($data['payment_method_id']);

        if (!$method) {
            throw new GenericException("Ativação da vaga - Método de pagamento não encontrado");
        }

        return $method;
    }

    public function validateParking(array &$data, \App\Model\Order $order = null)
    {
        if (isset($data['valid_from'])) {
            if (isset($data['extension_id'])) {
                $parking = $this->parking->find($data['extension_id']);
                if ($parking) {
                    $from = new \DateTime($parking->valid_to);
                    $data['valid_from'] = $from->format('Y-m-d H:i:s');
                    $data['valid_to'] = $from->modify("+{$this->getMinutes($data)} minutes")->format('Y-m-d H:i:s');
                }
            } elseif (empty($data['valid_from'])) {
                throw new GenericException("Horário de saída inválido");
            } elseif (!(isset($data['amount']) && is_numeric($data['amount']))) {
                throw new GenericException("Quantidade de CADs inválida");
            } elseif (!isset($data['parking_spot_id'])) {
                throw new GenericException("Local da vaga é obrigatório");
            } elseif ($data['valid_from'] == 'now') {
                $now = new \DateTime();
                $data['valid_from'] = $now->format('Y-m-d H:i:s');
                $data['valid_to'] = $now->modify("+{$this->getMinutes($data)} minutes")->format('Y-m-d H:i:s');
            }
        }

        $this->validateHoliday($data);

        if (!empty($order)) {
            if (Session::has('user.guest.id')) {
                $data['guest_id'] = Session::get('user.guest.id');
            }

            $data['user_id'] = $order->user_id;
            $parking = $this->parking->make($data);
            if (!$this->parking->isAllowedToCreate($order)) {
                throw new GenericException("Estacionamento já vinculado a outra compra");
            }
        } else {
            $parking = $this->parking->make($data);
            $parking->removeRule('order_id');
            $parking->removeRule('user_id');
        }

        if (!$parking->validate()) {
            throw new ValidationException($this->parking->errors());
        }
    }

    protected function getMinutes(array $data)
    {
        $spot = $this->parking->getSpot($data['parking_spot_id']);
        $time = $this->price->getTicketTimeForParkingSpot($spot);
        return ((int) $data['amount']) * $time;
    }

    public function create(array $data)
    {
        $order = $this->getOrder($data);

        if ($data['amount'] > 0) {
            // Compra de cad com ativação
            $parking = $this->save($data, $order);

            if ($order->method->pre_paid) {
                // Compra usando crédito pré-pago
                if ($data['valid_from'] == 'now' || $data['extension_id']) {
                    // Ativa cads
                    $this->createActivate($parking, false);
                }
            } else {
                // Compra de cad com pagamento
                if ($order->amount == $parking->amount) {
                    if ($data['valid_from'] == 'now' || is_numeric($data['extension_id'])) {
                        // Ativação junto com desbloqueio
                        $this->createActivateUnlock($parking);
                    } else {
                        // Desbloqueia cads
                        $this->createUnlock($order, $parking);
                    }
                } else {
                    // Desbloqueia cads
                    $this->createUnlock($order, $parking);

                    if ($data['valid_from'] == 'now' || is_numeric($data['extension_id'])) {
                        $this->createActivate($parking, false);
                    }
                }
            }

            if ($data['valid_from'] != 'now' && !is_numeric($data['extension_id'])) {
                // Programa cad para ativar futuramente
                $this->createTask($parking);
            }
        } else {
            // compra cad sem ativar
            $this->createUnlock($order);
        }

        return $order->id;
    }

    protected function getOrder(array $data)
    {
        if (!isset($data['order_id'])) {
            throw new GenericException("Ativação da vaga - Compra inválida");
        }

        $order = $this->order->find($data['order_id']);

        if (!$order) {
            throw new GenericException("Ativação da vaga - Compra não encontrada");
        }

        return $order;
    }

    protected function createTask(\App\Model\Parking $parking)
    {
        return $this->task->save([
            'type' => 'ticket',
            'target_id' => $parking->id,
            'at_time' => $parking->valid_from
        ]);
    }

    protected function save(array $data, \App\Model\Order $order)
    {
        DB::beginTransaction();
        try {
            $this->validateParking($data, $order);
            $return = $this->parking->save($data);
            DB::commit();
            return $return;
        } catch (ValidationException $e) {
            DB::rollBack();
            throw $e;
        } catch (GenericException $e) {
            DB::rollBack();
            throw $e;
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('Error saving parking');
            Log::error($e);
            throw new GenericException("Erro ao ativar vaga");
        }
    }

    protected function makeTransaction($orderId)
    {
        $data = [
            'status' => false,
            'message' => '',
            'order_id' => $orderId,
            'type' => 'credit',
            'data' => null
        ];

        return $this->transaction->make($data)->create();
    }

    protected function saveTransaction($transactionId, $status, $message, $data)
    {
        $data = [
            'status' => $status,
            'message' => $message,
            'id' => $transactionId,
            'type' => 'credit',
            'data' => $data
        ];

        return $this->transaction->save($data);
    }

    protected function makeActivateDataFromParking(
        \App\Model\Parking $parking,
        $transactionId,
        $amount = null
    ) {
        $data = [
            'transactionID' => $transactionId,
            'documentType' => strlen($parking->user->document) == 11 ? 'cpf' : 'cnpj',
            'document' => $parking->user->document,
            'deviceIDType' => 'udid',
            'deviceID' => $parking->order->device_id,
            'area' => $parking->spot ? substr($parking->spot->street->area->code, -2) : null,
            'sector' => $parking->spot ? $parking->spot->street->sector : null,
            'side' => $parking->spot ? $parking->spot->street->face : null,
            'lat' => $parking->lat,
            'lon' => $parking->lon,
            'plate' => $parking->order->plate->number,
            'time' => $parking->getTimeForRule($parking->rule),
            'amount' => $amount ? $amount : $parking->amount
        ];
        return $data;
    }

    protected function sendActivate(\App\Model\Parking $parking, $transactionId)
    {
        $ticketData = $this->makeActivateDataFromParking($parking, $transactionId);
        $activate = new Activate();
        $status = false;
        $message = '';
        $data = null;
        $token = false;

        try {
            $result = $activate->send($ticketData);
            $data = json_encode($result);
            if (!empty($result['autenticacao']) && $result['sucesso'] == 'true') {
                $status = true;
                $token = $message = $result['autenticacao'];
            } else {
                $message = $result['mensagem'];
            }
        } catch (\App\Cet\Exceptions\ConnectException $e) {
            $message = $e->getMessage();
        } catch (\App\Cet\Exceptions\InvalidException $e) {
            $message = $e->getMessage();
        } catch (\Exception $e) {
            Log::error('cet activate error');
            Log::error($e->getMessage());
            Log::error($e);
            $message = $e->getMessage();
        }

        return [$status, $message, $data, $token];
    }

    public function createActivate(\App\Model\Parking $parking, $scheduled = true)
    {
        $transaction = $this->makeTransaction($parking->order->id);

        try {
            list($status, $message, $data, $token) = $this->sendActivate($parking, $transaction->id);
        } catch (\Exception $e) {
            list($status, $message, $data, $token) = [false, $e->getMessage(), null, null];
        }

        $this->saveTransaction($transaction->id, $status, $message, $data);

        if (!$status) {
            $this->parking->updateStatus($parking, 'unlocked', 'canceled');
            $this->parking->detachCredits($parking);

            if (!$scheduled) {
                throw new ValidationException(['Erro ao ativar vaga', $message]);
            }

            return false;
        }

        if (!$scheduled) {
            $this->parking->attachCredits($parking, 'unlocked');
        }

        $this->order->saveToken($parking->order, $token);
        $this->parking->updateStatus($parking, 'activated');
        $this->parking->updateValidFrom($parking);
        return true;
    }

    protected function makeUnlockDataFromOrder(
        \App\Model\Order $order,
        $transactionId,
        $amount
    ) {
        return [
            'documentType' => strlen($order->user->document) == 11 ? 'cpf' : 'cnpj',
            'document' => $order->user->document,
            'transactionID' => $transactionId,
            'amount' => $amount
        ];
    }

    protected function sendUnlock(\App\Model\Order $order, $transactionId, $amount)
    {
        $ticketData = $this->makeUnlockDataFromOrder($order, $transactionId, $amount);
        $unlock = new Unlock();
        $status = false;
        $message = '';
        $data = null;
        $token = false;

        try {
            $result = $unlock->send($ticketData);
            $data = json_encode($result['params']);
            if (!empty($result['autenticacao']) && $result['sucesso'] == 'true') {
                $status = true;
                $token = $message = $result['autenticacao'];
            } else {
                $message = $result['mensagem'];
            }
        } catch (\App\Cet\Exceptions\ConnectException $e) {
            $message = $e->getMessage();
        } catch (\App\Cet\Exceptions\InvalidException $e) {
            $message = $e->getMessage();
        } catch (\Exception $e) {
            Log::error('cet activate error');
            Log::error($e->getMessage());
            Log::error($e);
            $message = $e->getMessage();
        }

        return [$status, $message, $data, $token];
    }

    protected function createUnlock(\App\Model\Order $order, \App\Model\Parking $parking = null)
    {
        if (!$order->method->pre_paid) {
            $transaction = $this->makeTransaction($order->id);

            try {
                list($status, $message, $data, $token) = $this->sendUnlock($order, $transaction->id, $order->amount);
            } catch (\Exception $e) {
                list($status, $message, $data, $token) = [false, $e->getMessage(), null, null];
            }

            $this->saveTransaction($transaction->id, $status, $message, $data);

            if (!$status) {
                $this->order->cancelCredits($order, $order->amount);

                if (!empty($parking)) {
                    $this->parking->updateStatus($parking, 'canceled');
                }

                throw new ValidationException(['Erro ao ativar vaga', $message]);
            }

            $this->order->saveToken($order, $token);
            $this->order->unlockCredits($order, $order->amount);
        }

        return true;
    }

    protected function sendActivateUnlock(\App\Model\Parking $parking, $transactionId, $amount = null)
    {
        $ticketData = $this->makeActivateDataFromParking($parking, $transactionId, $amount);
        $activate = new ActivateUnlock();
        $status = false;
        $message = '';
        $data = null;
        $token = false;

        try {
            $result = $activate->send($ticketData);
            $data = json_encode($result);
            if (!empty($result['autenticacao']) && $result['sucesso'] == 'true') {
                $status = true;
                $token = $message = $result['autenticacao'];
            } else {
                $message = $result['mensagem'];
            }
        } catch (\App\Cet\Exceptions\ConnectException $e) {
            $message = $e->getMessage();
        } catch (\App\Cet\Exceptions\InvalidException $e) {
            $message = $e->getMessage();
        } catch (\Exception $e) {
            Log::error('cet activate error');
            Log::error($e->getMessage());
            Log::error($e);
            $message = $e->getMessage();
        }

        return [$status, $message, $data, $token];
    }

    public function createActivateUnlock(\App\Model\Parking $parking)
    {
        $transaction = $this->makeTransaction($parking->order->id);

        try {
            list($status, $message, $data, $token) = $this->sendActivateUnlock(
                $parking,
                $transaction->id,
                $parking->amount
            );
        } catch (\Exception $e) {
            list($status, $message, $data, $token) = [false, $e->getMessage(), null, null];
        }

        $this->saveTransaction($transaction->id, $status, $message, $data);

        if (!$status) {
            $this->order->cancelCredits($parking->order);
            $this->parking->updateStatus($parking, 'canceled');
            throw new ValidationException(['Erro ao ativar vaga', $message]);
        }

        $this->parking->attachCredits($parking, 'created', true);
        $this->parking->updateStatus($parking, 'activated');
        $this->parking->updateValidFrom($parking);
        $this->order->saveToken($parking->order, $token);
        return true;
    }
}
