<?php

namespace App\Exceptions;

class ValidationException extends \Exception {

    protected $messages = [];

    public function __construct($message = null, $code = 0)
    {
        if (!is_array($message)) {
            $message = [$message];
        }

        $this->setMessages($message);
        parent::__construct($message[0], $code);
    }

    public function setMessages(array $messages)
    {
        $this->messages = $messages;
    }

    public function getMessages()
    {
        return $this->messages;
    }

}
