<?php

namespace App;

class Constants {

    // pattern that matches street code
    const STREET_CODE = '([aA-zZ]{3}\d{1,2})(\d{2})([aA-zZ])';

    // pattern for valid credit cards
    const CC_AMEX = '^3[47][0-9]{13}$';
    const CC_VISA = '^4[0-9]{12}(?:[0-9]{3})?$';
    const CC_MASTER = '^5[1-5][0-9]{14}$';
}
