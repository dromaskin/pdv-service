<?php

namespace App;

use App\Constants;

class Validate {

    public static function email($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public static function plate($number)
    {
        return !!preg_match('/^[aA-zZ]{3}\d{4}$/', $number);
    }

    public static function cpf($cpf)
    {
        if(empty($cpf)) {
            return false;
        }

        if (!is_numeric($cpf)) {
            return false;
        }

        if (strlen($cpf) != 11) {
            return false;
        } else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999') {
            return false;
        } else {
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
            return true;
        }
    }

    public static function cnpj($cnpj)
    {
        if(empty($cnpj)) {
            return false;
        }

        if (!is_numeric($cnpj)) {
            return false;
        }

        if (strlen($cnpj) != 14) {
            return false;
        }

        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto)) {
            return false;
        }

        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }
        $resto = $soma % 11;
        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }

    public static function document($document)
    {
        $length = strlen($document);

        if ($length == 11) {
            return self::cpf($document);
        } else if ($length == 14) {
            return self::cnpj($document);
        }

        return false;
    }

    public static function IMEI($imei, $use_checksum = false)
    {
    	if (is_string($imei) && !empty($imei)) {
            if (preg_match('/^[0-9]{15}$/', $imei)) {
    			if (!$use_checksum) return true;
    			for ($i = 0, $sum = 0; $i < 14; $i++) {
    				$tmp = $imei[$i] * (($i % 2) + 1);
    				$sum+= ($tmp % 10) + intval($tmp / 10);
    			}
    			return (((10 - ($sum % 10)) % 10) == $imei[14]);
    		}
    	}
    	return false;
    }

    public static function UDID($udid)
    {
        return strlen($udid) >= 15;
    }

    public static function numericString($string, $positions = null, $min = null, $max = null)
    {
        $valid = !empty($string) && is_numeric($string);


        if (!$valid) {
            return false;
        }

        if ($positions) {
            $valid = strlen($string) == $positions;
        }

        if (!$valid) {
            return false;
        }

        if ($min) {
            $valid = $string >= $min;
        }

        if (!$valid) {
            return false;
        }

        if ($max) {
            $valid = $string <= $max;
        }

        return $valid;
    }

    public static function phoneNumber($number)
    {
        return preg_match('/^\d{10,11}$/', $number);
    }

    public static function creditCard($number)
    {
        return (preg_match('/' . Constants::CC_VISA . '/', $number) ||
            preg_match('/' . Constants::CC_AMEX . '/', $number) ||
            preg_match('/' . Constants::CC_MASTER . '/', $number));
    }
}
