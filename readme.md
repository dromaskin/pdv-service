# Parkme - CAD - PDV API

## Configuration

To setup the parkme configuration use the environment configuration below

```
DB_CONNECTION=mysql
DB_HOST=IP_ADDRESS
DB_PORT=PORT
DB_DATABASE=DATABASE_NAME
DB_USERNAME=USERNAME
DB_PASSWORD=PASSWORD

PARKME_API=http://api.parkme.com
PARKME_PUB_ID=1

CET_API=https://apphml.cetsp.com.br/webservices/cad/wsDistribuidora.asmx
CET_DIST_COD=91
CET_DIST_NOME=NAME
CET_PASS=PASSWORD
CET_CERT=CERTIFICATION_PATH
```

## Setup

Install the follow command on crontab
```
* * * * * php /home/vagrant/Sites/parkme-cad/services/artisan tasks:run >/dev/null 2>&1
```

## Installing certification

Copy certification file

```
cp CAROOTCAD.cer /usr/local/share/ca-certificates/cet.cer
```

Update the certificate system
```
sudo update-ca-certificates
```

## Deployment

The deployment run when the production repository is pushed  
To run deployment on production execute the command below

```
sh /var/www/deploy/parkme-services
```

## Test

To test application execute `phpunit` on root folder

## Database

```
ALTER TABLE user_credit_cards ADD card_company_id int(10) UNSIGNED default null after user_id,
	ADD CONSTRAINT user_credit_cards_user_id_foreign FOREIGN KEY (card_company_id) REFERENCES card_companies(id);
ALTER TABLE parkings ADD street_id int(10) UNSIGNED default null after order_id,
	ADD CONSTRAINT parkings_street_id_foreign FOREIGN KEY (street_id) REFERENCES streets(id);
```
