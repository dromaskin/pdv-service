<?php

namespace Tests\Model;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Model\User;

class UserTest extends \TestCase
{

    protected $class;

    public function setUp()
    {
        parent::setUp();
        $this->class = new User;
    }

    public function testClassExists()
    {
        $this->assertTrue(class_exists('App\Model\User'));
    }

    public function testUserValidation()
    {
        $this->assertFalse($this->class->isValid());
        $this->class->name = "Lucas";
        $this->assertFalse($this->class->isValid());
        $this->class->last_name = "Andrade";
        $this->assertFalse($this->class->isValid());
        $this->class->document = '123';
        $this->assertFalse($this->class->isValid());
        $this->class->document = '3582705889';
        $this->assertFalse($this->class->isValid());
        $this->class->document = '35827058890';
        $this->assertTrue($this->class->isValid());
    }
}
