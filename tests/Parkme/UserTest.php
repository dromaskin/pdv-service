<?php

namespace Tests\Parkme;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Parkme\User;

class UserTest extends \TestCase
{

    protected $class;

    public function setUp()
    {
        parent::setUp();
        $this->class = new User;
    }

    public function testClassExists()
    {
        $this->assertTrue(class_exists('App\Parkme\User'));
    }

    public function testEndPoint()
    {
        $this->assertEquals('/user', $this->class->getEndpoint());
    }
}
