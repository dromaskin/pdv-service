<?php

namespace Tests;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Validate;

class ValidateTest extends \TestCase
{

    protected $class;

    public function setUp()
    {
        parent::setUp();
    }

    public function testClassExists()
    {
        $this->assertTrue(class_exists('App\Validate'));
    }

    public function testCPF()
    {
        $this->assertFalse(Validate::cpf(''));
        $this->assertFalse(Validate::cpf('726823764'));
        $this->assertTrue(Validate::cpf('35827058890'));
        $this->assertFalse(Validate::cpf('a35827058890'));
    }

    public function testCNPJ()
    {
        $this->assertFalse(Validate::cnpj(''));
        $this->assertFalse(Validate::cnpj('726823764'));
        $this->assertFalse(Validate::cnpj('35827058890'));
        $this->assertFalse(Validate::cnpj('a35827058890'));
        $this->assertTrue(Validate::cnpj('35229266000195'));
    }
}
