<?php

namespace Tests\Cet;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Cet\Api;
use Config;

class ApiTest extends \TestCase
{

    protected $class;

    public function setUp()
    {
        parent::setUp();
        $this->class = new Api;
    }

    public function testClassExists()
    {
        $this->assertTrue(class_exists('App\Cet\Api'));
    }

    public function testConfiguration()
    {
        $this->assertEquals($this->class->getUrl(), Config::get('cad.cet.api_url'));
        $this->assertEquals($this->class->getCertificate(), Config::get('cad.cet.certificate'));
        $this->assertEquals($this->class->getPassword(), Config::get('cad.cet.password'));
    }

    public function testConnection()
    {
        try {
            $this->class->connect();
            $this->assertTrue(true);
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            $this->assertTrue(false);
        }
    }
}
