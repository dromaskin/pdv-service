<?php

namespace Tests\Cet\Validator;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Cet\Validator\Unlock;
use Config;

class UnlockTest extends \TestCase
{

    protected $class;

    public function setUp()
    {
        parent::setUp();
        $this->class = new Unlock;
    }

    public function testClassExists()
    {
        $this->assertTrue(class_exists('App\Cet\Validator\Unlock'));
    }

    public function testRequiredParams()
    {
        $this->assertFalse($this->class->validateRequiredParams());
        $this->class->setParams([
            'document' => '123'
        ]);
        $this->assertFalse($this->class->validateRequiredParams());
        $this->class->setParams([
            'document' => '123',
            'documentType' => 'teste'
        ]);
        $this->assertFalse($this->class->validateRequiredParams());
        $this->class->setParams([
            'document' => '123',
            'documentType' => 'teste',
            'transacationID' => 1
        ]);
        $this->assertFalse($this->class->validateRequiredParams());
        $this->class->setParams([
            'document' => '123',
            'documentType' => 'teste',
            'transacationID' => 1,
            'amount' => 123
        ]);
        $this->assertTrue($this->class->validateRequiredParams());
    }

    public function testAmount()
    {
        $this->class->setParams([
            'amount' => 'qw'
        ]);
        $this->assertFalse($this->class->amount());
        $this->class->setParams([
            'amount' => 0
        ]);
        $this->assertFalse($this->class->amount());
        $this->class->setParams([
            'amount' => 11
        ]);
        $this->assertFalse($this->class->amount());
        $this->class->setParams([
            'amount' => 10
        ]);
        $this->assertTrue($this->class->amount());
    }

    public function testDocumentType()
    {
        $this->class->setParams([
            'documentType' => 'teste'
        ]);
        $this->assertFalse($this->class->documentType());
        $this->class->setParams([
            'documentType' => 'cpf'
        ]);
        $this->assertTrue($this->class->documentType());
        $this->class->setParams([
            'documentType' => 'cnpj'
        ]);
        $this->assertTrue($this->class->documentType());
    }

    public function testDocument()
    {
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => 'teste'
        ]);
        $this->assertFalse($this->class->document());
        $this->class->setParams([
            'documentType' => 'cnpj',
            'document' => 'teste'
        ]);
        $this->assertFalse($this->class->document());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890'
        ]);
        $this->assertTrue($this->class->document());
        $this->class->setParams([
            'documentType' => 'cnpj',
            'document' => '35827058890'
        ]);
        $this->assertFalse($this->class->document());
        $this->class->setParams([
            'documentType' => 'cnpj',
            'document' => '35229266000195'
        ]);
        $this->assertTrue($this->class->document());
    }

    public function testTransactionID()
    {
        $this->class->setParams([
            'transacationID' => 'wdf'
        ]);
        $this->assertFalse($this->class->transacationID());
        $this->class->setParams([
            'transacationID' => '123d'
        ]);
        $this->assertFalse($this->class->transacationID());
        $this->class->setParams([
            'transacationID' => '3459678'
        ]);
        $this->assertTrue($this->class->transacationID());
    }

    public function testValidation()
    {
        $this->class->setParams([
            'documentType' => 'cnpj',
            'document' => '35229266000195',
            'transacationID' => '3459678',
            'amount' => 5
        ]);
        $this->assertTrue($this->class->validate());
    }
}
