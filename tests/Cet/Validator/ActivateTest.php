<?php

namespace Tests\Cet\Validator;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Cet\Validator\Activate;
use Config;

class ActivateTest extends \TestCase
{

    protected $class;

    public function setUp()
    {
        parent::setUp();
        $this->class = new Activate;
    }

    public function testClassExists()
    {
        $this->assertTrue(class_exists('App\Cet\Validator\Activate'));
    }

    public function testDocumentType()
    {
        $this->class->setParams([
            'documentType' => 'teste'
        ]);
        $this->assertFalse($this->class->documentType());
        $this->class->setParams([
            'documentType' => 'cpf'
        ]);
        $this->assertTrue($this->class->documentType());
        $this->class->setParams([
            'documentType' => 'cnpj'
        ]);
        $this->assertTrue($this->class->documentType());
    }

    public function testDocument()
    {
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => 'teste'
        ]);
        $this->assertFalse($this->class->document());
        $this->class->setParams([
            'documentType' => 'cnpj',
            'document' => 'teste'
        ]);
        $this->assertFalse($this->class->document());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890'
        ]);
        $this->assertTrue($this->class->document());
        $this->class->setParams([
            'documentType' => 'cnpj',
            'document' => '35827058890'
        ]);
        $this->assertFalse($this->class->document());
        $this->class->setParams([
            'documentType' => 'cnpj',
            'document' => '35229266000195'
        ]);
        $this->assertTrue($this->class->document());
    }

    public function testAmount()
    {
        $this->class->setParams([
            'amount' => 3
        ]);
        $this->assertFalse($this->class->amount());
        $this->class->setParams([
            'amount' => 0
        ]);
        $this->assertFalse($this->class->amount());
        $this->class->setParams([
            'amount' => 1
        ]);
        $this->assertTrue($this->class->amount());
        $this->class->setParams([
            'amount' => 2
        ]);
        $this->assertTrue($this->class->amount());
    }

    public function testDeviceIDType()
    {
        $this->class->setParams([
            'deviceIDType' => 'teste'
        ]);
        $this->assertFalse($this->class->deviceIDType());
        $this->class->setParams([
            'deviceIDType' => ''
        ]);
        $this->assertFalse($this->class->deviceIDType());
        $this->class->setParams([
            'deviceIDType' => 'udid'
        ]);
        $this->assertTrue($this->class->deviceIDType());
        $this->class->setParams([
            'deviceIDType' => 'imei'
        ]);
        $this->assertTrue($this->class->deviceIDType());
    }

    public function testDeviceID()
    {
        $this->class->setParams([
            'deviceIDType' => 'udid',
            'deviceID' => ''
        ]);
        $this->assertFalse($this->class->deviceID());
        $this->class->setParams([
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb5'
        ]);
        $this->assertFalse($this->class->deviceID());
        $this->class->setParams([
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b'
        ]);
        $this->assertTrue($this->class->deviceID());
        $this->class->setParams([
            'deviceIDType' => 'imei',
            'deviceID' => ''
        ]);
        $this->assertFalse($this->class->deviceID());
        $this->class->setParams([
            'deviceIDType' => 'imei',
            'deviceID' => '86adftgasd'
        ]);
        $this->assertFalse($this->class->deviceID());
        $this->class->setParams([
            'deviceIDType' => 'imei',
            'deviceID' => '10291892553270'
        ]);
        $this->assertFalse($this->class->deviceID());
        $this->class->setParams([
            'deviceIDType' => 'imei',
            'deviceID' => '102918925532707'
        ]);
        $this->assertTrue($this->class->deviceID());
    }

    public function testArea()
    {
        $this->class->setParams([
            'area' => ''
        ]);
        $this->assertFalse($this->class->area());
        $this->class->setParams([
            'area' => 'a'
        ]);
        $this->assertFalse($this->class->area());
        $this->class->setParams([
            'area' => 'aa'
        ]);
        $this->assertFalse($this->class->area());
        $this->class->setParams([
            'area' => 'a1'
        ]);
        $this->assertFalse($this->class->area());
        $this->class->setParams([
            'area' => '1a'
        ]);
        $this->assertFalse($this->class->area());
        $this->class->setParams([
            'area' => '1'
        ]);
        $this->assertFalse($this->class->area());
        $this->class->setParams([
            'area' => '00'
        ]);
        $this->assertFalse($this->class->area());
        $this->class->setParams([
            'area' => '76'
        ]);
        $this->assertFalse($this->class->area());
        $this->class->setParams([
            'area' => '75'
        ]);
        $this->assertTrue($this->class->area());
        $this->class->setParams([
            'area' => '01'
        ]);
        $this->assertTrue($this->class->area());
    }

    public function testSector()
    {
        $this->class->setParams([
            'sector' => ''
        ]);
        $this->assertFalse($this->class->sector());
        $this->class->setParams([
            'sector' => 'a'
        ]);
        $this->assertFalse($this->class->sector());
        $this->class->setParams([
            'sector' => 'aa'
        ]);
        $this->assertFalse($this->class->sector());
        $this->class->setParams([
            'sector' => 'a1'
        ]);
        $this->assertFalse($this->class->sector());
        $this->class->setParams([
            'sector' => '1a'
        ]);
        $this->assertFalse($this->class->sector());
        $this->class->setParams([
            'sector' => '1'
        ]);
        $this->assertFalse($this->class->sector());
        $this->class->setParams([
            'sector' => '00'
        ]);
        $this->assertFalse($this->class->sector());
        $this->class->setParams([
            'sector' => '12'
        ]);
        $this->assertFalse($this->class->sector());
        $this->class->setParams([
            'sector' => '11'
        ]);
        $this->assertTrue($this->class->sector());
        $this->class->setParams([
            'sector' => '01'
        ]);
        $this->assertTrue($this->class->sector());
    }

    public function testSide()
    {
        $this->class->setParams([
            'side' => 1
        ]);
        $this->assertFalse($this->class->side());
        $this->class->setParams([
            'side' => '11'
        ]);
        $this->assertFalse($this->class->side());
        $this->class->setParams([
            'side' => 'a'
        ]);
        $this->assertFalse($this->class->side());
        $this->class->setParams([
            'side' => 'aa'
        ]);
        $this->assertFalse($this->class->side());
        $this->class->setParams([
            'side' => 'a1'
        ]);
        $this->assertFalse($this->class->side());
        $this->class->setParams([
            'side' => 'AAA'
        ]);
        $this->assertFalse($this->class->side());
        $this->class->setParams([
            'side' => 'A'
        ]);
        $this->assertTrue($this->class->side());
    }

    public function testLat()
    {
        $this->class->setParams([
            'lat' => 'as'
        ]);
        $this->assertFalse($this->class->lat());
        $this->class->setParams([
            'lat' => 12
        ]);
        $this->assertFalse($this->class->lat());
        $this->class->setParams([
            'lat' => 12.5
        ]);
        $this->assertTrue($this->class->lat());
        $this->class->setParams([
            'lat' => -12.5
        ]);
        $this->assertTrue($this->class->lat());
    }

    public function testLon()
    {
        $this->class->setParams([
            'lon' => 'as'
        ]);
        $this->assertFalse($this->class->lon());
        $this->class->setParams([
            'lon' => 12
        ]);
        $this->assertFalse($this->class->lon());
        $this->class->setParams([
            'lon' => 12.5
        ]);
        $this->assertTrue($this->class->lon());
        $this->class->setParams([
            'lon' => -12.5
        ]);
        $this->assertTrue($this->class->lon());
    }

    public function testPlate()
    {
        $this->class->setParams([
            'plate' => ''
        ]);
        $this->assertFalse($this->class->plate());
        $this->class->setParams([
            'plate' => 'aaz2222'
        ]);
        $this->assertFalse($this->class->plate());
        $this->class->setParams([
            'plate' => 'sss4444'
        ]);
        $this->assertFalse($this->class->plate());
        $this->class->setParams([
            'plate' => 'SSS444a'
        ]);
        $this->assertFalse($this->class->plate());
        $this->class->setParams([
            'plate' => 'SSS4444'
        ]);
        $this->assertTrue($this->class->plate());
    }

    public function testTime()
    {
        $this->class->setParams([
            'time' => 'aa'
        ]);
        $this->assertFalse($this->class->time());
        $this->class->setParams([
            'time' => '12'
        ]);
        $this->assertFalse($this->class->time());
        $this->class->setParams([
            'time' => '30'
        ]);
        $this->assertTrue($this->class->time());
        $this->class->setParams([
            'time' => '60'
        ]);
        $this->assertTrue($this->class->time());
        $this->class->setParams([
            'time' => '120'
        ]);
        $this->assertTrue($this->class->time());
        $this->class->setParams([
            'time' => '180'
        ]);
        $this->assertTrue($this->class->time());
    }

    public function testTransactionID()
    {
        $this->class->setParams([
            'transacationID' => 'wdf'
        ]);
        $this->assertFalse($this->class->transacationID());
        $this->class->setParams([
            'transacationID' => '123d'
        ]);
        $this->assertFalse($this->class->transacationID());
        $this->class->setParams([
            'transacationID' => '3459678'
        ]);
        $this->assertTrue($this->class->transacationID());
    }

    public function testValidate()
    {
        $this->class->setParams([
            'documentType' => 'cp',
            'document' => '35827058890',
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b',
            'area' => '01',
            'sector' => '01',
            'side' => 'A',
            'lat' => -12.5,
            'lon' => -12.5,
            'plate' => 'SSS4444',
            'time' => '180',
            'amount' => 2,
            'transacationID' => '3459678'
        ]);
        $this->assertFalse($this->class->validate());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '3582705889',
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b',
            'area' => '01',
            'sector' => '01',
            'side' => 'A',
            'lat' => -12.5,
            'lon' => -12.5,
            'plate' => 'SSS4444',
            'time' => '180',
            'amount' => 2,
            'transacationID' => '3459678'
        ]);
        $this->assertFalse($this->class->validate());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890',
            'deviceIDType' => 'udi',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b',
            'area' => '01',
            'sector' => '01',
            'side' => 'A',
            'lat' => -12.5,
            'lon' => -12.5,
            'plate' => 'SSS4444',
            'time' => '180',
            'amount' => 2,
            'transacationID' => '3459678'
        ]);
        $this->assertFalse($this->class->validate());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890',
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54',
            'area' => '01',
            'sector' => '01',
            'side' => 'A',
            'lat' => -12.5,
            'lon' => -12.5,
            'plate' => 'SSS4444',
            'time' => '180',
            'amount' => 2,
            'transacationID' => '3459678'
        ]);
        $this->assertFalse($this->class->validate());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890',
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b',
            'area' => '0',
            'sector' => '01',
            'side' => 'A',
            'lat' => -12.5,
            'lon' => -12.5,
            'plate' => 'SSS4444',
            'time' => '180',
            'amount' => 2,
            'transacationID' => '3459678'
        ]);
        $this->assertFalse($this->class->validate());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890',
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b',
            'area' => '01',
            'sector' => '0',
            'side' => 'A',
            'lat' => -12.5,
            'lon' => -12.5,
            'plate' => 'SSS4444',
            'time' => '180',
            'amount' => 2,
            'transacationID' => '3459678'
        ]);
        $this->assertFalse($this->class->validate());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890',
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b',
            'area' => '01',
            'sector' => '01',
            'side' => '1',
            'lat' => -12.5,
            'lon' => -12.5,
            'plate' => 'SSS4444',
            'time' => '180',
            'amount' => 2,
            'transacationID' => '3459678'
        ]);
        $this->assertFalse($this->class->validate());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890',
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b',
            'area' => '01',
            'sector' => '01',
            'side' => 'A',
            'lat' => 12,
            'lon' => -12.5,
            'plate' => 'SSS4444',
            'time' => '180',
            'amount' => 2,
            'transacationID' => '3459678'
        ]);
        $this->assertFalse($this->class->validate());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890',
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b',
            'area' => '01',
            'sector' => '01',
            'side' => 'A',
            'lat' => -12.5,
            'lon' => 'A',
            'plate' => 'SSS4444',
            'time' => '180',
            'amount' => 2,
            'transacationID' => '3459678'
        ]);
        $this->assertFalse($this->class->validate());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890',
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b',
            'area' => '01',
            'sector' => '01',
            'side' => 'A',
            'lat' => -12.5,
            'lon' => -12.5,
            'plate' => 'SSS444',
            'time' => '180',
            'amount' => 2,
            'transacationID' => '3459678'
        ]);
        $this->assertFalse($this->class->validate());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890',
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b',
            'area' => '01',
            'sector' => '01',
            'side' => 'A',
            'lat' => -12.5,
            'lon' => -12.5,
            'plate' => 'SSS4444',
            'time' => '181',
            'amount' => 2,
            'transacationID' => '3459678'
        ]);
        $this->assertFalse($this->class->validate());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890',
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b',
            'area' => '01',
            'sector' => '01',
            'side' => 'A',
            'lat' => -12.5,
            'lon' => -12.5,
            'plate' => 'SSS4444',
            'time' => '180',
            'amount' => 12,
            'transacationID' => '3459678'
        ]);
        $this->assertFalse($this->class->validate());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890',
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b',
            'area' => '01',
            'sector' => '01',
            'side' => 'A',
            'lat' => -12.5,
            'lon' => -12.5,
            'plate' => 'SSS4444',
            'time' => '180',
            'amount' => 2,
            'transacationID' => 'a'
        ]);
        $this->assertFalse($this->class->validate());
        $this->class->setParams([
            'documentType' => 'cpf',
            'document' => '35827058890',
            'deviceIDType' => 'udid',
            'deviceID' => '0e83ff56a12a9cf0c7290cbb08ab6752181fb54b',
            'area' => '01',
            'sector' => '01',
            'side' => 'A',
            'lat' => -12.5,
            'lon' => -12.5,
            'plate' => 'SSS4444',
            'time' => '180',
            'amount' => 2,
            'transacationID' => '3459678'
        ]);
        $this->assertTrue($this->class->validate());
    }
}
