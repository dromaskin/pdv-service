<?php

namespace Tests\Cet;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Cet\Date;
use Config;

class DateTest extends \TestCase
{

    protected $class;

    public function setUp()
    {
        parent::setUp();
    }

    public function testClassExists()
    {
        $this->assertTrue(class_exists('App\Cet\Date'));
    }

    public function testDate()
    {
        $this->assertEquals(Date::get(), date('Y-m-d\TH:i:s'));
    }
}
