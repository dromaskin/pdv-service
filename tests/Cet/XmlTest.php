<?php

namespace Tests\Cet;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Cet\Xml;

class XmlTest extends \TestCase
{

    protected $class;

    public function setUp()
    {
        parent::setUp();
    }

    public function testClassExists()
    {
        $this->assertTrue(class_exists('App\Cet\Xml'));
    }

    public function testXml()
    {
        $out = '<teste>1</teste><outroteste>2</outroteste>';
        $this->assertEquals($out, Xml::toString([
            'teste' => 1,
            'outroteste' => 2
        ]));
        $out = '<root><teste>1</teste><outroteste>2</outroteste></root>';
        $this->assertEquals($out, Xml::toString([
            'teste' => 1,
            'outroteste' => 2
        ], 'root'));
        $out = '<root><teste><mais>123</mais></teste><outroteste>2</outroteste></root>';
        $this->assertEquals($out, Xml::toString([
            'teste' => [
                'mais' => 123
            ],
            'outroteste' => 2
        ], 'root'));
    }
}
