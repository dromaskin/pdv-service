<?php

use Illuminate\Database\Seeder;

use App\Model\PaymentMethod;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'Cartão de Crédito',
                'slug' => 'credit_card',
                'style' => 'fa-credit-card',
                'order' => 1
            ],
            [
                'title' => 'Crédito Pré-pago',
                'slug' => 'pre_paid',
                'style' => 'fa-btc',
                'order' => 2,
                'pre_paid' => 1
            ]
        ];

        array_map(function ($payment) {
            PaymentMethod::create($payment);
        }, $data);
    }
}
