<?php

use Illuminate\Database\Seeder;

use App\Model\ParkingType;

class ParkingTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Convencional',
                'slug' => 'convencional'
            ],
            [
                'name' => 'Idoso',
                'slug' => 'idoso'
            ],
            [
                'name' => 'Deficiente',
                'slug' => 'deficiente'
            ],
            [
                'name' => 'Caminhão',
                'slug' => 'caminhao'
            ],
            [
                'name' => 'Fretamento',
                'slug' => 'fretamento'
            ]
        ];
        array_map(function ($type) {
            ParkingType::create($type);
        }, $data);
    }
}
