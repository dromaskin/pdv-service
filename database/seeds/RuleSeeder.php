<?php

use Illuminate\Database\Seeder;

use App\Model\Rule;
use App\Model\RuleSchedule;
use App\Lucandrade\Week;

class RuleSeeder extends Seeder
{

    const WEEK_DAYS = 31;
    const SATURDAY = 32;
    const SUNDAY = 64;

    protected $week;

    protected function getWeek()
    {
        if (!$this->week) {
            $this->week = new Week();
        }

        return $this->week;
    }

    protected function getData()
    {
        $filePath = base_path("files/regras.csv");
        $csv = array_map('str_getcsv', file($filePath));
        return $csv;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = $this->getData();
        array_map(function ($ruleData) {
            $this->checkRule($ruleData);
        }, $data);
    }

    protected function checkRule($ruleData)
    {
        $code = end($ruleData);
        $rule = Rule::where('code', $code)->first();

        if (!$rule) {
            $name = $ruleData[8];
            $maxTime = (int) $ruleData[5];
            $rule = Rule::create([
                'name' => $name,
                'code' => $code,
                'max_time' => $maxTime*60,
                'active' => true
            ]);
            $this->makeSchedule($ruleData[2], self::WEEK_DAYS, $rule->id);
            $this->makeSchedule($ruleData[3], self::SATURDAY, $rule->id);
            $this->makeSchedule($ruleData[4], self::SUNDAY, $rule->id);
        }
    }

    protected function makeSchedule($days, $integer, $ruleId)
    {
        $data = [
            'from' => null,
            'to' => null,
            'forbidden' => false,
            'free' => false,
            'days' => $integer,
            'rule_id' => $ruleId,
            'active' => true
        ];
        $days = strtolower($days);
        if ($days == 'liberado') {
            $data['free'] = true;
        } else if ($days == 'proibido') {
            $data['forbidden'] = true;
        } else {
            preg_match('/(\d{1,2})h\s{0,2}-\s{0,2}(\d{1,2})h/', $days, $match);
            if (count($match) == 3) {
                $data['from'] = $match[1];
                $data['to'] = $match[2];
            }
        }

        RuleSchedule::create($data);
    }
}
