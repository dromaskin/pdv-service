<?php

use Illuminate\Database\Seeder;

use App\Model\Price;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => '',
                'amount' => 1,
                'price' => 5.0,
                'order' => 1
            ],
            [
                'title' => '',
                'amount' => 2,
                'price' => 10.0,
                'order' => 2
            ],
            [
                'title' => 'Crédito Pré-Pago',
                'amount' => 10,
                'price' => 45.0,
                'order' => 3,
                'pre_charger' => true
            ]
        ];

        array_map(function ($price) {
            Price::create($price);
        }, $data);
    }
}
