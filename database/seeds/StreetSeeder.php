<?php

use Illuminate\Database\Seeder;

use App\Util;
use App\Model\Area;
use App\Model\Street;
use App\Model\Rule;
use App\Model\ParkingType;
use App\Model\ParkingSpot;

class StreetSeeder extends Seeder
{

    protected $types;

    protected function getData()
    {
        $filePath = base_path("files/completa.csv");
        $csv = array_map('str_getcsv', file($filePath));
        return $csv;
    }

    protected function getTypes()
    {
        if (!$this->types) {
            $this->types = ParkingType::pluck('id', 'slug');
        }

        return $this->types;
    }

    protected function getRule($code)
    {
        return Rule::where('code', $code)->first();
    }

    protected function makeArea($name, $code)
    {
        return Area::firstOrCreate([
            'name' => $name,
            'code' => substr($code, 0, 5)
        ]);
    }

    protected function makeParkingSpot(Street $street, $amount, $type)
    {
        $parkingSpot = new ParkingSpot([
            'amount' => $amount,
            'parking_type_id' => $this->getTypes()[$type]
        ]);
        $parkingSpot->street()->associate($street);
        $parkingSpot->save();
    }

    protected function makeParkingSpots(Street $street, $csvData)
    {
        $this->makeParkingSpot($street, $csvData[13], 'convencional');
        $this->makeParkingSpot($street, $csvData[11], 'idoso');
        $this->makeParkingSpot($street, $csvData[12], 'deficiente');
        $this->makeParkingSpot($street, $csvData[14], 'caminhao');
        $this->makeParkingSpot($street, $csvData[15], 'fretamento');
    }

    protected function makeStreet($csvData, $line)
    {
        $street = new Street([
            'sector' => $csvData[1],
            'face' => $csvData[2],
            'name' => $csvData[7],
            'short_name' => $csvData[8],
        ]);
        $street->area()->associate($this->makeArea($csvData[4], $csvData[5]));
        $street->rule()->associate($this->getRule($csvData[6]));
        $street->save();
        $this->makeParkingSpots($street, $csvData);
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = $this->getData();
        $types = $this->getTypes();

        // 1 - Setor
        // 2 - Face
        // 3 - Area
        // 4 - Rua
        // 5 - Nome curto
        // 8 - vaga idosos
        // 9 - vaga defis
        // 10 - vaga convenc
        // 11 - vaga caminhao
        // 12 - vaga fretamento
        while (list($key, $street) = each($data)) {
            $this->makeStreet($street, $key);
        }
    }
}
