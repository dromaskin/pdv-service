<?php

use Illuminate\Database\Seeder;

use App\Model\Holiday;

class HolidaySeeder extends Seeder
{

    protected function getData()
    {
            $filePath = base_path("files/holidays.json");
            $data = json_decode(file_get_contents($filePath), true);
            return $data;
    }

    protected function transformData(array &$data)
    {
        if (isset($data['holidays'])) {
            $data = array_map(function ($holiday) {
                if (array_key_exists(0, $holiday)) {
                    return $holiday[0];
                }
                throw new Exception("Error Processing Request");
            }, $data['holidays']);
        } else {
            throw new Exception("Error Processing Request");
        }
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = $this->getData();
        $this->transformData($data);
        array_map(function ($holiday) {
            $this->checkHoliday($holiday);
        }, $data);
    }

    protected function checkHoliday(array $data)
    {
        $name = $data['name'];
        $holiday = Holiday::where('name', $name)->count();

        if ($holiday == 0) {
            Holiday::create(array_only($data, ['name', 'date']));
        }
    }
}
