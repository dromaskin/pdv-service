<?php

use Illuminate\Database\Seeder;

class BinTableSeeder extends Seeder
{
    protected $companyName = 'Bradesco';
    protected $discount = 10;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = $this->getCompany();
        $data = $this->getBins($company->id);
        array_map(function ($bin) {
            $this->insertBin($bin);
        }, $data);
    }

    protected function getCompany()
    {
        $company = \DB::table('card_companies')->where('name', $this->companyName)->first();

        if ($company) {
            return $company;
        }

        if (\DB::table('card_companies')->insert([
            'name' => $this->companyName,
            'discount' => $this->discount
        ])) {
            return \DB::table('card_companies')->where('name', $this->companyName)->first();
        }

        throw new Exception("Erro ao buscar company");
    }

    protected function getBins($companyId)
    {
        $filePath = base_path("files/lista_bin.csv");
        $data = str_getcsv(file_get_contents($filePath));
        return array_map(function ($bin) use ($companyId) {
            return [
                'card_company_id' => $companyId,
                'number' => trim($bin)
            ];
        }, $data);
    }

    protected function insertBin(array $data)
    {
        $bin = DB::table('card_company_bins')->where($data)->first();

        if (!$bin) {
            DB::table('card_company_bins')->insert($data);
        }
    }
}
