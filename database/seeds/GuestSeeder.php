<?php

use Illuminate\Database\Seeder;

use App\Model\User;
use App\Model\Guest;

class GuestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();
        Guest::create([
            'user_id' => $user->id,
            'name' => 'Convidado',
            'login' => 'convidado',
            'password' => bcrypt('convidado')
        ]);
    }
}
