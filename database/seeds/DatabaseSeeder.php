<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ParkingTypeSeeder::class);
        // $this->call(PaymentMethodSeeder::class);
        $this->call(PriceSeeder::class);
        $this->call(RuleSeeder::class);
        $this->call(StreetSeeder::class);
    }
}
