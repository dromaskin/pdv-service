<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('message')->nullable();
            $table->integer('discount')->default(0);
            $table->timestamps();
        });
        Schema::create('card_company_bins', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('card_company_id')->unsigned();
            $table->foreign('card_company_id')
                ->references('id')
                ->on('card_companies')
                ->onDelete('cascade');
            $table->string('number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('card_company_bins');
        Schema::drop('card_companies');
    }
}
