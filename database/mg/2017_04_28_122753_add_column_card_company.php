<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCardCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_credit_cards', function (Blueprint $table) {
            $table->integer('card_company_id')
                ->after('user_id')
                ->nullable()
                ->unsigned();
            $table->foreign('card_company_id')
                ->references('id')
                ->on('card_companies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
