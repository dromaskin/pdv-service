<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveOrdersCreditsTable extends Migration
{

    protected function getMigrationClass()
    {
        return new CreateOrdersCreditsTable;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getMigrationClass()->down();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getMigrationClass()->up();
    }
}
