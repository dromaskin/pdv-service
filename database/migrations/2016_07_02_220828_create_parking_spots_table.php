<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingSpotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_spots', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('street_id')->unsigned();
            $table->foreign('street_id')
                ->references('id')
                ->on('streets')
                ->onDelete('cascade');
            $table->integer('parking_type_id')->unsigned();
            $table->foreign('parking_type_id')
                ->references('id')
                ->on('parking_types')
                ->onDelete('cascade');
            $table->integer('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parking_spots');
    }
}
