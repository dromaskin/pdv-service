<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingsCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parkings_credits', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('parking_id')->unsigned();
            $table->foreign('parking_id')
                ->references('id')
                ->on('parkings')
                ->onDelete('cascade');
            $table->integer('credit_id')->unsigned();
            $table->foreign('credit_id')
                ->references('id')
                ->on('credits')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parkings_credits');
    }
}
