<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePaymentMethodsOrdersTable extends Migration
{

    protected function getMigrationClass()
    {
        return new CreatePaymentMethodsTable;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['payment_method_id']);
            $table->dropColumn('payment_method_id');
            $table->dropColumn('token');
            $table->integer('user_credit_card_id')
                ->unsigned()
                ->nullable()
                ->after('price_id');
            $table->foreign('user_credit_card_id')
                ->references('id')
                ->on('user_credit_cards')
                ->onDelete('cascade');
        });
        $this->getMigrationClass()->down();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->getMigrationClass()->up();
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['user_credit_card_id']);
            $table->dropColumn('user_credit_card_id');
            $table->integer('payment_method_id')->unsigned();
            $table->foreign('payment_method_id')
                ->references('id')
                ->on('payment_methods')
                ->onDelete('cascade');
            $table->string('token')->nullable();
        });
    }
}
