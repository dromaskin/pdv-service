<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRulesSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rule_schedules', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('rule_id')->unsigned();
            $table->foreign('rule_id')
                ->references('id')
                ->on('rules')
                ->onDelete('cascade');
            $table->integer('days');
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->boolean('all_day')->default(false);
            $table->boolean('forbidden')->default(false);
            $table->boolean('free')->default(false);
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rule_schedules');
    }
}
