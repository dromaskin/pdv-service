<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `tasks` CHANGE `type` `type` " .
            "ENUM('sms','ticket') CHARACTER SET utf8 COLLATE " .
            "utf8_unicode_ci NOT NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE `tasks` CHANGE `type` `type` " .
            "ENUM('sms') CHARACTER SET utf8 COLLATE " .
            "utf8_unicode_ci NOT NULL;");
    }
}
