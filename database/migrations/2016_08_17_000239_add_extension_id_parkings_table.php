<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtensionIdParkingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parkings', function (Blueprint $table) {
            $table->dropColumn('extensao');
            $table->integer('extension_id')
                ->unsigned()
                ->nullable()
                ->after('parking_spot_id');
            $table->foreign('extension_id')
                ->references('id')
                ->on('parkings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parkings', function (Blueprint $table) {
            $table->dropForeign(['extension_id']);
            $table->dropColumn('extension_id');
            $table->boolean('extensao')
                ->after('rule')
                ->default(false);
        });
    }
}
