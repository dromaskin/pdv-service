<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('plate_id')
                ->after('payment_method_id')
                ->unsigned();
            $table->foreign('plate_id')
                ->references('id')
                ->on('plates')
                ->onDelete('cascade');
            $table->dropColumn('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['plate_id']);
            $table->enum('type', ['activate', 'unlock', 'activate_unlock']);
        });
    }
}
