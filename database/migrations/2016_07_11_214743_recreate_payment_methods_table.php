<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecreatePaymentMethodsTable extends Migration
{

    protected function getMigrationClass()
    {
        return new CreatePaymentMethodsTable;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->getMigrationClass()->up();
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('payment_method_id')->after('user_id')->unsigned();
            $table->foreign('payment_method_id')
                ->references('id')
                ->on('payment_methods')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['payment_method_id']);
            $table->dropColumn('payment_method_id');
        });
        $this->getMigrationClass()->down();
    }
}
