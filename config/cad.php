<?php

$instructions = <<<EOT
O usuário deve sempre observar a placa de sinalização para verificar o
horário de funcionamento e o tempo de validade do Cartão. Como regra geral 1
Cartão é válido para estacionar o veículo por 1 hora, no entanto, em alguns
locais o tempo de validade do Cartão é diferenciado. É permitido usar no máximo
2 Cartões na mesma vaga, sendo obrigatória a retirada do veículo ao término
do período. Para os casos em que o horário do comprovante for anterior ao
horário que consta da placa de sinalização, prevalecerá a informação
constante da sinalização. Este comprovante de pagamento não precisa ser
deixado no painel do veículo.
EOT;

return [
    'receipt' => [
        'cnpj' => 'CNPJ',
        'name' => 'RAZAO',
        'address' => 'Endereço',
        'zip' => 'xxxxx-xxx',
        'instructions' => $instructions,
        'short_phone' => '1188',
        'phone' => '3059-7300 <b>ou ligue no Fone 1188</b>',
        // 'email' => 'dce@cetsp.com.br',
        'email' => 'suporte@parkme.com.br',
        'site' => 'http://www.cetsp.com.br',
        'title' => 'ESTACIONAMENTO ROTATIVO SÃO PAULO CARTÃO AZUL DIGITAL',
        'law' => 'Estacionamento Regulamentado pelos Decretos no 11.661 de 30/12/74 e no 17.115 de 05/01/81'
    ],
    'parkme' => [
        'site_url' => env('PARKME_URL', 'https://parkme.com'),
        'api_url' => env('PARKME_API', 'http://api.parkme.com'),
        'pub_id' => env('PARKME_PUB_ID', 1),
        'user_test' => [
            'email' => 'lucasandradedeveloper@gmail.com',
            'password' => 'testing123'
        ]
    ],
    'cet' => [
        'api_url' => env('CET_API', null),
        'cod_publisher' => env('CET_DIST_COD', null),
        'name_publisher' => env('CET_DIST_NOME', null),
        'password' => env('CET_PASS', null),
        'certificate' => env('CET_CERT', null),
        'max_cad' => 2,
        'rules' => [
            [
                'text' => 'Regra Geral',
                'time' => 60,
                'number' => 2
            ],
            [
                'text' => 'Bolsão Caminhão',
                'time' => 30,
                'number' => 1
            ],
            [
                'text' => 'Pq Ibirapuera / Pq Aclimação',
                'time' => 120,
                'number' => 3
            ],
            [
                'text' => 'Pç Charles Miller',
                'time' => 180,
                'number' => 4
            ]
        ]
    ],
    'sms' => [
        'from' => env('SMS_FROM', 'Parkme CAD - DEV'),
        'country_code' => env('SMS_COUNTRY', '55'),
        'url' => env('SMS_URL'),
        'user' => env('SMS_USER'),
        'pass' => env('SMS_PASS')
    ],
    'email' => [
        'url' => env('SENDINBLUE_URL'),
        'key' => env('SENDINBLUE_KEY'),
        'from' => env('EMAIL_FROM', 'noreply@parkme.com.br'),
        'name' => env('EMAIL_NAME', 'Parkme Brasil')
    ],
    'stripe' => [
        'currency' => 'brl',
        'publishable' => env('STRIPE_PK', false),
        'secret' => env('STRIPE_SK', false),
    ],
    'admin' => [
        'secret' => env('ADMIN_SECRET', 'parkme_adm_secret'),
    ],
];
