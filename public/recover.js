(function ($) {
    $(document).ready(function () {
        var form = $('form'),
            password = $('[name="password"]'),
            confirmPassword = $('[name="password_confirm"]'),
            button = $('button'),
            alert = $('.alert'),
            token;

        if (window.location.search) {
            token = window.location.search.replace('?tk=', '');
        }

        form.submit(function (e) {
            e.preventDefault();

            var valid = validateForm();

            if (valid) {
                button.attr('disabled', 'disabled').text('Enviando');
                $.ajax({
                    url: '/api/update-password',
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        password: password.val(),
                        token: token
                    },
                    success: function (response) {
                        if (response.status) {
                            alert.text('Senha atualizada com sucesso')
                                .attr('class', 'alert alert-success');
                        } else {
                            alert.html(response.message ||
                                'Erro ao atualizar senha')
                                .attr('class', 'alert alert-danger');
                        }
                    },
                    error: function () {
                        alert.text('Erro ao conectar ao servidor')
                            .attr('class', 'alert alert-danger');
                    },
                    complete: function() {
                        button.removeAttr('disabled').text('Enviar');
                    }
                });
            }
        });

        $(':input').keyup(function () {
            var valid = validateForm();
            if (valid) {
                button.removeAttr('disabled');
            } else {
                button.attr('disabled', 'disabled');
            }
        });

        function validateForm() {
            return password.val() && password.val().length > 3 &&
                password.val() == confirmPassword.val();
        }
    });
})(jQuery);
